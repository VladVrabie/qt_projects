#ifndef NEWTONEQUATION_H
#define NEWTONEQUATION_H

#include <QVector>
#include <QPointF>
#include <functional>

class NewtonEquation
{
public:
    NewtonEquation();
    ~NewtonEquation();

    void setModel(const QVector<QPointF> *model);

    std::function<double(double)> getXFunctionPtr();
    std::function<double(double)> getYFunctionPtr();

    double getStart() const;
    double getEnd() const;
    unsigned getResolution() const;

private:
    double applyNewton(double);

    const QVector<QPointF> *_model;
    QVector<double> _coeficienti;
};

#endif // NEWTONEQUATION_H

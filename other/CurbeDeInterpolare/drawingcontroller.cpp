#include "drawingcontroller.h"

DrawingController::DrawingController(QObject *parent,
                                     QVector<QPointF> *model)
    : Controller(parent, model)
{

}

DrawingController::~DrawingController()
{

}

void DrawingController::myMousePressEvent(QMouseEvent *event)
{
    Qt::MouseButton pressedButton = event->button();
    switch (pressedButton)
    {
    case Qt::RightButton:
        emit switchControllers();
        break;
    }
}

void DrawingController::myMouseMoveEvent(QMouseEvent *)
{

}

void DrawingController::myMouseReleaseEvent(QMouseEvent *)
{

}

void DrawingController::myKeyPressEvent(QKeyEvent *)
{

}

void DrawingController::myPaintEvent(QPaintEvent *, QPainter &painter)
{
    for (QPointF point : *model)
        painter.drawEllipse(point, 5, 5);

    NewtonEquation newton;
    newton.setModel(model);

    CurvePointsCalculator calc;
    QVector<QPointF> points = calc.calculatePoints(newton.getXFunctionPtr(),
                                                   newton.getYFunctionPtr(),
                                                   newton.getStart(),
                                                   newton.getEnd(),
                                                   newton.getResolution());

    QPainterPath path(points[0]);
    for (int i = 1; i < points.size(); ++i)
        path.lineTo(points[i]);

    painter.drawPath(path);
}

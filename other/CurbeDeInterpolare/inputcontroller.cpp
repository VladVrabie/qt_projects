#include "inputcontroller.h"

InputController::InputController(QObject *parent,
                                 QVector<QPointF> *model)
    : Controller(parent, model)
{

}

InputController::~InputController()
{

}

void InputController::myMousePressEvent(QMouseEvent *event)
{
    Qt::MouseButton pressedButton = event->button();
    switch (pressedButton)
    {
    case Qt::LeftButton:
        if (model->isEmpty())
            model->append(event->localPos());
        else
            if (event->localPos().x() > model->constLast().x())
                model->append(event->localPos());
        emit updatePainting();
        break;
    case Qt::MiddleButton:
        model->clear();
        emit updatePainting();
        break;
    case Qt::RightButton:
        if (model->size() > 2)
            emit switchControllers();
        break;
    }
}

void InputController::myMouseMoveEvent(QMouseEvent *event)
{

}

void InputController::myMouseReleaseEvent(QMouseEvent *event)
{

}

void InputController::myKeyPressEvent(QKeyEvent *event)
{

}

void InputController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
    for (QPointF point : *model)
        painter.drawEllipse(point, 5, 5);
}



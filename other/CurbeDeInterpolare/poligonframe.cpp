#include "poligonframe.h"
#include <QCursor>
#include <QDebug>
#include <QPainter>

PoligonFrame::PoligonFrame(QWidget *parent)
    : QFrame(parent)
{
    InputController *inputController = new InputController(this, model.get());
    controller = inputController;

    // cannot use new signal/slot syntax
    // weird Qt bug
    connect(controller, SIGNAL(updatePainting()),
            this, SLOT(update()));

    connect(inputController, &InputController::switchControllers,
            this, &PoligonFrame::switchingToDrawing);
}

PoligonFrame::~PoligonFrame()
{

}

void PoligonFrame::mousePressEvent(QMouseEvent *event)
{
    controller->myMousePressEvent(event);
}

void PoligonFrame::mouseMoveEvent(QMouseEvent *event)
{
    controller->myMouseMoveEvent(event);
}

void PoligonFrame::mouseReleaseEvent(QMouseEvent *event)
{
    controller->myMouseReleaseEvent(event);
}

void PoligonFrame::keyPressEvent(QKeyEvent *event)
{
    controller->myKeyPressEvent(event);
}

void PoligonFrame::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    controller->myPaintEvent(event, painter);
}

void PoligonFrame::switchingToDrawing()
{
   InputController *inputController = dynamic_cast<InputController*>(controller);

    disconnect(inputController, &InputController::switchControllers,
            this, &PoligonFrame::switchingToDrawing);
    disconnect(controller, SIGNAL(updatePainting()),
               this, SLOT(update()));

    // instantiat celalalt controler
    DrawingController *drawingController = new DrawingController(this, model.get());
    controller = drawingController;

    connect(drawingController, &DrawingController::switchControllers,
            this, &PoligonFrame::switchingToInput);
    connect(controller, SIGNAL(updatePainting()),
            this, SLOT(update()));

    update();
}

void PoligonFrame::switchingToInput()
{
    DrawingController *drawingController = dynamic_cast<DrawingController*>(controller);

    disconnect(drawingController, &DrawingController::switchControllers,
               this, &PoligonFrame::switchingToInput);
    disconnect(controller, SIGNAL(updatePainting()),
               this, SLOT(update()));

    // instantiat celalalt controler
    InputController *inputController = new InputController(this, model.get());
    controller = inputController;

    connect(inputController, &InputController::switchControllers,
            this, &PoligonFrame::switchingToDrawing);
    connect(controller, SIGNAL(updatePainting()),
            this, SLOT(update()));

    model->clear();
    update();
}

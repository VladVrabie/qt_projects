#ifndef INPUTCONTROLLER_H
#define INPUTCONTROLLER_H

#include "controller.h"

class InputController : public Controller
{
    Q_OBJECT

public:
    explicit InputController(QObject *parent = nullptr,
                             QVector<QPointF> *model = nullptr);
    ~InputController();

    // Controller interface
    void myMousePressEvent(QMouseEvent *event);
    void myMouseMoveEvent(QMouseEvent *event);
    void myMouseReleaseEvent(QMouseEvent *event);
    void myKeyPressEvent(QKeyEvent *event);
    void myPaintEvent(QPaintEvent *event, QPainter & painter);

signals:
    void switchControllers();

public slots:

private:

};

#endif // INPUTCONTROLLER_H

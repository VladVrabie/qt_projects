#ifndef ICONTROLLER_H
#define ICONTROLLER_H

#include <QObject>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QPainter>
#include <QPaintEvent>

#include <QVector>
#include <QPointF>

class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(QObject *parent = nullptr, QVector<QPointF> *model = nullptr);
    ~Controller();

    virtual void myMousePressEvent(QMouseEvent *event) = 0;
    virtual void myMouseMoveEvent(QMouseEvent *event) = 0;
    virtual void myMouseReleaseEvent(QMouseEvent *event) = 0;

    virtual void myKeyPressEvent(QKeyEvent *event) = 0;

    virtual void myPaintEvent(QPaintEvent *event, QPainter & painter) = 0;

signals:
    void updatePainting();

protected:
    QVector<QPointF> *model = nullptr;
};



#endif // ICONTROLLER_H

#ifndef POLIGONFRAME_H
#define POLIGONFRAME_H

#include <QFrame>
#include <QWidget>
#include <QDoubleSpinBox>

#include <memory>

#include "drawingcontroller.h"
#include "inputcontroller.h"

class PoligonFrame : public QFrame
{
public:
    PoligonFrame(QWidget *parent = 0);
    ~PoligonFrame();

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

    void keyPressEvent(QKeyEvent *event);

    void paintEvent(QPaintEvent *event);

signals:

public slots:
    void switchingToDrawing();
    void switchingToInput();

private:
    std::unique_ptr<QVector<QPointF>> model = std::make_unique<QVector<QPointF>>();
    Controller *controller = nullptr;
};

#endif // POLIGONFRAME_H

#include "newtonequation.h"

NewtonEquation::NewtonEquation()
{

}

NewtonEquation::~NewtonEquation()
{

}

void NewtonEquation::setModel(const QVector<QPointF> *model)
{
    _model = model;
    const auto &modelRef = *model;
    int nrPuncte = modelRef.size();

    QVector<QVector<double>> matrix;
    matrix.resize(nrPuncte);

    for (int i = 0; i < nrPuncte; ++i)
        matrix[i].append(modelRef[i].y());

    for (int j = 1; j < nrPuncte; ++j)
        for (int i = 0; i < nrPuncte - j; ++i)
        {
            matrix[i].append((matrix[i+1][j-1] - matrix[i][j-1]) / (modelRef[j + i].x() - modelRef[i].x()));
        }

    _coeficienti = matrix[0];
}

std::function<double (double)> NewtonEquation::getXFunctionPtr()
{
    std::function<double(double)> fPtr = [](double u){
        return u;
    };
    return fPtr;
}

std::function<double (double)> NewtonEquation::getYFunctionPtr()
{
    std::function<double(double)> fPtr = [this](double u){
        return this->applyNewton(u);
    };
    return fPtr;
}

double NewtonEquation::getStart() const
{
    return _model->first().x();
}

double NewtonEquation::getEnd() const
{
    return _model->last().x();
}

unsigned NewtonEquation::getResolution() const
{
    return 500;
}

double NewtonEquation::applyNewton(double x)
{
    double produs = 1.0;
    double result = 0.0;
    auto &modelRef = *_model;
    int nrPuncte = modelRef.size();

    for (int i = 0; i < nrPuncte - 1; ++i)
    {
        result += _coeficienti[i] * produs;
        produs *= (x - modelRef[i].x());
    }
    return result + _coeficienti[nrPuncte - 1] * produs;
}

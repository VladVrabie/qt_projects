#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->functionComboBox->addItem("");
    ui->functionComboBox->addItem("Elipsa");
    ui->functionComboBox->addItem("Spirala");
    ui->functionComboBox->addItem("Parabola oriz");
    ui->functionComboBox->addItem("Lissajous 12");
    ui->functionComboBox->addItem("Lissajous 32");
    ui->functionComboBox->addItem("Hypotrochoid 535");
    ui->functionComboBox->addItem("Star");
    ui->functionComboBox->addItem("Circles");
    ui->functionComboBox->addItem("Hearts");
    ui->functionComboBox->addItem("Vale");
    ui->functionComboBox->addItem("Evantai");

    connect(ui->functionComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &MainWindow::setFunctions);


    connect(ui->startSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            ui->curbaFrame, &CurbaFrame::setStartValue);
    connect(ui->endSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
            ui->curbaFrame, &CurbaFrame::setEndValue);
    connect(ui->resolutionSpinBox, QOverload<int>::of(&QSpinBox::valueChanged),
            ui->curbaFrame, &CurbaFrame::setResolution);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setFunctions(int index)
{
    switch (index)
    {
    case 1:
        ui->curbaFrame->setXFunction(FunctionList::xEllipse);
        ui->curbaFrame->setYFunction(FunctionList::yEllipse);
        ui->curbaFrame->setStartValue(0.0);
        ui->startSpinBox->setValue(0.0);
        ui->curbaFrame->setEndValue(2 * M_PI);
        ui->endSpinBox->setValue(2 * M_PI);
        break;
    case 2:
        ui->curbaFrame->setXFunction(FunctionList::xSpirala);
        ui->curbaFrame->setYFunction(FunctionList::ySpirala);
        ui->curbaFrame->setStartValue(0.0);
        ui->startSpinBox->setValue(0.0);
        ui->curbaFrame->setEndValue(20.0);
        ui->endSpinBox->setValue(20.0);
        break;
    case 3:
        ui->curbaFrame->setXFunction(FunctionList::xParabolaOriz);
        ui->curbaFrame->setYFunction(FunctionList::yParabolaOriz);
        ui->curbaFrame->setStartValue(-2.0);
        ui->startSpinBox->setValue(-2.0);
        ui->curbaFrame->setEndValue(1.0);
        ui->endSpinBox->setValue(1.0);
        break;
    case 4:
        ui->curbaFrame->setXFunction(FunctionList::xLissajous1);
        ui->curbaFrame->setYFunction(FunctionList::yLissajous2);
        ui->curbaFrame->setStartValue(0.0);
        ui->startSpinBox->setValue(0.0);
        ui->curbaFrame->setEndValue(2 * M_PI);
        ui->endSpinBox->setValue(2 * M_PI);
        break;
    case 5:
        ui->curbaFrame->setXFunction(FunctionList::xLissajous3);
        ui->curbaFrame->setYFunction(FunctionList::yLissajous2);
        ui->curbaFrame->setStartValue(0.0);
        ui->startSpinBox->setValue(0.0);
        ui->curbaFrame->setEndValue(2 * M_PI);
        ui->endSpinBox->setValue(2 * M_PI);
        break;
    case 6:
        ui->curbaFrame->setXFunction(FunctionList::xHypotrochoid535);
        ui->curbaFrame->setYFunction(FunctionList::yHypotrochoid535);
        ui->curbaFrame->setStartValue(0.0);
        ui->startSpinBox->setValue(0.0);
        ui->curbaFrame->setEndValue(6 * M_PI);
        ui->endSpinBox->setValue(6 * M_PI);
        break;
    case 7:
        ui->curbaFrame->setXFunction(FunctionList::xStar1);
        ui->curbaFrame->setYFunction(FunctionList::yStar1);
        ui->curbaFrame->setStartValue(0.0);
        ui->startSpinBox->setValue(0.0);
        ui->curbaFrame->setEndValue(2 * M_PI);
        ui->endSpinBox->setValue(2 * M_PI);
        break;
    case 8:
        ui->curbaFrame->setXFunction(FunctionList::xCircles);
        ui->curbaFrame->setYFunction(FunctionList::yCircles);
        ui->curbaFrame->setStartValue(0.0);
        ui->startSpinBox->setValue(0.0);
        ui->curbaFrame->setEndValue(2 * M_PI);
        ui->endSpinBox->setValue(2 * M_PI);
        break;
    case 9:
        ui->curbaFrame->setXFunction(FunctionList::xHearts);
        ui->curbaFrame->setYFunction(FunctionList::yHearts);
        ui->curbaFrame->setStartValue(0.0);
        ui->startSpinBox->setValue(0.0);
        ui->curbaFrame->setEndValue(2 * M_PI);
        ui->endSpinBox->setValue(2 * M_PI);
        break;
    case 10:
        ui->curbaFrame->setXFunction(FunctionList::xVale);
        ui->curbaFrame->setYFunction(FunctionList::yVale);
        ui->curbaFrame->setStartValue(0.0);
        ui->startSpinBox->setValue(0.0);
        ui->curbaFrame->setEndValue(2 * M_PI);
        ui->endSpinBox->setValue(2 * M_PI);
        break;
    case 11:
        ui->curbaFrame->setXFunction(FunctionList::xEvantai);
        ui->curbaFrame->setYFunction(FunctionList::yEvantai);
        ui->curbaFrame->setStartValue(0.0);
        ui->startSpinBox->setValue(0.0);
        ui->curbaFrame->setEndValue(2 * M_PI);
        ui->endSpinBox->setValue(2 * M_PI);
        break;
    }
    ui->curbaFrame->setResolution(100);
    ui->resolutionSpinBox->setValue(100);
}


#include "curbaframe.h"


CurbaFrame::CurbaFrame(QWidget *parent)
    : QFrame(parent)
{

}

CurbaFrame::~CurbaFrame()
{

}

void CurbaFrame::paintEvent(QPaintEvent *)
{
    calc.setWidgetWidth(width());
    calc.setWidgetHeight(height());

    if (xFunction != nullptr && yFunction != nullptr)
    {
        QPainter painter(this);

        QVector<QPointF> points = calc.calculatePoints(xFunction, yFunction,
                                       startValue, endValue, resolutionValue);

        QPointF O = points.takeFirst();
        QPointF A = points.takeFirst();
        QPointF B = points.takeFirst();
        QPointF C = points.takeFirst();
        QPointF D = points.takeFirst();

        int arrowOffset = 6;
        int arrowLength = 12;

        QPointF upArrowLeft(D.x() - arrowOffset, D.y() + arrowLength);
        QPointF upArrowRight(D.x() + arrowOffset, D.y() + arrowLength);
        QPointF rightArrowUp(B.x() - arrowLength, B.y() - arrowOffset);
        QPointF rightArrowDown(B.x() - arrowLength, B.y() + arrowOffset);

        painter.drawLine(A, B);
        painter.drawLine(C, D);

        painter.drawLine(D, upArrowLeft);
        painter.drawLine(D, upArrowRight);
        painter.drawLine(B, rightArrowUp);
        painter.drawLine(B, rightArrowDown);

        painter.setFont(QFont("Times", 11));
        painter.drawText(QPointF(O.x() + 5, O.y() - 5), QString("O"));
        painter.drawText(QPointF(B.x() - 25, B.y() - 5), QString("x"));
        painter.drawText(QPointF(D.x() + 5, D.y() + 25), QString("y"));

        QPainterPath path(points[0]);
        for (int i = 1; i < points.size(); ++i)
            path.lineTo(points[i]);

        painter.drawPath(path);
    }
}

void CurbaFrame::setStartValue(double value)
{
    startValue = value;
    update();
}

void CurbaFrame::setEndValue(double value)
{
    endValue = value;
    update();
}

void CurbaFrame::setResolution(int value)
{
    resolutionValue = static_cast<unsigned>(value);
    update();
}

void CurbaFrame::setXFunction(CurvePointsCalculator::fPtr xFunction)
{
    this->xFunction = xFunction;
    update();
}

void CurbaFrame::setYFunction(CurvePointsCalculator::fPtr yFunction)
{
    this->yFunction = yFunction;
    update();
}

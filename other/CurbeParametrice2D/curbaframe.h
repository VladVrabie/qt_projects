#ifndef CURBAFRAME_H
#define CURBAFRAME_H

#include <QObject>
#include <QWidget>
#include <QFrame>
#include <QPainter>
#include <QPainterPath>
#include <QFont>

#include "parametriccurvescalculator.h"

class CurbaFrame : public QFrame
{
    Q_OBJECT
public:
    explicit CurbaFrame(QWidget *parent = nullptr);
    ~CurbaFrame();

    void paintEvent(QPaintEvent *);

signals:

public slots:
    void setStartValue(double value);
    void setEndValue(double value);
    void setResolution(int value);
    void setXFunction(CurvePointsCalculator::fPtr xFunction);
    void setYFunction(CurvePointsCalculator::fPtr yFunction);

private:
    ParametricCurvesCalculator calc;

    CurvePointsCalculator::fPtr xFunction = nullptr;
    CurvePointsCalculator::fPtr yFunction = nullptr;
    double startValue = 0.0;
    double endValue = 1.0;
    unsigned resolutionValue = 10;
};

#endif // CURBAFRAME_H

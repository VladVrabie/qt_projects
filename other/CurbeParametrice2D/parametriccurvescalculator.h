#ifndef PARAMETRICCURVESCALCULATOR_H
#define PARAMETRICCURVESCALCULATOR_H

#include <algorithm>
#include "curvepointscalculator.h"
#include "transform.h"

class ParametricCurvesCalculator : public CurvePointsCalculator
{
public:
    ParametricCurvesCalculator();
    ~ParametricCurvesCalculator();

    void setWidgetWidth(double width);
    void setWidgetHeight(double height);

protected:
    QVector<QPointF> extraProcessing() override;

private:
    void firstStep();
    void secondStep();
    void thirdStep();
    void fourthStep();


    QVector<QPointF> _aux;
    Transform _transform;
    double _widgetWidth;
    double _widgetHeight;
    double _s3_xMax;
    double _s3_yMax;
};

#endif // PARAMETRICCURVESCALCULATOR_H

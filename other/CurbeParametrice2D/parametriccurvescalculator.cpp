#include "parametriccurvescalculator.h"

ParametricCurvesCalculator::ParametricCurvesCalculator()
{

}

ParametricCurvesCalculator::~ParametricCurvesCalculator()
{

}

void ParametricCurvesCalculator::setWidgetWidth(double width)
{
    _widgetWidth = width;
}

void ParametricCurvesCalculator::setWidgetHeight(double height)
{
    _widgetHeight = height;
}

QVector<QPointF> ParametricCurvesCalculator::extraProcessing()
{
    _transform.toIdentity();
    _P.prepend(QPointF(0.0f, 0.0f));
    _aux = _P;

    firstStep();
    secondStep();
    thirdStep();
    fourthStep();

    QPointF A(0.0, _aux[0].y());
    QPointF B(_widgetWidth, _aux[0].y());
    QPointF C(_aux[0].x(), _widgetHeight);
    QPointF D(_aux[0].x(), 0.0);

    _aux.insert(1, D);
    _aux.insert(1, C);
    _aux.insert(1, B);
    _aux.insert(1, A);

    return _aux;
}

void ParametricCurvesCalculator::firstStep()
{
    QPointF *xMinPoint = std::min_element(_P.begin(), _P.end(),
        [](const QPointF & p1, const QPointF & p2)
    {
        return p1.x() < p2.x();
    });

    QPointF *yMinPoint = std::min_element(_P.begin(), _P.end(),
        [](const QPointF & p1, const QPointF & p2)
    {
        return p1.y() < p2.y();
    });

    _transform.translatie(-xMinPoint->x(), -yMinPoint->y());
    _transform.aplicaTransformare(_P, _aux);
}

void ParametricCurvesCalculator::secondStep()
{
    QPointF *xMaxPoint = std::max_element(_aux.begin(), _aux.end(),
        [](const QPointF & p1, const QPointF & p2)
    {
        return p1.x() < p2.x();
    });

    QPointF *yMaxPoint = std::max_element(_aux.begin(), _aux.end(),
        [](const QPointF & p1, const QPointF & p2)
    {
        return p1.y() < p2.y();
    });

    double Sx = _widgetWidth / xMaxPoint->x();
    double Sy = _widgetHeight / yMaxPoint->y();
    double S = std::min(Sx, Sy);

    _s3_xMax = S * xMaxPoint->x();
    _s3_yMax = S * yMaxPoint->y();

    _aux = _P;
    _transform.scalareOrig(S, S);
    _transform.aplicaTransformare(_P, _aux);
}

void ParametricCurvesCalculator::thirdStep()
{
    double dx = (_widgetWidth - _s3_xMax) / 2;
    double dy = (_widgetHeight - _s3_yMax) / 2;

    _aux = _P;
    _transform.translatie(dx, dy);
    _transform.aplicaTransformare(_P, _aux);
}

void ParametricCurvesCalculator::fourthStep()
{
    _aux = _P;
    _transform.simetrieDreapta(0.0, _widgetHeight / 2, _widgetWidth, _widgetHeight / 2);
    _transform.aplicaTransformare(_P, _aux);
}

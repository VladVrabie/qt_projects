#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setPen(QPen(Qt::GlobalColor::black));

    painter.drawLine(0, 400, 200, 400);
    painter.drawLine(200, 500, 450, 500);
    painter.drawLine(450, 400, 800, 400);
    painter.drawLine(200, 250, 450, 250);
    painter.drawLine(200, 250, 200, 500);
    painter.drawLine(450, 250, 450, 500);

    painter.drawLine(300, 400, 300, 500);
    painter.drawLine(350, 400, 350, 500);
    painter.drawLine(300, 400, 350, 400);
    painter.drawLine(250, 300, 300, 300);
    painter.drawLine(250, 300, 250, 350);
    painter.drawLine(250, 350, 300, 350);
    painter.drawLine(300, 300, 300, 350);
    painter.drawLine(350, 300, 400, 300);
    painter.drawLine(350, 350, 400, 350);
    painter.drawLine(350, 300, 350, 350);
    painter.drawLine(400, 300, 400, 350);

    painter.drawLine(325, 150, 175, 270);
    painter.drawLine(325, 150, 475, 270);

    painter.drawEllipse(500, 50, 100, 100);
    painter.drawLine(450, 100, 490, 100);
    painter.drawLine(610, 100, 650, 100);
    painter.drawLine(515, 145, 480, 170);
    painter.drawLine(585, 145, 610, 170);
    painter.drawLine(515, 55, 480, 30);
    painter.drawLine(585, 55, 610, 30);


    QRectF rectangle(100.0, 100.0, 50.0, 50.0);
    QRectF rectangle2(58.0, 100.0, 50.0, 50.0);
    QRectF rectangle3(70.0, 50.0, 50.0, 50.0);
    QRectF rectangle4(28.0, 50.0, 50.0, 50.0);
    int startAngle = 30 * 16;
    int spanAngle = 120 * 16;
    painter.drawArc(rectangle, startAngle, spanAngle);
    painter.drawArc(rectangle2, startAngle, spanAngle);
    painter.drawArc(rectangle3, startAngle, spanAngle);
    painter.drawArc(rectangle4, startAngle, spanAngle);
}

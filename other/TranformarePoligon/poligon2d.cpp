#include "poligon2d.h"

Poligon2D::Poligon2D()
{

}

Poligon2D::~Poligon2D()
{

}

QVector<QPointF> &Poligon2D::getPoints()
{
    return points;
}

QPointF Poligon2D::getCentroid()
{
    return centroid;
}

void Poligon2D::aplicaTransformare()
{
    transform.aplicaTransformare(initialPoints, points);
}

void Poligon2D::translatie(double dx, double dy)
{
    transform.translatie(dx, dy);
}

void Poligon2D::scalarePunct(const QPointF &p, double sx, double sy)
{
    transform.scalarePunct(p, sx, sy);
}

void Poligon2D::scalareOrig(double sx, double sy)
{
    transform.scalareOrig(sx, sy);
}

void Poligon2D::rotatiePunct(const QPointF &p, double alpha)
{
    transform.rotatiePunct(p, alpha);
}

void Poligon2D::rotatieOrig(double alpha)
{
    transform.rotatieOrig(alpha);
}

void Poligon2D::simetrieDreapta(double xA, double yA, double xB, double yB)
{
    transform.simetrieDreapta(xA, yA, xB, yB);
}

void Poligon2D::setInitialPoints()
{
    initialPoints = points;
}

void Poligon2D::calculateCentroid()
{
    double area = 0.0f;
    double cx = 0.0f;
    double cy = 0.0f;
    int nbPoints = points.size();

    for (int i = 0; i < nbPoints; ++i)
    {
        double aux = points[i].x() * points[(i + 1) % nbPoints].y()
              - points[(i + 1) % nbPoints].x() * points[i].y();
        area += aux;

        cx += (points[i].x() + points[(i + 1) % nbPoints].x()) * aux;
        cy += (points[i].y() + points[(i + 1) % nbPoints].y()) * aux;
    }

    area /= 2;
    cx /= 6 * area;
    cy /= 6 * area;

    centroid.setX(cx);
    centroid.setY(cy);
}

void Poligon2D::resetPolygon()
{
    points = initialPoints;
    calculateCentroid();
    transform.toIdentity();
}

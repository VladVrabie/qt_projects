#include "transformationcontroller.h"
#include <QPainter>
#include <QPainterPath>
#include <QtMath>

TransformationController::TransformationController(QObject *parent, Poligon2D *model)
    : Controller(parent, model)
{

}

TransformationController::~TransformationController()
{

}

void TransformationController::myMousePressEvent(QMouseEvent *event)
{
    pressedButton = event->button();
    mousePressPos = event->localPos();
    lastPos = mousePressPos;
}

void TransformationController::myMouseMoveEvent(QMouseEvent *event)
{
    QPointF currentPos = event->localPos();

    switch (pressedButton) {
    case Qt::LeftButton: // translatie
    {
        QPointF delta = currentPos - lastPos;
        model->translatie(delta.x(), delta.y());

        model->aplicaTransformare();
        model->calculateCentroid();
        break;
    }
    case Qt::MiddleButton: // rotatie
    {
        double currentAngle = atan2(currentPos.y() - model->getCentroid().y(),
                                    currentPos.x() - model->getCentroid().x());
        if (currentAngle < 0)
            currentAngle += 2 * M_PI;

        double previousAngle = atan2(lastPos.y() - model->getCentroid().y(),
                                     lastPos.x() - model->getCentroid().x());
        if (previousAngle < 0)
            previousAngle += 2 * M_PI;

        double delta = currentAngle - previousAngle;
        model->rotatiePunct(model->getCentroid(), delta);

        model->aplicaTransformare();
        break;
    }
    case Qt::RightButton: // scalare
    {
        // Varianta cu scalare pe x si pe y
//        double sx = (currentPos.x() - model->getCentroid().x())
//                / (lastPos.x() - model->getCentroid().x());
//        double sy = (currentPos.y() - model->getCentroid().y())
//                / (lastPos.y() - model->getCentroid().y());

//        model->scalarePunct(model->getCentroid(), sx, sy);


        double currentDistance = qSqrt(qPow((currentPos.x() - model->getCentroid().x()), 2.0)
                                  + qPow((currentPos.y() - model->getCentroid().y()), 2.0));

        double lastDistance = qSqrt(qPow((lastPos.x() - model->getCentroid().x()), 2.0)
                                  + qPow((lastPos.y() - model->getCentroid().y()), 2.0));
        double s = currentDistance / lastDistance;

        model->scalarePunct(model->getCentroid(), s, s);

        model->aplicaTransformare();
        break;
    }
    default:
        break;
    }

    lastPos = currentPos;
    updatePainting();
}

void TransformationController::myMouseReleaseEvent(QMouseEvent *event)
{
    pressedButton = Qt::NoButton;
}

void TransformationController::myKeyPressEvent(QKeyEvent *event)
{
    // cand apas 'r' sa se reseteze poligonul
    if (event->key() == Qt::Key_R)
    {
        model->resetPolygon();
        updatePainting();
    }
    // cand apas 'd' se intoarce la modul desenat
    if (event->key() == Qt::Key_D)
    {
        emit switchControllers();
    }
    // cand apas 's' se face simetrie
    if (event->key() == Qt::Key_S)
    {
        model->simetrieDreapta(0.0, 0.0, 300.0, 300.0);
        model->aplicaTransformare();
        model->calculateCentroid();
        updatePainting();
    }
}

void TransformationController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
    int size = model->getPoints().size();
    QPainterPath path;
    path.moveTo(model->getPoints()[0]);
    for (int i = 1; i < size; ++i)
    {
        path.lineTo(model->getPoints()[i]);
    }

    path.lineTo(model->getPoints()[0]);
    painter.drawPath(path);

    painter.drawEllipse(model->getCentroid(), 3, 3);
}

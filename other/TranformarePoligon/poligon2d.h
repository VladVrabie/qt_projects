#ifndef MODEL_H
#define MODEL_H

#include "transform.h"

class Poligon2D
{
public:
    Poligon2D();
    ~Poligon2D();

    QVector<QPointF>& getPoints();
    QPointF getCentroid();

    void aplicaTransformare();
    void translatie(double dx, double dy);
    void scalarePunct(const QPointF &p, double sx, double sy);
    void scalareOrig(double sx, double sy);
    void rotatiePunct(const QPointF &p, double alpha);
    void rotatieOrig(double alpha);
    void simetrieDreapta(double xA, double yA, double xB, double yB);

    void setInitialPoints();
    void calculateCentroid();
    void resetPolygon();

private:
    QVector<QPointF> points;
    QVector<QPointF> initialPoints;
    QPointF centroid;
    Transform transform;
};

#endif // MODEL_H

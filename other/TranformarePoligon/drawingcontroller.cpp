#include "drawingcontroller.h"
#include <QPainter>
#include <QPainterPath>

DrawingController::DrawingController(QObject *parent, Poligon2D *model)
    : Controller(parent, model)
{

}

DrawingController::~DrawingController()
{

}

void DrawingController::myMousePressEvent(QMouseEvent *event)
{
    Qt::MouseButton pressedButton = event->button();
    switch (pressedButton)
    {
    case Qt::LeftButton:
        model->getPoints().append(event->localPos());
        break;
    case Qt::MiddleButton:
        model->getPoints().clear();
        emit updatePainting();
        break;
    case Qt::RightButton:
        if (model->getPoints().size() > 2)
            emit switchControllers();
        break;
    }
}

void DrawingController::myMouseMoveEvent(QMouseEvent *event)
{
    currentMousePosition = event->localPos();
    emit updatePainting();
}

void DrawingController::myMouseReleaseEvent(QMouseEvent *event)
{

}

void DrawingController::myKeyPressEvent(QKeyEvent *event)
{

}

void DrawingController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
    int size = model->getPoints().size();
    if (size > 0)
    {
        QPainterPath path;
        path.moveTo(model->getPoints()[0]);
        for (int i = 1; i < size; ++i)
            path.lineTo(model->getPoints()[i]);
        path.lineTo(currentMousePosition);
        painter.drawPath(path);
    }
}

#ifndef ICONTROLLER_H
#define ICONTROLLER_H

#include <QObject>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QPaintEvent>

#include "poligon2d.h"

class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(QObject *parent = nullptr, Poligon2D *model = nullptr);
    ~Controller();

    virtual void myMousePressEvent(QMouseEvent *event) = 0;
    virtual void myMouseMoveEvent(QMouseEvent *event) = 0;
    virtual void myMouseReleaseEvent(QMouseEvent *event) = 0;

    virtual void myKeyPressEvent(QKeyEvent *event) = 0;

    virtual void myPaintEvent(QPaintEvent *event, QPainter & painter) = 0;

signals:
    // add switchController here
    void updatePainting();

protected:
    Poligon2D *model = nullptr;
};



#endif // ICONTROLLER_H

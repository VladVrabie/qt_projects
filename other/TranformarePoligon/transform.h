#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <QPointF>
#include <QGenericMatrix>
#include <QVector>

class Transform
{
public:
    Transform();
    ~Transform();

    Transform & aplicaTransformare(const QVector<QPointF> & base,
                                   QVector<QPointF> & result);

    Transform & translatie(double dx, double dy);
    Transform & scalarePunct(const QPointF &p, double sx, double sy);
    Transform & scalareOrig(double sx, double sy);
    Transform & rotatiePunct(const QPointF &p, double alpha);
    Transform & rotatieOrig(double alpha);
    Transform & simetrieDreapta(double xA, double yA, double xB, double yB);

    Transform & toIdentity();

private:
    QGenericMatrix<3, 3, double> transformMatrix;

};

#endif // TRANSFORM_H

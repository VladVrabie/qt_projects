#ifndef DRAWINGCONTROLLER_H
#define DRAWINGCONTROLLER_H

#include "controller.h"

class DrawingController : public Controller
{
    Q_OBJECT

public:
    explicit DrawingController(QObject *parent = nullptr, Poligon2D *model = nullptr);
    ~DrawingController();

    // Controller interface
    void myMousePressEvent(QMouseEvent *event);
    void myMouseMoveEvent(QMouseEvent *event);
    void myMouseReleaseEvent(QMouseEvent *event);
    void myKeyPressEvent(QKeyEvent *event);
    void myPaintEvent(QPaintEvent *event, QPainter & painter);

signals:
    void switchControllers();

public slots:

private:
    QPointF currentMousePosition;
};

#endif // DRAWINGCONTROLLER_H

#ifndef TRANFORMATIONCONTROLLER_H
#define TRANFORMATIONCONTROLLER_H

#include "controller.h"

class TransformationController : public Controller
{
    Q_OBJECT

public:
    explicit TransformationController(QObject *parent = nullptr, Poligon2D *model = nullptr);
    ~TransformationController();

    // Controller interface
    void myMousePressEvent(QMouseEvent *event);
    void myMouseMoveEvent(QMouseEvent *event);
    void myMouseReleaseEvent(QMouseEvent *event);
    void myKeyPressEvent(QKeyEvent *event);
    void myPaintEvent(QPaintEvent *event, QPainter & painter);

signals:
    void switchControllers();

public slots:

private:
    Qt::MouseButton pressedButton = Qt::NoButton;
    QPointF mousePressPos, lastPos;
};

#endif // TRANFORMATIONCONTROLLER_H

#include "poligonframe.h"
#include <QCursor>
#include <QDebug>
#include <QPainter>

PoligonFrame::PoligonFrame(QWidget *parent)
    : QFrame(parent)
{
    DrawingController *drawingController = new DrawingController(this, model.get());
    controller = drawingController;

    // cannot use new signal/slot syntax
    // weird Qt bug
    connect(controller, SIGNAL(updatePainting()),
            this, SLOT(update()));

    connect(drawingController, &DrawingController::switchControllers,
            this, &PoligonFrame::switchingToTransformation);
}

PoligonFrame::~PoligonFrame()
{

}

void PoligonFrame::mousePressEvent(QMouseEvent *event)
{
    controller->myMousePressEvent(event);
}

void PoligonFrame::mouseMoveEvent(QMouseEvent *event)
{
    controller->myMouseMoveEvent(event);
}

void PoligonFrame::mouseReleaseEvent(QMouseEvent *event)
{
    controller->myMouseReleaseEvent(event);
}

void PoligonFrame::keyPressEvent(QKeyEvent *event)
{
    controller->myKeyPressEvent(event);
}

void PoligonFrame::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    controller->myPaintEvent(event, painter);
}

void PoligonFrame::switchingToDrawing()
{
    TransformationController *transfController = dynamic_cast<TransformationController*>(controller);

    disconnect(transfController, &TransformationController::switchControllers,
            this, &PoligonFrame::switchingToDrawing);
    disconnect(controller, SIGNAL(updatePainting()),
               this, SLOT(update()));

    // instantiat celalalt controler
    DrawingController *drawingController = new DrawingController(this, model.get());
    controller = drawingController;

    connect(drawingController, &DrawingController::switchControllers,
            this, &PoligonFrame::switchingToTransformation);
    connect(controller, SIGNAL(updatePainting()),
            this, SLOT(update()));

    model->getPoints().clear();
    update();
}

void PoligonFrame::switchingToTransformation()
{
    DrawingController *drawingController = dynamic_cast<DrawingController*>(controller);

    disconnect(drawingController, &DrawingController::switchControllers,
               this, &PoligonFrame::switchingToTransformation);
    disconnect(controller, SIGNAL(updatePainting()),
               this, SLOT(update()));

    // instantiat celalalt controler
    TransformationController *transfController = new TransformationController(this, model.get());
    controller = transfController;

    connect(transfController, &TransformationController::switchControllers,
            this, &PoligonFrame::switchingToDrawing);
    connect(controller, SIGNAL(updatePainting()),
            this, SLOT(update()));

    model->setInitialPoints();
    model->calculateCentroid();
    update();
}

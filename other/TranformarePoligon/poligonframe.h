#ifndef POLIGONFRAME_H
#define POLIGONFRAME_H

#include <QFrame>
#include <QWidget>

#include <memory>

#include "drawingcontroller.h"
#include "transformationcontroller.h"
#include "poligon2d.h"

class PoligonFrame : public QFrame
{
public:
    PoligonFrame(QWidget *parent = 0);
    ~PoligonFrame();

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

    void keyPressEvent(QKeyEvent *event);

    void paintEvent(QPaintEvent *event);

signals:

public slots:
    void switchingToDrawing();
    void switchingToTransformation();

private:
    std::unique_ptr<Poligon2D> model = std::make_unique<Poligon2D>();
    Controller *controller = nullptr;

};

#endif // POLIGONFRAME_H

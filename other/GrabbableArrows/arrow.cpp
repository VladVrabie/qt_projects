#include "arrow.h"

Arrow::Arrow(QPointF start, QPointF stop)
    : line(start, stop)
{
    initArrowLegs();

    startRect.setHeight(2*circleRadius);
    startRect.setWidth(2*circleRadius);

    stopRect.setHeight(2*circleRadius);
    stopRect.setWidth(2*circleRadius);

    moveRects(start, stop);
}

Arrow::Arrow(double x1, double y1, double x2, double y2)
    : Arrow(QPointF(x1, y1), QPointF(x2, y2))
{

}

Arrow::~Arrow()
{

}

double Arrow::x1() const
{
    return line.x1();
}

double Arrow::y1() const
{
    return line.y1();
}

double Arrow::x2() const
{
    return line.x2();
}

double Arrow::y2() const
{
    return line.y2();
}

double Arrow::dx() const
{
    return line.dx();
}

double Arrow::dy() const
{
    return line.dy();
}

void Arrow::setStart(const QPointF & start)
{
    line.setP1(start);
    initArrowLegs();
    moveRects(line.p1(), line.p2());
}

void Arrow::setStop(const QPointF & stop)
{
    line.setP2(stop);
    initArrowLegs();
    moveRects(line.p1(), line.p2());
}

Arrow::setterFPtr Arrow::setterToPoint(const QPointF &mousePosition)
{
    setterFPtr result = nullptr;
    if (insideCircle(mousePosition, startRect.center()))
    {
        result = [this](const QPointF & start){
            this->setStart(start);
        };
        return result;
    }
    else if (insideCircle(mousePosition, stopRect.center()))
    {
        result = [this](const QPointF & stop){
            this->setStop(stop);
        };
        return result;
    }
    return result;
}

void Arrow::drawArrow(QPainter &painter, bool withCircles) const
{
    if (line.isNull() == false)
    {
        painter.drawLine(line);
        painter.drawLine(leftLeg);
        painter.drawLine(rightLeg);

        if (withCircles == true)
        {
            painter.drawEllipse(startRect);
            painter.drawEllipse(stopRect);
        }
    }
}

void Arrow::initArrowLegs()
{
    if (line.isNull() == false)
    {
        leftLeg.setP1(line.p2());
        leftLeg.setP2(line.pointAt(0.8));
        leftLeg.setAngle(line.angle() + 150);
        if (leftLeg.length() < 10.0)
            leftLeg.setLength(10.0);

        rightLeg.setP1(line.p2());
        rightLeg.setP2(line.pointAt(0.8));
        rightLeg.setAngle(line.angle() - 150);
        if (rightLeg.length() < 10.0)
            rightLeg.setLength(10.0);
    }
}

void Arrow::moveRects(const QPointF & start, const QPointF & stop)
{
    startRect.moveTo(start - offset);
    stopRect.moveTo(stop - offset);
}

bool Arrow::insideCircle(const QPointF &point, const QPointF &center) const
{
    double x = point.x() - center.x();
    double y = point.y() - center.y();

    return x*x + y*y <= circleRadius * circleRadius;
}

#ifndef ARROWFRAME_H
#define ARROWFRAME_H

#include <QObject>
#include <QWidget>
#include <QFrame>

#include <QPointF>
#include <QPainter>
#include <QMouseEvent>

#include <functional>
#include <arrow.h>

class ArrowFrame : public QFrame
{
    Q_OBJECT

public:
    ArrowFrame(QWidget *parent = nullptr);
    ~ArrowFrame();

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

    void paintEvent(QPaintEvent *event);

private:
    bool expectingFirst = true;
    bool arrowNotDrawn = true;

    Arrow::setterFPtr setterPtr = nullptr;
    Arrow arrow;
};

#endif // ARROWFRAME_H

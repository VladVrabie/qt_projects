#include "arrowframe.h"

ArrowFrame::ArrowFrame(QWidget *parent)
    : QFrame(parent)
{

}

ArrowFrame::~ArrowFrame()
{

}

void ArrowFrame::mousePressEvent(QMouseEvent *event)
{
    Qt::MouseButton pressedButton = event->button();
    switch (pressedButton)
    {
    case Qt::LeftButton:
        if (arrowNotDrawn == true)
        {
            if (expectingFirst)
            {
                arrow.setStart(event->localPos());
                expectingFirst = false;
            }
            else
            {
                arrow.setStop(event->localPos());
                arrowNotDrawn = false;
                update();
            }
        }
        else
        {
            // std::function<void(const Arrow &, const QPointF &)> a = arrow.setterToPoint(event->localPos());
            // setterPtr = a;
            // setterPtr.assign(arrow.setterToPoint(event->localPos()));
            // setterPtr = std::mem_fn(arrow.setterToPoint(event->localPos()));

            setterPtr = arrow.setterToPoint(event->localPos());
            // update();
        }
        break;
    }

}

void ArrowFrame::mouseMoveEvent(QMouseEvent *event)
{
    if (arrowNotDrawn == false && setterPtr)
    {
        setterPtr(event->localPos());
        update();
    }
}

void ArrowFrame::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    arrow.drawArrow(painter);
}

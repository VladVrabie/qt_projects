#include <iostream>
#include "Matrix.h"

template <class T>
std::ostream & operator<< (std::ostream& out, const Matrix<T> & matrix)
{
	for (unsigned i = 0; i < matrix.getNbRows(); ++i)
	{
		for (unsigned j = 0; j < matrix.getNbColumns(); ++j)
			out << matrix(i, j) << " ";
		out << std::endl;
	}
	return out;
}

int main()
{
	//int *a = new int[0];
	//a[0] = 7;
	//delete[] a;

	Matrix<int> m(3, 3, 0, 1);

	m(0, 2) = 2;
	m(-1, -3) = 4;
	m(-1, -2) = 5;
	m(-3, 0) = 6;
	m(1, -1) = 3;
	std::cout << (m.isIdentity() ? "t" : "f") << std::endl;

	Matrix<int> m2(m);

	m.toIdentity();
	std::cout << (m.isIdentity() ? "t" : "f") << std::endl << std::endl;

	std::cout << m;
	std::cout << "Rows: " << m.getNbRows() << std::endl;
	std::cout << "Columns: " << m.getNbColumns() << std::endl << std::endl;

	std::cout << m2 << std::endl;

	std::cout << (m = m2) << std::endl;

	std::cout << (m == m2 ? "t" : "f") << std::endl;
	m2(0, 0) = -1;
	std::cout << (m != m2 ? "t" : "f") << std::endl << std::endl;

	//std::cout << m.transposed() << std::endl;

	std::cout << m2 << std::endl;
	std::cout << m << std::endl;
	std::cout << (m += m2) << std::endl;
	std::cout << (m -= m2) << std::endl;
	std::cout << (m *= 2) << std::endl;
	std::cout << (m /= 2) << std::endl;

	std::cout << (m + m + m) << std::endl;
	std::cout << (m + m + m - m) << std::endl;
	std::cout << ((m + m) * 2) << std::endl;
	std::cout << (2 * (m + m)) << std::endl;
	std::cout << ((m + m) / 2) << std::endl;

	Matrix<int> mp(3, 3, 0, 1), mp2(3, 3, 0, 1);
	mp.fill(1);
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			mp2(i, j) = i * 3 + j + 1;

	std::cout << mp << std::endl << mp2 << std::endl << (mp*mp2) << std::endl;
	std::cout << (mp2*mp2) << std::endl;
	std::cout << (mp*mp) << std::endl;

	Matrix<int> m3(3, 1, 0, 1);
	m3(0, 0) = 1;
	m3(1, 0) = 0;
	m3(2, 0) = 2;
	std::cout << (mp2*m3) << std::endl;
	return 0;
}
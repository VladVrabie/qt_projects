#pragma once

template<class T>
class Matrix
{
	unsigned nbRows = 0, nbColumns = 0;
	T **matrix = nullptr;
	T one, zero;
public:
	// Creates a matrix and sets it to identity if possible, to zero otherwise.
	Matrix(unsigned rows, unsigned columns, T zero, T one)
		: nbRows{ rows }, nbColumns{ columns }, zero{ zero }, one{ one }
	{
		matrix = allocateMatrix(rows, columns);
		for (unsigned i = 0; i < nbRows; ++i)
			for (unsigned j = 0; j < nbColumns; ++j)
				matrix[i][j] = zero;
		toIdentity();
	}

	// If sizes don't match, it deallocates this matrix and reallocates it to the same size as parameter other.
	Matrix<T> & operator= (const Matrix<T> & other) 
	{
		if (this != &other)
		{
			if (nbRows != other.getNbRows() || nbColumns != other.getNbColumns())
			{
				deallocateMatrix(matrix, nbRows);
				matrix = allocateMatrix(other.getNbRows(), other.getNbColumns());
			}

			for (unsigned i = 0; i < nbRows; ++i)
				for (unsigned j = 0; j < nbColumns; ++j)
					matrix[i][j] = other.matrix[i][j];
		}
		return *this;
	}
	
	~Matrix()
	{
		deallocateMatrix(matrix, nbRows);
	}

	unsigned getNbRows() const { return nbRows; }
	unsigned getNbColumns() const { return nbColumns; }
	T getZero() const { return zero; }
	T getOne() const { return one; }

	// First checks if nbRows is equal to nbColumns; if not it returns false.
	bool isIdentity() const 
	{
		if (nbRows == nbColumns)
		{
			for (unsigned i = 0; i < nbRows; ++i)
				for (unsigned j = 0; j < nbColumns; ++j)
				{
					if (i == j)
					{
						if (matrix[i][j] != one)
							return false;
					}
					else
						if (matrix[i][j] != zero)
							return false;
				}
			return true;
		}
		return false;
	}

	// Can only be done if nbRows is equal to nbColumns; otherwise does nothing.
	void toIdentity() 
	{
		if (nbRows == nbColumns)
		{
			for (unsigned i = 0; i < nbRows; ++i)
				for (unsigned j = 0; j < nbColumns; ++j)
				{
					if (i == j)
						matrix[i][j] = one;
					else
						matrix[i][j] = zero;
				}
		}
	}

	// First line becomes first column, second line becomes second column and so on.
	Matrix<T> transposed() const 
	{
		Matrix<T> result(nbColumns, nbRows, zero, one);
		for (unsigned i = 0; i < nbRows; ++i)
			for (unsigned j = 0; j < nbColumns; ++j)
				result.matrix[j][i] = matrix[i][j];
		return result;
	}

	void fill(const T & filler)
	{
		for (unsigned i = 0; i < nbRows; ++i)
			for (unsigned j = 0; j < nbColumns; ++j)
				matrix[i][j] = filler;
	}

	// Parameters can range from (-n, n-1) inclusive, where n is nbRows and nbColumns respectively.
	// If parameters are out of this range, zero is returned (please consider exception...)
	// great weakpoint for encapsulation
	T & operator() (int row, int column) 
	{
		int rowsInt = static_cast<int>(nbRows);
		int columnsInt = static_cast<int>(nbColumns);

		if (row >= rowsInt || row < -rowsInt || column >= columnsInt || column < -columnsInt)
			return zero;

		unsigned rowIndex, columnIndex;

		if (row < 0)
			rowIndex = rowsInt + row;
		else
			rowIndex = row;

		if (column < 0)
			columnIndex = columnsInt + column;
		else
			columnIndex = column;

		return matrix[rowIndex][columnIndex];
	}

	// Parameters can range from (-n, n-1) inclusive, where n is nbRows and nbColumns respectively.
	// If parameters are out of this range, zero is returned (please consider exception...)
	// great weakpoint for encapsulation
	const T & operator() (int row, int column) const 
	{
		int rowsInt = static_cast<int>(nbRows);
		int columnsInt = static_cast<int>(nbColumns);

		if (row >= rowsInt || row < -rowsInt || column >= columnsInt || column < -columnsInt)
			return zero;

		unsigned rowIndex, columnIndex;

		if (row < 0)
			rowIndex = rowsInt + row;
		else
			rowIndex = row;

		if (column < 0)
			columnIndex = columnsInt + column;
		else
			columnIndex = column;

		return matrix[rowIndex][columnIndex];
	}

	// First checks if matrices' sizes match; if not it returns false.
	bool operator== (const Matrix<T> & other) const 
	{
		if (nbRows == other.nbRows && nbColumns == other.nbColumns)
		{
			for (unsigned i = 0; i < nbRows; ++i)
				for (unsigned j = 0; j < nbColumns; ++j)
					if (matrix[i][j] != other.matrix[i][j])
						return false;
			return true;
		}
		return false;
	}

	// First checks if matrices' sizes match; if not it returns false.
	bool operator!= (const Matrix<T> & other) const 
	{
		return !(*this == other);
	}

	// First checks if matrices' sizes match; if not it does nothing.
	// Performs algebraic matrices addition.
	Matrix<T> & operator+= (const Matrix<T> & other) 
	{
		if (nbRows == other.nbRows && nbColumns == other.nbColumns)
		{
			for (unsigned i = 0; i < nbRows; ++i)
				for (unsigned j = 0; j < nbColumns; ++j)
					matrix[i][j] += other.matrix[i][j];
		}
		return *this;
	}

	// First checks if matrices' sizes match; if not it does nothing.
	// Performs algebraic matrices subtraction.
	Matrix<T> & operator-= (const Matrix<T> & other) 
	{
		if (nbRows == other.nbRows && nbColumns == other.nbColumns)
		{
			for (unsigned i = 0; i < nbRows; ++i)
				for (unsigned j = 0; j < nbColumns; ++j)
					matrix[i][j] -= other.matrix[i][j];
		}
		return *this;
	}

	// Multiplies all elements of this matrix by factor.
	Matrix<T> & operator*= (const T & factor) 
	{
		for (unsigned i = 0; i < nbRows; ++i)
			for (unsigned j = 0; j < nbColumns; ++j)
				matrix[i][j] *= factor;
		return *this;
	}

	// Divides all elements of this matrix by factor.
	Matrix<T> & operator/= (const T & factor) 
	{
		for (unsigned i = 0; i < nbRows; ++i)
			for (unsigned j = 0; j < nbColumns; ++j)
				matrix[i][j] /= factor;
		return *this;
	}

	// First checks if matrices' sizes match; if not it returns a copy of the first operand.
	// Performs algebraic matrices addition.
	template <class T>
	friend Matrix<T> operator+ (const Matrix<T> & m1, const Matrix<T> & m2) 
	{
		Matrix<T> result(m1);
		if (m1.nbRows == m2.nbRows && m1.nbColumns == m2.nbColumns)
			return result += m2;
		return result;
	}

	// First checks if matrices' sizes match; if not it returns a copy of the first operand.
	// Performs algebraic matrices subtraction.
	template <class T>
	friend Matrix<T> operator- (const Matrix<T> & m1, const Matrix<T> & m2) 
	{
		Matrix<T> result(m1);
		if (m1.nbRows == m2.nbRows && m1.nbColumns == m2.nbColumns)
			return result -= m2;
		return result;
	}

	// This works on the presumption that m1.one == m2.one and m1.zero == m2.zero
	// Checks if m1.getNrColumns() == m2.getNrRows(); if not returns result filled with m1.zero
	// Returns 
	template <class T>
	friend Matrix<T> operator* (const Matrix<T> & m1, const Matrix<T> & m2)
	{
		Matrix<T> rezultat(m1.getNbRows(), m2.getNbColumns(), m1.getZero(), m1.getOne());
		rezultat.fill(m1.getZero());
		if (m1.getNbColumns() == m2.getNbRows())
			for (unsigned i = 0; i < m1.getNbRows(); ++i)
				for (unsigned j = 0; j < m2.getNbColumns(); ++j)
					for (unsigned k = 0; k < m1.getNbColumns(); ++k)
						rezultat(i, j) += m1(i, k) * m2(k, j);
		return rezultat;
	}

	// Multiplies all elements of parameter matrix by factor.
	template <class T>
	friend Matrix<T> operator* (const Matrix<T> & matrix, const T & factor) 
	{
		Matrix<T> result(matrix);
		return result *= factor;
	}

	// Multiplies all elements of parameter matrix by factor.
	template <class T>
	friend Matrix<T> operator* (const T & factor, const Matrix<T> & matrix) 
	{
		return matrix * factor;
	}

	// Divides all elements of parameter matrix by factor.
	template <class T>
	friend Matrix<T> operator/ (const Matrix<T> & matrix, const T & factor) 
	{
		Matrix<T> result(matrix);
		return result /= factor;
	}

private:
	T ** allocateMatrix(unsigned rows, unsigned columns)
	{
		T **newMatrix = new T*[rows];
		for (unsigned i = 0; i < rows; ++i)
			newMatrix[i] = new T[columns];
		return newMatrix;
	}
	void deallocateMatrix(T ** & matrixPtr, unsigned rows)
	{
		for (unsigned i = 0; i < rows; ++i)
			delete[] matrixPtr[i];
		delete[] matrixPtr;
		matrixPtr = nullptr;
	}
};


#pragma once

template<unsigned rows, unsigned columns, class T = int>
class Matrix
{
	T **matrix = nullptr;
	T one, zero;

	//bool oneZeroSet = false;

public:

	// Creates a matrix and sets it to identity if possible, to zero otherwise.
	explicit Matrix(T zero, T one)
		: zero{ zero }, one{ one }
	{
		matrix = allocateMatrix(rows, columns);
		fill(zero);
		toIdentity();
	}

	Matrix(const Matrix<rows, columns, T> &other)
	{
		one = other.one;
		zero = other.zero;
		matrix = allocateMatrix(rows, columns);
		for (unsigned i = 0; i < rows; ++i)
			for (unsigned j = 0; j < columns; ++j)
				matrix[i][j] = other.matrix[i][j];
	}

	Matrix<rows, columns, T> & operator= (const Matrix<rows, columns, T> & other)
	{
		if (this != &other)
		{			
			for (unsigned i = 0; i < rows; ++i)
				for (unsigned j = 0; j < columns; ++j)
					matrix[i][j] = other.matrix[i][j];
		}
		return *this;
	}

	~Matrix()
	{
		deallocateMatrix(matrix, rows);
	}

	unsigned getRows() const { return rows; }
	unsigned getColumns() const { return columns; }
	T getZero() const { return zero; }
	T getOne() const { return one; }

	// First checks if rows is equal to columns; if not it returns false.
	bool isIdentity() const 
	{
		if (rows == columns)
		{
			for (unsigned i = 0; i < rows; ++i)
				for (unsigned j = 0; j < columns; ++j)
				{
					if (i == j)
					{
						if (matrix[i][j] != one)
							return false;
					}
					else
						if (matrix[i][j] != zero)
							return false;
				}
			return true;
		}
		return false;
	}

	// Can only be done if rows is equal to columns; otherwise does nothing.
	void toIdentity() 
	{
		if (rows == columns)
		{
			for (unsigned i = 0; i < rows; ++i)
				for (unsigned j = 0; j < columns; ++j)
				{
					if (i == j)
						matrix[i][j] = one;
					else
						matrix[i][j] = zero;
				}
		}
	}

	// First line becomes first column, second line becomes second column and so on.
	Matrix<columns, rows, T> transposed() const
	{
		Matrix<columns, rows, T> result(zero, one);
		for (unsigned i = 0; i < rows; ++i)
			for (unsigned j = 0; j < columns; ++j)
				result(j, i) = matrix[i][j];
		return result;
	}

	void fill(const T & filler)
	{
		for (unsigned i = 0; i < rows; ++i)
			for (unsigned j = 0; j < columns; ++j)
				matrix[i][j] = filler;
	}

	// Parameters can range from (-n, n-1) inclusive, where n is rows and columns respectively.
	// If parameters are out of this range, zero is returned (please consider exception...)
	// great weakpoint for encapsulation
	T & operator() (int row, int column) 
	{
		// TODO: verifify if cast went well
		int rowsInt = static_cast<int>(rows);
		int columnsInt = static_cast<int>(columns);

		if (row >= rowsInt || row < -rowsInt || column >= columnsInt || column < -columnsInt)
			return zero;

		unsigned rowIndex, columnIndex;

		if (row < 0)
			rowIndex = rowsInt + row;
		else
			rowIndex = row;

		if (column < 0)
			columnIndex = columnsInt + column;
		else
			columnIndex = column;

		return matrix[rowIndex][columnIndex];
	}

	// Parameters can range from (-n, n-1) inclusive, where n is rows and columns respectively.
	// If parameters are out of this range, zero is returned (please consider exception...)
	// great weakpoint for encapsulation
	const T & operator() (int row, int column) const 
	{
		// TODO: verifify if cast went well
		int rowsInt = static_cast<int>(rows);
		int columnsInt = static_cast<int>(columns);

		if (row >= rowsInt || row < -rowsInt || column >= columnsInt || column < -columnsInt)
			return zero;

		unsigned rowIndex, columnIndex;

		if (row < 0)
			rowIndex = rowsInt + row;
		else
			rowIndex = row;

		if (column < 0)
			columnIndex = columnsInt + column;
		else
			columnIndex = column;

		return matrix[rowIndex][columnIndex];
	}


	bool operator== (const Matrix<rows, columns, T> & other) const
	{
		for (unsigned i = 0; i < rows; ++i)
			for (unsigned j = 0; j < columns; ++j)
				if (matrix[i][j] != other.matrix[i][j])
					return false;
		return true;
	}

	
	bool operator!= (const Matrix<rows, columns, T> & other) const
	{
		return !(*this == other);
	}

	// Performs algebraic matrices addition.
	Matrix<rows, columns, T> & operator+= (const Matrix<rows, columns, T> & other)
	{
		for (unsigned i = 0; i < rows; ++i)
			for (unsigned j = 0; j < columns; ++j)
				matrix[i][j] += other.matrix[i][j];
		return *this;
	}

	// Performs algebraic matrices subtraction.
	Matrix<rows, columns, T> & operator-= (const Matrix<rows, columns, T> & other)
	{
		for (unsigned i = 0; i < rows; ++i)
			for (unsigned j = 0; j < columns; ++j)
				matrix[i][j] -= other.matrix[i][j];
		return *this;
	}

	// Multiplies all elements of this matrix by factor.
	Matrix<rows, columns, T> & operator*= (const T & factor)
	{
		for (unsigned i = 0; i < rows; ++i)
			for (unsigned j = 0; j < columns; ++j)
				matrix[i][j] *= factor;
		return *this;
	}

	// Divides all elements of this matrix by factor.
	Matrix<rows, columns, T> & operator/= (const T & factor)
	{
		for (unsigned i = 0; i < rows; ++i)
			for (unsigned j = 0; j < columns; ++j)
				matrix[i][j] /= factor;
		return *this;
	}

	// Performs algebraic matrices addition.
	friend Matrix<rows, columns, T> operator+ (const Matrix<rows, columns, T> & m1, const Matrix<rows, columns, T> & m2)
	{
		Matrix<rows, columns, T> result(m1);
		return result += m2;
	}


	// Performs algebraic matrices subtraction.
	friend Matrix<rows, columns, T> operator- (const Matrix<rows, columns, T> & m1, const Matrix<rows, columns, T> & m2)
	{
		Matrix<rows, columns, T> result(m1);
		return result -= m2;
	}

	// This works on the presumption that m1.one == m2.one and m1.zero == m2.zero
	// No check is done
	template <unsigned columns2>
	friend Matrix<rows, columns2, T> operator* (const Matrix<rows, columns, T> & m1, 
												const Matrix<columns, columns2, T> & m2)
	{


		Matrix<rows, columns2, T> rezultat(m1.getZero(), m1.getOne());
		rezultat.fill(m1.getZero());

		for (unsigned i = 0; i < rows; ++i)
			for (unsigned j = 0; j < columns2; ++j)
				for (unsigned k = 0; k < columns; ++k)
					rezultat(i, j) += m1(i, k) * m2(k, j);
		return rezultat;

	}

	// Multiplies all elements of parameter matrix by factor.
	friend Matrix<rows, columns, T> operator* (const Matrix<rows, columns, T> & matrix, const T & factor) 
	{
		Matrix<rows, columns, T> result(matrix);
		return result *= factor;
	}

	// Multiplies all elements of parameter matrix by factor.
	friend Matrix<rows, columns, T> operator* (const T & factor, const Matrix<rows, columns, T> & matrix) 
	{
		return matrix * factor;
	}

	// Divides all elements of parameter matrix by factor.
	friend Matrix<rows, columns, T> operator/ (const Matrix<rows, columns, T> & matrix, const T & factor) 
	{
		Matrix<rows, columns, T> result(matrix);
		return result /= factor;
	}


private:
	T ** allocateMatrix(unsigned rows, unsigned columns)
	{
		T **newMatrix = new T*[rows];
		for (unsigned i = 0; i < rows; ++i)
			newMatrix[i] = new T[columns];
		return newMatrix;
	}
	void deallocateMatrix(T ** & matrixPtr, unsigned rows)
	{
		for (unsigned i = 0; i < rows; ++i)
			delete[] matrixPtr[i];
		delete[] matrixPtr;
		matrixPtr = nullptr;
	}
};


typedef Matrix<3, 3, int> Matrix3x3I;
typedef Matrix<3, 3, float> Matrix3x3F;
typedef Matrix<3, 3, double> Matrix3x3D;
#include <QCoreApplication>

#include <iostream>
#include <chrono>

#include <QGenericMatrix>
#include "Matrix.h"


void mine()
{
    Matrix3x3D m(0.0, 1.0);
    m.fill(2.0);
    for (int i = 0; i < 1000000;++i)
    {
        m * m + m;
    }
}

void theirs()
{
    QGenericMatrix<3, 3, double> m;
    m.fill(2.0);
    for (int i = 0; i < 1000000;++i)
    {
        m * m + m;
    }
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    mine();
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 ).count();
    std::cout << duration << std::endl;

    t1 = std::chrono::high_resolution_clock::now();
    theirs();
    t2 = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 ).count();
    std::cout << duration << std::endl;


    return a.exec();
}

#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QObject>
#include <QString>
#include <QPair>
#include "NrRational.h"

class Calculator : public QObject
{
    Q_OBJECT
public:
    enum PendingOperation
    {
        NONE,
        ADDITION,
        SUBTRACTION,
        MULTIPLICATION,
        DIVISION
    };

    enum MemoryOperation
    {
        M_ADDITION,
        M_SUBTRACTION
    };


    explicit Calculator(QObject *parent = nullptr);

    QPair<QString, QString> nextOperation(const QString &, const QString &, PendingOperation);
    void clearTemp();
    void setPendingOperation(PendingOperation op);

    void memoryOperation(const QString &, const QString &, MemoryOperation);
    QPair<QString, QString> memoryRecall();
    void clearMemory();

signals:

public slots:

private:
    NrRational stringsToNrRational(const QString &, const QString &);
    void doOperation(NrRational);


    NrRational memTemporara, memPermanenta;
    PendingOperation pendingOp = PendingOperation::NONE;
};

#endif // CALCULATOR_H

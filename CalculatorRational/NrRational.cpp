#include "NrRational.h"
#include <climits>
#include "IntOverflow.h"
#include "IllegalFraction.h"

NrRational::NrRational()
{
}

NrRational::~NrRational()
{
}

NrRational::NrRational(int numarator, int numitor)
{
	if (numitor == 0)
		throw IllegalFraction("Nu aveti voie cu numitor 0");
	this->numarator = numarator;
	this->numitor = numitor;
	this->standardizeSign()
		.simplifica();
}

NrRational::NrRational(const NrRational & other) 
	: NrRational(other.numarator, other.numitor)
{
}

NrRational & NrRational::operator=(const NrRational & other)
{
	if (this != &other)
	{
		numarator = other.numarator;
		numitor = other.numitor;
		this->standardizeSign()
			.simplifica();
	}
	return *this;
}

int NrRational::getNumarator() const
{
	return numarator;
}

int NrRational::getNumitor() const
{
	return numitor;
}

void NrRational::setNumarator(const int & numarator)
{
	this->numarator = numarator;
	// nu cred ca trebuie standardizeSign
	simplifica();
}

void NrRational::setNumitor(const int & numitor)
{
	if (numitor == 0)
		throw IllegalFraction("Nu aveti voie cu numitor 0");
	this->numitor = numitor;
	this->standardizeSign()
		.simplifica();
}

NrRational& NrRational::standardizeSign()
{
	standardizeSign(numarator, numitor);
	return *this;
}

NrRational& NrRational::simplifica()
{
	simplifica(numarator, numitor);
	return *this;
}

NrRational& NrRational::flip()
{
	if (numarator == 0)
		throw IllegalFraction("You can't flip a fraction when numerator is 0");
	int aux = numarator;
	numarator = numitor;
	numitor = aux;
	
	return this->standardizeSign();
}

NrRational & NrRational::operator+=(const NrRational & other)
{
	long long int numitor_nou = cmmmc(numitor, other.numitor);
	long long int numarator_nou = static_cast<long long int>(numarator) * (numitor_nou / numitor)
		+ static_cast<long long int>(other.numarator) * (numitor_nou / other.numitor);
	
	standardizeSign(numarator_nou, numitor_nou);
	simplifica(numarator_nou, numitor_nou);

	if (numarator_nou > INT_MAX || numitor_nou > INT_MAX)
		throw IntOverflow("Eroare la adunare de overflow");
	if (numarator_nou < INT_MIN)
		throw IntOverflow("Eroare la adunare de underflow");

	numitor = static_cast<int>(numitor_nou);
	numarator = static_cast<int>(numarator_nou);
	return *this;
}

NrRational operator+(NrRational nr1, const NrRational & nr2)
{
	return nr1 += nr2;
}

NrRational & NrRational::operator-=(const NrRational & other)
{
	return *this += -other;
}

NrRational operator-(NrRational nr1, const NrRational & nr2)
{
	return nr1 -= nr2;
}

NrRational & NrRational::operator*=(const NrRational & other)
{

	long long int numarator_nou = static_cast<long long int>(numarator) * other.numarator;
	long long int numitor_nou = static_cast<long long int>(numitor) * other.numitor;
	
	standardizeSign(numarator_nou, numitor_nou);
	simplifica(numarator_nou, numitor_nou);
	
	if (numarator_nou > INT_MAX || numitor_nou > INT_MAX)
		throw IntOverflow("Eroare la inmultire de overflow");
	if (numarator_nou < INT_MIN)
		throw IntOverflow("Eroare la inmultire de underflow");

	numarator = static_cast<int>(numarator_nou);
	numitor = static_cast<int>(numitor_nou);
	return *this;
}

NrRational operator*(NrRational nr1, const NrRational & nr2)
{
	return nr1 *= nr2;
}

NrRational & NrRational::operator/=(const NrRational & other)
{
	return *this *= NrRational(other).flip();
}

NrRational operator/(NrRational nb1, const NrRational & nb2)
{
	return nb1 /= nb2;
}

NrRational operator+(const NrRational & nr)
{
	return nr;
}

NrRational operator-(const NrRational & nr)
{
	return NrRational(-nr.numarator, nr.numitor);
}

int NrRational::cmmmc(int numb1, int numb2)
{
	long long int product = static_cast<long long int>(numb1) * numb2;
	if (product > INT_MAX)
		throw IntOverflow("Eroare la cmmmc de overflow");
	if (product < INT_MIN)
		throw IntOverflow("Eroare la cmmmc de underflow");
	return static_cast<int>(product) / cmmdc(numb1, numb2);
}

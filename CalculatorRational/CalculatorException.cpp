#include "CalculatorException.h"

CalculatorException::CalculatorException(const char* msg)
    : std::exception(msg)
{
}

CalculatorException::~CalculatorException()
{
}


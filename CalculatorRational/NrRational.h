#pragma once
class NrRational
{
	int numarator = 0, numitor = 1;

public:
	NrRational();
	NrRational(int, int);
	NrRational(const NrRational &);

	NrRational& operator= (const NrRational &);

	~NrRational();

	int getNumarator() const;
	int getNumitor() const;
	void setNumarator(const int &);
	void setNumitor(const int &);

	NrRational& standardizeSign();
	NrRational& simplifica();
	NrRational& flip();

	NrRational& operator+= (const NrRational &);
	friend NrRational operator+ (NrRational , const NrRational &);

	NrRational& operator-= (const NrRational &);
	friend NrRational operator- (NrRational, const NrRational &);

	NrRational& operator*= (const NrRational &);
	friend NrRational operator* (NrRational, const NrRational &);

	NrRational& operator/= (const NrRational &);
	friend NrRational operator/ (NrRational, const NrRational &);

	friend NrRational operator+ (const NrRational &);
	friend NrRational operator- (const NrRational &);

	template <class T>
	static T cmmdc(T numb1, T numb2)
	{
		for (T rest = 0; numb2; (rest = numb1 % numb2), numb1 = numb2, numb2 = rest);
		return numb1;
	}
	
	static int cmmmc(int, int);
	
private:
	template <class T>
	void standardizeSign(T& numarator, T& numitor)
	{
		if (numitor < 0)
		{
			numarator = -numarator;
			numitor = -numitor;
		}
	}
	
	template <class T>
	void simplifica(T& numarator, T& numitor)
	{
		T divizor = cmmdc(abs(numarator), numitor);
		numarator /= divizor;
		numitor /= divizor;
	}
};


#include "Calculator.h"
#include "CalculatorException.h"

Calculator::Calculator(QObject *parent)
    : QObject(parent)
{
}


QPair<QString, QString> Calculator::nextOperation(const QString &numaratorStr,
                                                  const QString &numitorStr,
                                                  Calculator::PendingOperation nextOp = PendingOperation::NONE)
{
    NrRational newOperand = stringsToNrRational(numaratorStr, numitorStr);

    doOperation(newOperand);

    newOperand = memTemporara;

    switch (nextOp) {
    case PendingOperation::NONE:
        //memTemporara = NrRational(0, 1);
    case PendingOperation::ADDITION:
    case PendingOperation::SUBTRACTION:
    case PendingOperation::MULTIPLICATION:
    case PendingOperation::DIVISION:
        pendingOp = nextOp;
        break;
    default:
        throw CalculatorException("Arithmetic Operation not supported");
    }


    return qMakePair(QString::number(newOperand.getNumarator()),
                     QString::number(newOperand.getNumitor()));
}

void Calculator::clearTemp()
{
    memTemporara = NrRational(0, 1);
    pendingOp = PendingOperation::NONE;
}

void Calculator::setPendingOperation(Calculator::PendingOperation op)
{
    pendingOp = op;
}

void Calculator::memoryOperation(const QString &numaratorStr,
                                 const QString &numitorStr,
                                 Calculator::MemoryOperation operation)
{
    NrRational newOperand = stringsToNrRational(numaratorStr, numitorStr);

    switch (operation) {
    case MemoryOperation::M_ADDITION:
        memPermanenta += newOperand;
        break;
    case MemoryOperation::M_SUBTRACTION:
        memPermanenta -= newOperand;
        break;
    default:
        throw CalculatorException("Operation on memory not supported");
    }
}

QPair<QString, QString> Calculator::memoryRecall()
{
    return qMakePair(QString::number(memPermanenta.getNumarator()),
                     QString::number(memPermanenta.getNumitor()));
}

void Calculator::clearMemory()
{
    memPermanenta = NrRational(0, 1);
}

NrRational Calculator::stringsToNrRational(const QString &numaratorStr, const QString &numitorStr)
{
    bool ok = true;
    int numarator = numaratorStr.toInt(&ok);
    if (!ok)
        throw CalculatorException("Eroare la conversia numaratorului la int");
    int numitor = numitorStr.toInt(&ok);
    if (!ok)
        throw CalculatorException("Eroare la conversia numitorului la int");

    return NrRational(numarator, numitor);
}

void Calculator::doOperation(NrRational newOperand)
{
    switch (pendingOp)
    {
    case PendingOperation::NONE:
        memTemporara = newOperand;
        break;
    case PendingOperation::ADDITION:
        memTemporara += newOperand;
        break;
    case PendingOperation::SUBTRACTION:
        memTemporara -= newOperand;
        break;
    case PendingOperation::MULTIPLICATION:
        memTemporara *= newOperand;
        break;
    case PendingOperation::DIVISION:
        memTemporara /= newOperand;
        break;
    default:
        throw CalculatorException("Operation not supported");
    }
}


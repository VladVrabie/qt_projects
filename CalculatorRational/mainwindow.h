#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QValidator>
#include <QMap>
#include "Calculator.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


public slots:
    void digitButtonClicked();
    void buttonCEClicked();
    void buttonCClicked();
    void buttonBackspaceClicked();
    void buttonFocusClicked();
    void buttonSignClicked();
    void buttonInversClicked();
    void buttonArithmeticOpClicked();
    void buttonMemoryOpClicked();
    void buttonMemoryRClicked();
    void buttonMemoryCClicked();

    void keyBoardPressed(const QString & text);

private slots:
    void on_lineEditNumarator_returnPressed();
    void on_lineEditNumitor_returnPressed();

private:
    void digitClicked(const QString & digit);
    void doArithmeticOperation(const QString &);
    void initData();
    void conectareaButoanelor();

    Ui::MainWindow *ui;

    Calculator *calculator;

    // facut in QMap
    QMap<QString, QString> buttonToDigitMap;
    QMap<QString, Calculator::PendingOperation> buttonToOperationMap;
    QMap<QString, Calculator::MemoryOperation> buttonToMemOpMap;

    QString intRegexPatternNumarator, intRegexPatternNumitor;
    QValidator *intRegexValidatorNumarator, *intRegexValidatorNumitor;

    QString previousNumarator, previousNumitor;
    bool expectingNumarator = true;
    bool expectingNumitor = true;
};

#endif // MAINWINDOW_H

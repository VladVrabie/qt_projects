#pragma once
#include <exception>

class IllegalFraction : public std::exception
{
public:
	IllegalFraction(const char * msg);
	~IllegalFraction();
};


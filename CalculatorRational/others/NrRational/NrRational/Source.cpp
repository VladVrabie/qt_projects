#include <iostream>
#include "NrRational.h"

std::ostream & operator<< (std::ostream& out, const NrRational & nr)
{
	return out << nr.getNumarator() << " " << nr.getNumitor();
}

auto main() -> int
{
	try 
	{
		NrRational a;
		NrRational b(2, 1);
		//NrRational c(1, 0);
		NrRational d(1, -1), e(2, -4), f(e);
		d = f;
		f = f;

		std::cout << a << " " << b << " " << d << " " << e << " " << f << std::endl;
		e.setNumarator(-100);
		std::cout << e << std::endl;
		e.setNumitor(-200);
		std::cout << e << std::endl;
		//e.setNumitor(0);
		//a.flip();
		e.flip().setNumitor(-8);
		std::cout << e << std::endl;

		e += f; // -1/2 + -1/2
		std::cout << e << std::endl;

		NrRational s = NrRational(1, 3) + NrRational(1, 17);
		std::cout << "s = " << s << std::endl;

		s = NrRational(1, 3) + NrRational(1, 15);
		std::cout << "s = " << s << std::endl;

		s = NrRational(2, 3) + NrRational(1, 15);
		std::cout << "s = " << s << std::endl;

		s = NrRational(2'000'000'001, 5) + NrRational(2'000'000'004, 5);
		std::cout << "s = " << s << std::endl;

		s = NrRational(2'000'000'001, -5) + NrRational(2'000'000'004, -5);
		std::cout << "s = " << s << std::endl;

		s = NrRational(-2'000'000'001, 5) + NrRational(2'000'000'004, -5);
		std::cout << "s = " << s << std::endl;

		//s = NrRational(2'000'000'001, 1) + NrRational(2'000'000'004, 1);
		//std::cout << "s = " << s << std::endl;

		//s = NrRational(-2'000'000'001, 1) + NrRational(2'000'000'004, -1);
		//std::cout << "s = " << s << std::endl;

		s = NrRational(2, 5) - NrRational(1, 5);
		std::cout << "s = " << s << std::endl;

		s = NrRational(2, 5) - NrRational(-1, 5);
		std::cout << "s = " << s << std::endl;

		s = NrRational(2, 5) - NrRational(1, -5);
		std::cout << "s = " << s << std::endl;

		s = NrRational(1, 100) - NrRational(1, 10);
		std::cout << "s = " << s << std::endl;

		s = NrRational(2, 5) - NrRational(5, 5);
		std::cout << "s = " << s << std::endl;

		NrRational p = NrRational(1, 5) * NrRational(1, 2);
		std::cout << "p = " << p << std::endl;

		p = NrRational(1, 5) * NrRational(1, -2);
		std::cout << "p = " << p << std::endl;

		p = NrRational(1, -5) * NrRational(1, -2);
		std::cout << "p = " << p << std::endl;

		p = NrRational(1, 5) * NrRational(5, -2);
		std::cout << "p = " << p << std::endl;

		//p = NrRational(1'100'000, 1) * NrRational(2'200'000, 1);
		//std::cout << "p = " << p << std::endl;

		//p = NrRational(-1'100'000, 1) * NrRational(2'200'000, 1);
		//std::cout << "p = " << p << std::endl;

		//p = NrRational(1, 1'100'000) * NrRational(1, 2'200'000);
		//std::cout << "p = " << p << std::endl;

		p = NrRational(121, 1000) * NrRational(500, -11);
		std::cout << "p = " << p << std::endl;

		p = NrRational(1, -5) / NrRational(-5, -2);
		std::cout << "p = " << p << std::endl;

		p = NrRational(1, 5) / NrRational(5, -2);
		std::cout << "p = " << p << std::endl;

		p = NrRational(100, -5) / NrRational(25, -10);
		std::cout << "p = " << p << std::endl;

		//p = NrRational(1'000'000, 5) / NrRational(-2, 200'000'000);
		//std::cout << "p = " << p << std::endl;
	}
	catch (std::exception e)
	{
		std::cout << e.what() << std::endl;
	}

	return 0;
}
#pragma once
#include <exception>
class IntOverflow : public std::exception
{
public:
	IntOverflow(const char* msg);
	~IntOverflow();
};


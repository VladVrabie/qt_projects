#ifndef CALCULATOREXCEPTION_H
#define CALCULATOREXCEPTION_H

#include <exception>

class CalculatorException : public std::exception
{
public:
    CalculatorException(const char* msg);
    ~CalculatorException();
};

#endif // CALCULATOREXCEPTION_H

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    initData();

    // SETAREA VALIDATORILOR PT LINEEDITURI
    QRegularExpression intRegexNumarator(intRegexPatternNumarator);
    QRegularExpression intRegexNumitor(intRegexPatternNumitor);

    intRegexValidatorNumarator = new QRegularExpressionValidator(intRegexNumarator, this);
    intRegexValidatorNumitor = new QRegularExpressionValidator(intRegexNumitor, this);

    ui->lineEditNumarator->setValidator(intRegexValidatorNumarator);
    ui->lineEditNumitor->setValidator(intRegexValidatorNumitor);

    ui->lineEditNumarator->setFocus();

    conectareaButoanelor();

    connect(ui->lineEditNumarator, &QLineEdit::textEdited,
            this, &MainWindow::keyBoardPressed);
    connect(ui->lineEditNumitor, &QLineEdit::textEdited,
            this, &MainWindow::keyBoardPressed);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::digitButtonClicked()
{
    QObject *senderObject = QObject::sender();
    digitClicked(buttonToDigitMap[senderObject->objectName()]);
}

void MainWindow::buttonCEClicked()
{
    if (ui->lineEditNumarator->hasFocus())
        ui->lineEditNumarator->setText("0");
    else
        ui->lineEditNumitor->setText("1");
}

void MainWindow::buttonCClicked()
{
    calculator->clearTemp();
    ui->lineEditNumarator->setText("0");
    ui->lineEditNumitor->setText("1");
    ui->lineEditNumarator->setFocus();
    expectingNumarator = true;
    expectingNumitor = true;
}

void MainWindow::buttonBackspaceClicked()
{
    QString emptyPlaceholder;
    QLineEdit * focusedEdit;
    bool *expectingLineEdit;
    if (ui->lineEditNumarator->hasFocus())
    {
        focusedEdit = ui->lineEditNumarator;
        emptyPlaceholder = "0";
        expectingLineEdit = &expectingNumarator;
    }
    else if (ui->lineEditNumitor->hasFocus())
    {
        focusedEdit = ui->lineEditNumitor;
        emptyPlaceholder = "";
        expectingLineEdit = &expectingNumitor;
    }
    if (focusedEdit->text().compare("") != 0)
    {
        QString newValue = focusedEdit->text();
        newValue.chop(1);
        if (newValue.compare("") == 0)
            newValue = emptyPlaceholder;
        focusedEdit->setText(newValue);
        *expectingLineEdit = false;
    }
}

void MainWindow::buttonFocusClicked()
{
    if (ui->lineEditNumarator->hasFocus())
        ui->lineEditNumitor->setFocus();
    else
        ui->lineEditNumarator->setFocus();
}

void MainWindow::buttonSignClicked()
{
    bool ok = true;
    int numarator = ui->lineEditNumarator->text().toInt(&ok);
    if (!ok)
        return;
    int numitor = ui->lineEditNumitor->text().toInt(&ok);
    if (!ok)
        return;

    if (numarator == 0)
    {
        if (numitor < 0)
            ui->lineEditNumitor->setText(QString::number(-numitor));
        return;
    }

    if (numarator > 0)
    {
        if (numitor < 0)
            ui->lineEditNumitor->setText(QString::number(-numitor));
        else
            ui->lineEditNumarator->setText(QString::number(-numarator));
    }
    else
    {
        if (numitor >= 0)
            ui->lineEditNumarator->setText(QString::number(-numarator));
        else
            ui->lineEditNumitor->setText(QString::number(-numitor));
    }

    expectingNumarator = false;
    expectingNumitor = false;
}

void MainWindow::buttonInversClicked()
{
    QString numarator = ui->lineEditNumarator->text();
    QString numitor = ui->lineEditNumitor->text();
    if (numarator.compare("0") != 0)
    {
        ui->lineEditNumarator->setText(numitor);
        ui->lineEditNumitor->setText(numarator);

        expectingNumarator = false;
        expectingNumitor = false;
    }
}

void MainWindow::buttonArithmeticOpClicked()
{
    // modificare pt a te putea razgandi cand faci o operatie
    if (expectingNumarator && expectingNumitor)
    {
        calculator->setPendingOperation(buttonToOperationMap[sender()->objectName()]);
    }
    else
    // pana aici
        doArithmeticOperation(sender()->objectName());
}

void MainWindow::buttonMemoryOpClicked()
{
    try
    {
        calculator->memoryOperation(ui->lineEditNumarator->text(),
                                    ui->lineEditNumitor->text(),
                                    buttonToMemOpMap[sender()->objectName()]);
        ui->labelMemoryCache->setText(QString("M"));
    }
    catch(std::exception e)
    {
        QMessageBox msgBox;
        msgBox.setText("Operation exceeds limits");
        msgBox.setInformativeText("Memory unaffected");
        msgBox.exec();
    }
}

void MainWindow::buttonMemoryRClicked()
{
    QPair<QString, QString> result = calculator->memoryRecall();

    previousNumarator = result.first;
    previousNumitor = result.second;

    expectingNumarator = false;
    expectingNumitor = false;

    ui->lineEditNumarator->setText(result.first);
    ui->lineEditNumitor->setText(result.second);
}

void MainWindow::buttonMemoryCClicked()
{
    calculator->clearMemory();
    ui->labelMemoryCache->setText(QString(""));
}

void MainWindow::keyBoardPressed(const QString &text)
{
    if (sender()->objectName()
       .compare(ui->lineEditNumarator->objectName()) == 0
       && expectingNumarator)
    {
        ui->lineEditNumarator->setText(text.right(1));
        expectingNumarator = false;
    }

    if (sender()->objectName()
       .compare(ui->lineEditNumitor->objectName()) == 0
       && expectingNumitor)
    {
        if (text.right(1).compare("0") == 0)
        {
            ui->lineEditNumitor->setText(text.chopped(1));
        }
        else
        {
            ui->lineEditNumitor->setText(text.right(1));
            expectingNumitor = false;
        }
    }
}

void MainWindow::on_lineEditNumarator_returnPressed()
{
    doArithmeticOperation(ui->opButton_Equals->objectName());
}

void MainWindow::on_lineEditNumitor_returnPressed()
{
    doArithmeticOperation(ui->opButton_Equals->objectName());
}

void MainWindow::digitClicked(const QString & digit)
{
    QLineEdit * focusedEdit;
    QValidator *focusedEditValidator;
    bool *expectingLineEdit, isNumitor;

    if (ui->lineEditNumarator->hasFocus())
    {
        focusedEdit = ui->lineEditNumarator;
        focusedEditValidator = intRegexValidatorNumarator;
        expectingLineEdit = &expectingNumarator;
        isNumitor = false;
    }
    else if (ui->lineEditNumitor->hasFocus())
    {
        focusedEdit = ui->lineEditNumitor;
        focusedEditValidator = intRegexValidatorNumitor;
        expectingLineEdit = &expectingNumitor;
        isNumitor = true;
    }

    if (focusedEdit->text().compare("0") == 0 || *expectingLineEdit)
    {
        if (isNumitor == false || digit.compare("0") != 0)
        {
            focusedEdit->setText(digit);
            *expectingLineEdit = false;
        }
    }
    else
    {
        int pos = 0;
        QString newValue = focusedEdit->text() + digit;
        if (focusedEditValidator->validate(newValue, pos) != QValidator::Invalid)
        {
            focusedEdit->setText(newValue);
        }
    }
}

void MainWindow::doArithmeticOperation(const QString &operationStr)
{
    try
    {
    QPair<QString, QString> result = calculator->nextOperation(
                ui->lineEditNumarator->text(),
                ui->lineEditNumitor->text(),
                buttonToOperationMap[operationStr]);

    // setez booleenele pe true
    expectingNumarator = true;
    expectingNumitor = true;

    previousNumarator = result.first;
    previousNumitor = result.second;

    ui->lineEditNumarator->setText(result.first);
    ui->lineEditNumitor->setText(result.second);
    }
    catch(std::exception e)
    {
        QMessageBox msgBox;
        msgBox.setText("Operation exceeds limits");
        msgBox.setInformativeText("Showing previous fraction or partial result.");
        msgBox.exec();
        ui->lineEditNumarator->setText(previousNumarator);
        ui->lineEditNumitor->setText(previousNumitor);
    }
}

void MainWindow::initData()
{
    calculator = new Calculator(this);

    previousNumarator = "0";
    previousNumitor = "1";


    buttonToDigitMap.insert(ui->digitButton_0->objectName(), "0");
    buttonToDigitMap.insert(ui->digitButton_1->objectName(), "1");
    buttonToDigitMap.insert(ui->digitButton_2->objectName(), "2");
    buttonToDigitMap.insert(ui->digitButton_3->objectName(), "3");
    buttonToDigitMap.insert(ui->digitButton_4->objectName(), "4");
    buttonToDigitMap.insert(ui->digitButton_5->objectName(), "5");
    buttonToDigitMap.insert(ui->digitButton_6->objectName(), "6");
    buttonToDigitMap.insert(ui->digitButton_7->objectName(), "7");
    buttonToDigitMap.insert(ui->digitButton_8->objectName(), "8");
    buttonToDigitMap.insert(ui->digitButton_9->objectName(), "9");

    buttonToOperationMap.insert(ui->opButton_Addition->objectName(), Calculator::ADDITION);
    buttonToOperationMap.insert(ui->opButton_Subtraction->objectName(), Calculator::SUBTRACTION);
    buttonToOperationMap.insert(ui->opButton_Multiplication->objectName(), Calculator::MULTIPLICATION);
    buttonToOperationMap.insert(ui->opButton_Division->objectName(), Calculator::DIVISION);
    buttonToOperationMap.insert(ui->opButton_Equals->objectName(), Calculator::NONE);

    buttonToMemOpMap.insert(ui->memoryButton_Add->objectName(), Calculator::M_ADDITION);
    buttonToMemOpMap.insert(ui->memoryButton_Subtract->objectName(), Calculator::M_SUBTRACTION);

    // INITIALIZAREA EXPRESIILOR REGULATE
    intRegexPatternNumarator =      "0|";
    intRegexPatternNumarator.append("-?[1-9]\\d{0,8}|");
    intRegexPatternNumarator.append("-?1\\d{9}|");
    intRegexPatternNumarator.append("-?20\\d{8}|");
    intRegexPatternNumarator.append("-?21[0-3]\\d{7}|");
    intRegexPatternNumarator.append("-?214[0-6]\\d{6}|");
    intRegexPatternNumarator.append("-?2147[0-3]\\d{5}|");
    intRegexPatternNumarator.append("-?21474[0-7]\\d{4}|");
    intRegexPatternNumarator.append("-?214748[012]\\d{4}|");
    intRegexPatternNumarator.append("-?2147483[0-5]\\d{3}|");
    intRegexPatternNumarator.append("-?21474836[0-3]\\d{2}|");
    intRegexPatternNumarator.append("214748364[0-7]|");
    intRegexPatternNumarator.append("-214748364[0-7]");

    intRegexPatternNumitor = intRegexPatternNumarator.mid(2);
}

void MainWindow::conectareaButoanelor()
{
    // CONECTAREA BUTOANELOR 0-9
    connect(ui->digitButton_1, &QPushButton::clicked, this, &MainWindow::digitButtonClicked);
    connect(ui->digitButton_2, &QPushButton::clicked, this, &MainWindow::digitButtonClicked);
    connect(ui->digitButton_3, &QPushButton::clicked, this, &MainWindow::digitButtonClicked);
    connect(ui->digitButton_4, &QPushButton::clicked, this, &MainWindow::digitButtonClicked);
    connect(ui->digitButton_5, &QPushButton::clicked, this, &MainWindow::digitButtonClicked);
    connect(ui->digitButton_6, &QPushButton::clicked, this, &MainWindow::digitButtonClicked);
    connect(ui->digitButton_7, &QPushButton::clicked, this, &MainWindow::digitButtonClicked);
    connect(ui->digitButton_8, &QPushButton::clicked, this, &MainWindow::digitButtonClicked);
    connect(ui->digitButton_9, &QPushButton::clicked, this, &MainWindow::digitButtonClicked);
    connect(ui->digitButton_0, &QPushButton::clicked, this, &MainWindow::digitButtonClicked);

    // CONECTAREA BUTONULUI CE
    connect(ui->pushButton_CE, &QPushButton::clicked, this, &MainWindow::buttonCEClicked);

    // CONECTAREA BUTONULUI C
    connect(ui->pushButton_C, &QPushButton::clicked, this, &MainWindow::buttonCClicked);

    // CONECTAREA BUTONULUI BACKSPACE
    connect(ui->pushButton_BackSpace, &QPushButton::clicked, this, &MainWindow::buttonBackspaceClicked);

    // CONECTAREA BUTONULUI FOCUS
    connect(ui->pushButton_Focus, &QPushButton::clicked, this, &MainWindow::buttonFocusClicked);

    // CONECTAREA BUTONULUI +/-
    connect(ui->pushButton_Sign, &QPushButton::clicked, this, &MainWindow::buttonSignClicked);

    // CONECTAREA BUTONULUI 1/x
    connect(ui->pushButton_Invers, &QPushButton::clicked, this, &MainWindow::buttonInversClicked);

    // CONECTAREA BUTOANELOR ARITMETICE SI =
    connect(ui->opButton_Addition, &QPushButton::clicked, this, &MainWindow::buttonArithmeticOpClicked);
    connect(ui->opButton_Subtraction, &QPushButton::clicked, this, &MainWindow::buttonArithmeticOpClicked);
    connect(ui->opButton_Multiplication, &QPushButton::clicked, this, &MainWindow::buttonArithmeticOpClicked);
    connect(ui->opButton_Division, &QPushButton::clicked, this, &MainWindow::buttonArithmeticOpClicked);
    connect(ui->opButton_Equals, &QPushButton::clicked, this, &MainWindow::buttonArithmeticOpClicked);

    // CONECTAREA BUTOANELOR DE MEMORIE
    connect(ui->memoryButton_Add, &QPushButton::clicked, this, &MainWindow::buttonMemoryOpClicked);
    connect(ui->memoryButton_Subtract, &QPushButton::clicked, this, &MainWindow::buttonMemoryOpClicked);
    connect(ui->memoryButton_Recall, &QPushButton::clicked, this, &MainWindow::buttonMemoryRClicked);
    connect(ui->memoryButton_Clear, &QPushButton::clicked, this, &MainWindow::buttonMemoryCClicked);

}

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    createMenu();
	connectMenu();

	connectGame();

    ui->buttonNewGame->setFixedSize(QSize(30, 30));
    ui->buttonNewGame->setIconSize(QSize(30, 30));

	game.NewGame(9, 9, 12, GameController::Difficulty::EASY); // easy game
}

MainWindow::~MainWindow()
{
    deleteGrid();
    delete ui;
}

void MainWindow::shrink()
{
	resize(0, 0);
}

void MainWindow::customGameSelected()
{
	if (bombsSpinBox->value() < linesSpinBox->value() * columnsSpinBox->value())
	{
		ui->menuFile->close();
		game.NewGame(linesSpinBox->value(), columnsSpinBox->value(), bombsSpinBox->value(), GameController::Difficulty::CUSTOM);

		QApplication::processEvents();
		QTimer::singleShot(50, this, &MainWindow::shrink);
		return;
	}
	
	QMessageBox msgBox;
	msgBox.setText("Numarul de bombe trebuie sa fie mai mic decat produsul liniilor si al coloanelor!");
	msgBox.exec();
}

void MainWindow::tileChanged(int i, int j)
{
	buttons[i * game.getColumns() + j]->setIcon(QIcon(QPixmap(game.getTileIcon(i, j))));
}

void MainWindow::gridChanged()
{
	updateGrid();
	QApplication::processEvents();
	QTimer::singleShot(50, this, &MainWindow::shrink);
}

void MainWindow::nbBombsRemainingChanged(int newValue)
{
	ui->labelNbBombs->setText(QString::number(newValue));
}

void MainWindow::timeChanged(int newValue)
{
	ui->labelTimer->setText(QString::number(newValue));
}

void MainWindow::smileFaceChanged(QString newIcon)
{
	ui->buttonNewGame->setIcon(QIcon(QPixmap(newIcon)));
}

void MainWindow::showHighScoresWindow()
{
	QMessageBox msgBox;
	msgBox.setText(
		QString("Easy:         %1\nMedium:  %2\nHard:        %3")
		.arg(game.getEasyHighScore(), 4)
		.arg(game.getMediumHighScore(), 4)
		.arg(game.getHardHighScore(), 4)
	);
	msgBox.exec();
}

void MainWindow::createMenu()
{
    QWidget *gridLayoutWidget = new QWidget(ui->menuCustom_Game);
    gridLayoutWidget->setGeometry(QRect(0, 0, 200, 100));

    QGridLayout *gridLayout = new QGridLayout(gridLayoutWidget);

    QLabel *labelLinii = new QLabel("Lines", gridLayoutWidget);
    gridLayout->addWidget(labelLinii, 0, 0, 1, 1);

    QLabel *labelColoane = new QLabel("Columns", gridLayoutWidget);
    gridLayout->addWidget(labelColoane, 1, 0, 1, 1);

    QLabel *labelBombe = new QLabel("Bombs", gridLayoutWidget);
    gridLayout->addWidget(labelBombe, 2, 0, 1, 1);

    linesSpinBox = new QSpinBox(gridLayoutWidget);
    linesSpinBox->setMinimum(2);
    linesSpinBox->setMaximum(20);
    gridLayout->addWidget(linesSpinBox, 0, 1, 1, 1);

    columnsSpinBox = new QSpinBox(gridLayoutWidget);
    columnsSpinBox->setMinimum(2);
    columnsSpinBox->setMaximum(30);
    gridLayout->addWidget(columnsSpinBox, 1, 1, 1, 1);

    bombsSpinBox = new QSpinBox(gridLayoutWidget);
    bombsSpinBox->setMinimum(1);
    bombsSpinBox->setMaximum(400);
    gridLayout->addWidget(bombsSpinBox, 2, 1, 1, 1);

    okButton = new QPushButton(gridLayoutWidget);
    okButton->setText("Ok");
    gridLayout->addWidget(okButton, 3, 0, 1, 2);
	
    QWidgetAction *qa1 = new QWidgetAction(ui->menuCustom_Game);
    qa1->setDefaultWidget(gridLayoutWidget);
    ui->menuCustom_Game->addAction(qa1);
    ui->menuCustom_Game->removeAction(ui->actioncustom);
}

void MainWindow::connectMenu()
{
	connect(ui->actionNew_Game, &QAction::triggered, &game, QOverload<>::of(&GameController::NewGame));
	
	void (GameController::*newGame_fPtr)(int, int, int, GameController::Difficulty) = &GameController::NewGame;
	std::function<void()> easyGameFunctor = std::bind(newGame_fPtr, &game, 9, 9, 12, GameController::Difficulty::EASY);
	std::function<void()> mediumGameFunctor = std::bind(newGame_fPtr, &game, 16, 16, 40, GameController::Difficulty::MEDIUM);
	std::function<void()> hardGameFunctor = std::bind(newGame_fPtr, &game, 16, 30, 99, GameController::Difficulty::HARD);

	connect(ui->actionEasy_Game, &QAction::triggered, easyGameFunctor);
	connect(ui->actionMedium_Game, &QAction::triggered, mediumGameFunctor);
	connect(ui->actionHard_Game, &QAction::triggered, hardGameFunctor);

	connect(okButton, &QPushButton::clicked, this, &MainWindow::customGameSelected);

	connect(ui->actionHighscores, &QAction::triggered, this, &MainWindow::showHighScoresWindow);

	connect(ui->actionExit, &QAction::triggered, &QApplication::quit);
}

void MainWindow::connectGame()
{
	connect(&game, &GameController::updateTile, this, &MainWindow::tileChanged);
	connect(&game, &GameController::updateGrid, this, &MainWindow::gridChanged);
	connect(&game, &GameController::updateNbBombsRemaining, this, &MainWindow::nbBombsRemainingChanged);
	connect(&game, &GameController::updateTime, this, &MainWindow::timeChanged);
	connect(&game, &GameController::updateSmileyFace, this, &MainWindow::smileFaceChanged);

	connect(ui->buttonNewGame, &QPushButton::clicked, &game, QOverload<>::of(&GameController::NewGame));
}

void MainWindow::createGrid(int lines, int columns)
{
    for (int i = 0; i < lines; ++i)
        for (int j = 0; j < columns; ++j)
        {
            MyPushButton *button = new MyPushButton();
            button->setFixedSize(QSize(30, 30));
            button->setIconSize(QSize(30, 30));
            buttons.append(button);

			std::function<void()> leftClickFunctor = std::bind(&GameController::TileLeftClicked, &game, i, j);
			connect(button, &MyPushButton::clicked, this, leftClickFunctor);

			std::function<void()> rightClickFunctor = std::bind(&GameController::TileRightClicked, &game, i, j);
			connect(button, &MyPushButton::rightClicked, this, rightClickFunctor);

			std::function<void()> doubleClickFunctor = std::bind(&GameController::TileDoubleClicked, &game, i, j);
			connect(button, &MyPushButton::doubleClicked, this, doubleClickFunctor);

            ui->gridLayoutGame->addWidget(button, i, j, 1, 1);
        }
}

void MainWindow::updateGrid()
{
	deleteGrid();
	createGrid(game.getLines(), game.getColumns());

	for (int i = 0, lines = game.getLines(); i < lines; ++i)
		for (int j = 0, columns = game.getColumns(); j < columns; ++j)
			buttons[i * columns + j]->setIcon(QIcon(QPixmap(game.getTileIcon(i, j))));
}

void MainWindow::deleteGrid()
{
    for (auto& button : buttons)
        button->deleteLater();
    buttons.clear();
}

#include "gamecontroller.h"


GameController::GameController(QObject *parent)
    : QObject(parent), gen(rd())
{
	timer.setInterval(1000);
	connect(&timer, &QTimer::timeout, this, &GameController::timerIntervalFinished);
	readFile();

	numberIcons.append(QString(":/images/tileEmpty"));
	numberIcons.append(QString(":/images/tile1"));
	numberIcons.append(QString(":/images/tile2"));
	numberIcons.append(QString(":/images/tile3"));
	numberIcons.append(QString(":/images/tile4"));
	numberIcons.append(QString(":/images/tile5"));
	numberIcons.append(QString(":/images/tile6"));
	numberIcons.append(QString(":/images/tile7"));
	numberIcons.append(QString(":/images/tile8"));
}

GameController::~GameController()
{
	writeFile();
}

int GameController::getLines() const
{
	return lines;
}

int GameController::getColumns() const
{
	return columns;
}

QString GameController::getTileIcon(int i, int j) const
{
	if (i > -1 && i < tiles.size() && j > -1 && j < tiles[i].size())
		return tiles[i][j].iconString;
	return QString();
}

int GameController::getEasyHighScore() const
{
	return easyHighScore;
}

int GameController::getMediumHighScore() const
{
	return mediumHighScore;
}

int GameController::getHardHighScore() const
{
	return hardHighScore;
}

void GameController::NewGame()
{
	isWon = isLost = false;
	remainingBombs = initialNbBombs;

	for (auto& line : tiles)
		line.clear();
	tiles.clear();

	tiles.resize(lines);
	for (auto& line : tiles)
	{
		line.resize(columns);
		for (auto& tile : line)
			tile.iconString = tileUnturnedIcon;
	}

	// populating with bombs
	for (int i = 0, linie, coloana; i < remainingBombs; ++i)
		do
		{
			linie = static_cast<int>(gen() % lines);
			coloana = static_cast<int>(gen() % columns);
			if (tiles[linie][coloana].isBomb == false)
			{
				tiles[linie][coloana].isBomb = true;

				incrementNeighbouringBombs(linie - 1, coloana - 1);
				incrementNeighbouringBombs(linie - 1, coloana);
				incrementNeighbouringBombs(linie - 1, coloana + 1);

				incrementNeighbouringBombs(linie, coloana - 1);
				incrementNeighbouringBombs(linie, coloana + 1);

				incrementNeighbouringBombs(linie + 1, coloana - 1);
				incrementNeighbouringBombs(linie + 1, coloana);
				incrementNeighbouringBombs(linie + 1, coloana + 1);
				break;
			}
		} while (true);

	emit updateGrid();
	emit updateNbBombsRemaining(remainingBombs);
	currentTime = 0;
	emit updateTime(currentTime);
	emit updateSmileyFace(faceSmileIcon);
	timer.start();
}

void GameController::NewGame(int nbLines, int nbColumns, int bombs, Difficulty difficulty)
{
	lines = nbLines;
	columns = nbColumns; 
	initialNbBombs = bombs;
	if (difficulty != Difficulty::NO_CHANGE)
		gameDifficulty = difficulty;

	NewGame();
}

void GameController::TileLeftClicked(int i, int j)
{
	auto& tile = tiles[i][j];
	if (isWon == false && isLost == false && tile.isTurned == false)
	{

		if (tile.isFlagged)
		{
			tile.isFlagged = false;
			tile.iconString = tileUnturnedIcon;
			emit updateNbBombsRemaining(++remainingBombs);
			emit updateTile(i, j);
		}
		else
		{
			if (tile.isBomb)
			{
				gameLost(i, j);
			}
			else
			{
				if (tile.neighouringBombs == 0)
					floodFill(i, j);
				else
					turnTile(i, j);

				if (checkWin())
					gameWon();
			}
		}
	}
}

void GameController::TileRightClicked(int i, int j)
{
	if (isWon == false && isLost == false && tiles[i][j].isTurned == false)
	{
		if (tiles[i][j].isFlagged)
		{
			tiles[i][j].isFlagged = false;
			tiles[i][j].iconString = tileUnturnedIcon;
			emit updateNbBombsRemaining(++remainingBombs);
		}
		else
		{
			if (remainingBombs != 0)
			{
				tiles[i][j].isFlagged = true;
				tiles[i][j].iconString = tileFlaggedIcon;
				emit updateNbBombsRemaining(--remainingBombs);
				if (remainingBombs == 0 && checkWin())
					gameWon();
			}
		}
		emit updateTile(i, j);
	}
}

void GameController::TileDoubleClicked(int i, int j)
{
	if (isWon == false && isLost == false && tiles[i][j].isTurned)
	{
		try
		{
			checkNeighbourForDoubleClick(i - 1, j - 1);
			checkNeighbourForDoubleClick(i - 1, j);
			checkNeighbourForDoubleClick(i - 1, j + 1);

			checkNeighbourForDoubleClick(i, j - 1);
			checkNeighbourForDoubleClick(i, j + 1);

			checkNeighbourForDoubleClick(i + 1, j - 1);
			checkNeighbourForDoubleClick(i + 1, j);
			checkNeighbourForDoubleClick(i + 1, j + 1);

			if (checkWin())
				gameWon();
		}
		catch (const std::exception &) {}
	}
}

void GameController::timerIntervalFinished()
{
	emit updateTime(++currentTime);
}

bool GameController::checkWin()
{
	// daca toate celulele fara bomba sunt deschise
	// daca toate bombele au flag
	bool oneTileUnturned = false;
	bool oneUnflaggedBomb = false;

	for (auto &line : tiles)
	{
		for (auto& tile : line)
			if (tile.isBomb == false && tile.isTurned == false)
			{
				oneTileUnturned = true;
				break;
			}
		if (oneTileUnturned)
			break;
	}
	
	for (auto &line : tiles)
	{
		for (auto& tile : line)
			if (tile.isBomb && tile.isFlagged == false)
			{
				oneUnflaggedBomb = true;
				break;
			}
		if (oneUnflaggedBomb)
			break;
	}
	return !(oneTileUnturned && oneUnflaggedBomb);
}

void GameController::gameWon()
{
	isWon = true;
	timer.stop();
	emit updateSmileyFace(faceCoolIcon);

	// daca e highscore de salvat in fisier
	switch (gameDifficulty)
	{
	case GameController::Difficulty::EASY:
		easyHighScore = std::max(easyHighScore, static_cast<int>(currentTime));
		break;
	case GameController::Difficulty::MEDIUM:
		mediumHighScore = std::max(mediumHighScore, static_cast<int>(currentTime));
		break;
	case GameController::Difficulty::HARD:
		hardHighScore = std::max(hardHighScore, static_cast<int>(currentTime));
		break;
	default:
		break;
	}
}

void GameController::gameLost(int bombI, int bombJ)
{
	isLost = true;
	timer.stop();
	for (int i = 0; i < lines; ++i)
		for (int j = 0; j < columns; ++j)
		{
			if (tiles[i][j].isFlagged && tiles[i][j].isBomb == false)
				tiles[i][j].iconString = bombXIcon;
			else if (tiles[i][j].isBomb && tiles[i][j].isFlagged == false)
				tiles[i][j].iconString = bombIcon;
			emit updateTile(i, j);
		}
	tiles[bombI][bombJ].iconString = bombRedIcon;
	emit updateTile(bombI, bombJ);
	emit updateSmileyFace(faceSadIcon);
}

void GameController::turnTile(int i, int j)
{
	auto& tile = tiles[i][j];
	tile.isTurned = true;
	if (tile.isFlagged)
	{
		tiles[i][j].isFlagged = false;
		emit updateNbBombsRemaining(++remainingBombs);
	}
	tile.iconString = numberIcons[tile.neighouringBombs];
	emit updateTile(i, j);
}

void GameController::floodFill(int starti, int startj)
{
	std::deque<std::tuple<Tile&, int, int>> coada;
	coada.push_back(std::make_tuple(std::ref(tiles[starti][startj]), starti, startj));

	while (coada.empty() == false)
	{
		auto [currentTile, i, j] = coada.front();
		coada.pop_front();
		currentTile.iconString = numberIcons[0];
		currentTile.isTurned = true;
		if (currentTile.isFlagged)
		{
			currentTile.isFlagged = false;
			emit updateNbBombsRemaining(++remainingBombs);
		}
		emit updateTile(i, j);

		floodFillCheckNeighbour(coada, i - 1, j - 1);
		floodFillCheckNeighbour(coada, i - 1, j);
		floodFillCheckNeighbour(coada, i - 1, j + 1);

		floodFillCheckNeighbour(coada, i, j - 1);
		floodFillCheckNeighbour(coada, i, j + 1);

		floodFillCheckNeighbour(coada, i + 1, j - 1);
		floodFillCheckNeighbour(coada, i + 1, j);
		floodFillCheckNeighbour(coada, i + 1, j + 1);
	}
}

bool GameController::tileNotInQueue(const std::deque<std::tuple<Tile&, int, int>> &coada, const std::tuple<Tile&, int, int> &tileToInsert)
{
	return std::none_of(
				coada.begin(),
				coada.end(),
				std::bind(
					[](const std::tuple<Tile&, int, int> &tile, const std::tuple<Tile&, int, int> &tileToInsert)
					{
						return std::get<1>(tile) == std::get<1>(tileToInsert) && std::get<2>(tile) == std::get<2>(tileToInsert);
					},
					std::placeholders::_1,
					tileToInsert
				)
			);
}

void GameController::floodFillCheckNeighbour(std::deque<std::tuple<Tile&, int, int>> &coada, int tileI, int tileJ)
{
	if (tileI >= 0 && tileI < lines && tileJ >= 0 && tileJ < columns)
		if (tiles[tileI][tileJ].isTurned == false)
		{
			if (auto &&t = std::make_tuple(std::ref(tiles[tileI][tileJ]), tileI, tileJ);
			tiles[tileI][tileJ].neighouringBombs == 0 && tileNotInQueue(coada, t))
				coada.emplace_back(t);
			else
				turnTile(tileI, tileJ);
		}
}

void GameController::readFile()
{
	std::ifstream in;
	in.exceptions(std::ifstream::failbit);
	try
	{
		in.open("highscores.ini");
		
		std::string easyLine;
		in >> easyLine;
		easyHighScore = QString::fromStdString(easyLine).split("=")[1].toInt();
		
		std::string mediumLine;
		in >> mediumLine;
		mediumHighScore = QString::fromStdString(mediumLine).split("=")[1].toInt();

		std::string hardLine;
		in >> hardLine;
		hardHighScore = QString::fromStdString(hardLine).split("=")[1].toInt();

		in.close();
	}
	catch (const std::ifstream::failure&)
	{
		writeFile();
	}
}

void GameController::writeFile()
{
	std::ofstream out("highscores.ini");
	out << "easy=" << easyHighScore << std::endl;
	out << "medium=" << mediumHighScore << std::endl;
	out << "hard=" << hardHighScore;
	out.close();
}

void GameController::incrementNeighbouringBombs(int tileI, int tileJ)
{
	if (tileI >= 0 && tileI < lines && tileJ >= 0 && tileJ < columns)
		tiles[tileI][tileJ].neighouringBombs += 1;
}

void GameController::checkNeighbourForDoubleClick(int tileI, int tileJ)
{
	if (tileI >= 0 && tileI < lines && tileJ >= 0 && tileJ < columns)
		if (tiles[tileI][tileJ].isTurned == false && tiles[tileI][tileJ].isFlagged == false)
		{
			if (tiles[tileI][tileJ].isBomb)
			{
				gameLost(tileI, tileJ);
				throw std::exception();
			}
			else
			{
				if (tiles[tileI][tileJ].neighouringBombs == 0)
					floodFill(tileI, tileJ);
				else
					turnTile(tileI, tileJ);
			}
		}
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>
#include <QWidgetAction>
#include <QList>
#include <QMessageBox>

#include "mypushbutton.h"
#include "gamecontroller.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
	void shrink();
	void customGameSelected();

	void tileChanged(int i, int j);
	void gridChanged();
	void nbBombsRemainingChanged(int newValue);
	void timeChanged(int newValue);
	void smileFaceChanged(QString newIcon);

	void showHighScoresWindow();

private:
    void createMenu();
	void connectMenu();

	void connectGame();

    void createGrid(int lines, int columns);
	void updateGrid();
    void deleteGrid();



    Ui::MainWindow *ui;

    QSpinBox *linesSpinBox = nullptr;
    QSpinBox *columnsSpinBox = nullptr;
    QSpinBox *bombsSpinBox = nullptr;
    QPushButton *okButton = nullptr;

    QList<MyPushButton*> buttons;
	GameController game;
};

#endif // MAINWINDOW_H

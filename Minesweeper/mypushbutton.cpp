#include "mypushbutton.h"

MyPushButton::MyPushButton(QWidget * parent)
	: QPushButton(parent)
{
}

void MyPushButton::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
		emit clicked();
	else if (event->button() == Qt::RightButton)
		emit rightClicked();
}

void MyPushButton::mouseDoubleClickEvent(QMouseEvent * event)
{
	if (event->button() == Qt::LeftButton)
		emit doubleClicked();
}
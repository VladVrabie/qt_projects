#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <QObject>
#include <QString>
#include <QTimer>
#include <QVector>

#include <random>
#include <deque>
#include <tuple>
#include <functional>
#include <fstream>
#include <string>
#include <exception>

class GameController : public QObject
{
	Q_OBJECT

		struct Tile
	{
		bool isBomb = false;
		bool isTurned = false;
		bool isFlagged = false;
		unsigned neighouringBombs = 0u;
		QString iconString;
	};

public:
	enum class Difficulty
	{
		EASY,
		MEDIUM,
		HARD,
		CUSTOM,
		NO_CHANGE
	};

	explicit GameController(QObject *parent = nullptr);
	~GameController();

	int getLines() const;
	int getColumns() const;
	QString getTileIcon(int i, int j) const;

	int getEasyHighScore() const;
	int getMediumHighScore() const;
	int getHardHighScore() const;

signals:
	void updateTile(int i, int j);
	void updateGrid();
	void updateNbBombsRemaining(int value);
	void updateTime(int time);
	void updateSmileyFace(QString iconString);

public slots:
	void NewGame();
	void NewGame(int nbLines, int nbColumns, int bombs, Difficulty difficulty = Difficulty::NO_CHANGE);

	void TileLeftClicked(int i, int j);
	void TileRightClicked(int i, int j);
	void TileDoubleClicked(int i, int j);

private slots:
	void timerIntervalFinished();

private:
	bool checkWin();
	void gameWon();
	void gameLost(int i, int j);

	void turnTile(int i, int j);
	void floodFill(int i, int j);
	bool tileNotInQueue(const std::deque<std::tuple<Tile&, int, int>> &coada, const std::tuple<Tile&, int, int> &tileToInsert);
	void floodFillCheckNeighbour(std::deque<std::tuple<Tile&, int, int>> &coada, int tileI, int tileJ);

	void readFile();
	void writeFile();

	void incrementNeighbouringBombs(int tileI, int tileJ);
	void checkNeighbourForDoubleClick(int tileI, int tileJ);

	int lines = 0;
	int columns = 0;
	QTimer timer;
	unsigned currentTime = 0u;
	int initialNbBombs = 0;
	int remainingBombs = 0;
	QVector<QVector<Tile>> tiles;

	QVector<QString> numberIcons;

	bool isWon = false;
	bool isLost = false;

	int easyHighScore = 0;
	int mediumHighScore = 0;
	int hardHighScore = 0;
	Difficulty gameDifficulty = Difficulty::EASY;

	std::random_device rd;
	std::mt19937 gen;

	const QString tileUnturnedIcon = QString(":/images/tileUnturned");
	const QString tileFlaggedIcon = QString(":/images/flag");

	const QString bombRedIcon = QString(":/images/bombRed");
	const QString bombIcon = QString(":/images/bomb");
	const QString bombXIcon = QString(":/images/bombX");

	const QString faceSmileIcon = QString(":/images/faceSmile");
	const QString faceSadIcon = QString(":/images/faceSad");
	const QString faceCoolIcon = QString(":/images/faceCool");

};

#endif // GAMECONTROLLER_H

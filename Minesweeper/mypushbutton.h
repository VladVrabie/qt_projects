#ifndef MYPUSHBUTTON_H
#define MYPUSHBUTTON_H

#include <QPushButton>
#include <QMouseEvent>

class MyPushButton : public QPushButton
{
	Q_OBJECT

public:
    explicit MyPushButton(QWidget *parent = nullptr);
	virtual ~MyPushButton() = default;

protected:
	virtual void mouseReleaseEvent(QMouseEvent *event) override;
	virtual void mouseDoubleClickEvent(QMouseEvent *event) override;

signals:
	void rightClicked();
	void doubleClicked();
};

#endif // MYPUSHBUTTON_H
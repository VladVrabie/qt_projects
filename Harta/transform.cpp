#include "transform.h"
#include <QtMath>


Transform & Transform::aplicaTransformare(const QVector<QPointF> &base, QVector<QPointF> & result)
{
    int size = base.size();
    for (int i = 0; i <size; ++i)
    {
        QGenericMatrix<1, 3, double> pointMatrix;
        pointMatrix(0, 0) = base[i].x();
        pointMatrix(1, 0) = base[i].y();
        pointMatrix(2, 0) = 1.0f;

        pointMatrix = transformMatrix * pointMatrix;

        result[i].setX(pointMatrix(0,0));
        result[i].setY(pointMatrix(1, 0));
    }
    return *this;
}

Transform & Transform::translatie(double dx, double dy)
{
    QGenericMatrix<3, 3, double> tMatrix;
    tMatrix(0, 2) = dx;
    tMatrix(1, 2) = dy;
    transformMatrix = tMatrix * transformMatrix;
    return *this;
}

Transform & Transform::scalarePunct(const QPointF &p, double sx, double sy)
{
    translatie(-p.x(), -p.y());
    scalareOrig(sx, sy);
    translatie(p.x(), p.y());
    return *this;
}

Transform & Transform::scalareOrig(double sx, double sy)
{
    QGenericMatrix<3, 3, double> sMatrix;
    sMatrix(0, 0) = sx;
    sMatrix(1, 1) = sy;
    transformMatrix = sMatrix * transformMatrix;
    return *this;
}

Transform & Transform::rotatiePunct(const QPointF &p, double alpha)
{
    translatie(-p.x(), -p.y());
    rotatieOrig(alpha);
    translatie(p.x(), p.y());
    return *this;
}

Transform & Transform::rotatieOrig(double alpha)
{
    double sinAlpha = qSin(alpha);
    double cosAlpha = qCos(alpha);

    QGenericMatrix<3, 3, double> rMatrix;
    rMatrix(0, 0) = rMatrix(1, 1) = cosAlpha;
    rMatrix(0, 1) = -sinAlpha;
    rMatrix(1, 0) = sinAlpha;
    transformMatrix = rMatrix * transformMatrix;
    return *this;
}

Transform & Transform::simetrieDreapta(double xA, double yA, double xB, double yB)
{
    double angle = atan2(yB - yA, xB - xA);
    translatie(-xA, -yA);
    rotatieOrig(-angle);
    scalareOrig(1.0, -1.0);
    rotatieOrig(angle);
    translatie(xA, yA);
    return *this;
}

Transform & Transform::toIdentity()
{
    transformMatrix.setToIdentity();
    return *this;
}

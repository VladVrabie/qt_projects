#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include <deque>
#include <memory>
#include "Graf/graph.h"

class Dijkstra
{
	struct _Node
	{
		_Node() = default;
		~_Node() = default;
		_Node& operator= (const _Node& other) = default;
		
		int id = -1;
		unsigned long distance = ULONG_MAX;
		int predecessor = -1;
		bool visited = false;
		bool destination = false;
	};


public:
    Dijkstra(Graph *graph = nullptr);
	~Dijkstra() = default;

	void setGraph(Graph *graph);

	auto compute(unsigned startID, unsigned stopID) ->std::pair<std::unique_ptr<std::deque<int>>, unsigned long>;

private:
	Graph *_graph;
};

#endif // DIJKSTRA_H

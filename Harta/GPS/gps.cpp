#include "GPS/gps.h"

GPS::GPS(Graph * graph)
	: _graph{ graph }, _dijkstra{ graph }
{
}

void GPS::setGraph(Graph * graph)
{
	_graph = graph;
	_dijkstra.setGraph(graph);
}

bool GPS::isStartSet() const
{
	return _startID != UINT32_MAX;
}

bool GPS::isStopSet() const
{
	return _stopID != UINT32_MAX;
}

unsigned GPS::startID()
{
	return _startID;
}

unsigned GPS::stopID()
{
	return _stopID;
}

void GPS::setStartId(unsigned newStartId)
{
	_startID = newStartId;
}

void GPS::setStopId(unsigned newStopId)
{
	_stopID = newStopId;
}

auto GPS::applyDijkstra() -> pathDistancePair
{
	return std::move(_dijkstra.compute(_startID, _stopID));
}

#include "GPS/dijkstra.h"

Dijkstra::Dijkstra(Graph * graph)
	: _graph{ graph }
{
}

void Dijkstra::setGraph(Graph * graph)
{
	_graph = graph;
}

// Returns an empty deque and 0 if there is no path between them
auto Dijkstra::compute(unsigned startID, unsigned stopID) -> std::pair<std::unique_ptr<std::deque<int>>, unsigned long>
{

	unsigned nbNodes = _graph->getNodes().size();

	if (!nbNodes)
		return std::make_pair(std::make_unique<std::deque<int>>(), 0UL);
	
	if (startID >= nbNodes || stopID >= nbNodes)
		throw std::exception("Dijkstra: Incorrect ids in parameters.");

	std::vector<_Node> nodes{ nbNodes };

	for (unsigned i = 0; i < nbNodes; ++i)
		nodes[i].id = i;

	// setez nodurile de start si stop
	nodes[startID].distance = 0UL;
	nodes[stopID].destination = true;


	// priority queue-ul va fi un deque facut heap manual cand se modifica valorile din noduri
	std::deque<_Node*> pQueue;

	// creez comparatorul pt pQ
	auto comparator = []( _Node * const& n1, _Node * const& n2)
	{
		return n1->distance > n2->distance;
	};

	// punem nodul de start in pQ
	pQueue.push_back(&nodes[startID]);

	while (true) 
	{
		if (pQueue.empty() == false)
		{
			// extragem nodul cu distanta minima din pQueue
			_Node &currentNode = *pQueue.front();
			pQueue.pop_front();

			currentNode.visited = true;

			if (currentNode.destination == false)
			{
				for (auto& [idSuccessor, cost] : _graph->getNodes()[currentNode.id].getSuccessors())
				{
					_Node &currentSuccessor = nodes[idSuccessor];
					if (currentSuccessor.visited == false && cost + currentNode.distance < currentSuccessor.distance)
					{
						currentSuccessor.distance = cost + currentNode.distance;
						currentSuccessor.predecessor = currentNode.id;
						pQueue.push_back(&currentSuccessor);
						std::make_heap(pQueue.begin(), pQueue.end(), comparator);
					}
				}
			}
			else
			{
				// formez deque-ul rezultat
				std::unique_ptr<std::deque<int>> result = std::make_unique<std::deque<int>>();

				int &predecessorID = currentNode.id;
				while (predecessorID != -1)
				{
					result->push_front(predecessorID);
					predecessorID = nodes[predecessorID].predecessor;
				}

				return std::make_pair(std::move(result), currentNode.distance);
			}
		}
		else
		{
			// graful e padurice si nu exista drum pana la stop
			break;
		}
	}

	return std::make_pair(std::make_unique<std::deque<int>>(), 0UL);
}

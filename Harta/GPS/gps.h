#ifndef GPS_H
#define GPS_H

#include <QPainter>

#include "GPS/dijkstra.h"

class GPS
{
public:
    GPS(Graph *graph = nullptr);
	~GPS() = default;

	void setGraph(Graph *graph);

	bool isStartSet() const;
	bool isStopSet() const;

	unsigned startID();
	unsigned stopID();

	void setStartId(unsigned newStartId);
	void setStopId(unsigned newStopId);

	typedef std::pair<std::unique_ptr<std::deque<int>>, unsigned long> pathDistancePair;
	auto applyDijkstra() ->pathDistancePair;

private:
	Dijkstra _dijkstra;

	Graph *_graph = nullptr;

	unsigned _startID = UINT32_MAX;
	unsigned _stopID = UINT32_MAX;
};

#endif // GPS_H
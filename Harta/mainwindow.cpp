#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

	connect(ui->frameHarta, &FrameHarta::startChanged,
			ui->labelStart, &QLabel::setText);
	connect(ui->frameHarta, &FrameHarta::stopChanged,
			ui->labelStop, &QLabel::setText);
	connect(ui->frameHarta, &FrameHarta::distanceChanged,
			ui->labelDistance, &QLabel::setText);

	movie = new QMovie(this);
	movie->setFileName(QStringLiteral(":/Harta/loader.gif"));
	ui->labelLoader->setMovie(movie);

	movie->start();

	connect(ui->frameHarta, &FrameHarta::setAnimation,
			this, &MainWindow::setLoadingAnimation, Qt::DirectConnection);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setLoadingAnimation(bool start)
{
	if (start)
		movie->start();
	else
		movie->stop();
}


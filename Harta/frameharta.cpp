#include "frameharta.h"


FrameHarta::FrameHarta(QWidget *parent)
    : QFrame(parent)
{
	readFromFile();

	_drawingTimer.setSingleShot(true);
	connect(&_drawingTimer, &QTimer::timeout, this, &FrameHarta::calculateAndUpdate);
}

FrameHarta::~FrameHarta()
{
	_workerThread.quit();
	_workerThread.wait();
}

void FrameHarta::readFromFile()
{
	GraphReader *gReader = new GraphReader();

	gReader->moveToThread(&_workerThread);

	connect(&_workerThread, &QThread::finished, gReader, &GraphReader::deleteLater);

	connect(this, &FrameHarta::startReading, gReader, &GraphReader::read);

	connect(gReader, &GraphReader::resultReady, this, &FrameHarta::handleReadingResult);

	_workerThread.start();

	emit startReading(QStringLiteral(":/Harta/map.xml"));
}

void FrameHarta::mousePressEvent(QMouseEvent * event)
{
	_lastPos = event->localPos();
	_pressedButton = event->button();

	if (_sGraph)
	{
		switch (_pressedButton)
		{
		case Qt::MiddleButton:
			_graphDrawer.resetMap();

			_gps.setStartId(UINT32_MAX);
			_gps.setStopId(UINT32_MAX);

			emit startChanged(QStringLiteral("Start:"));
			emit stopChanged(QStringLiteral("Stop:"));
			emit distanceChanged(QStringLiteral("Distance:"));

			update();
			break;

		case Qt::RightButton:
		{
			unsigned closestID = _graphDrawer.closestPoint(_lastPos);

			if (_gps.isStartSet() == false && _gps.isStopSet() == false)
			{
				_gps.setStartId(closestID);
				emit startChanged(QStringLiteral("Start: %L1").arg(closestID));
			}
			else if (_gps.isStartSet() == true && _gps.isStopSet() == false)
			{
				_gps.setStopId(closestID);
				emit stopChanged(QStringLiteral("Stop: %L1").arg(closestID));

				//emit setAnimation(true);
				_pathPair = _gps.applyDijkstra();
				//emit setAnimation(false);

				emit distanceChanged(QStringLiteral("Distance: %L1").arg(_pathPair.second));
			}
			else if (_gps.isStartSet() == true && _gps.isStopSet() == true)
			{
				_gps.setStartId(closestID);
				_gps.setStopId(UINT32_MAX);

				emit startChanged(QStringLiteral("Start: %L1").arg(closestID));
				emit stopChanged(QStringLiteral("Stop:"));
				emit distanceChanged(QStringLiteral("Distance:"));
			}

			update();
		}
		break;

		default:
			break;
		}
	}
}

void FrameHarta::mouseMoveEvent(QMouseEvent * event)
{
	if (_sGraph)
	{
		if (_pressedButton == Qt::LeftButton)
		{
			QPointF currentPos = event->localPos();

			QPointF delta = currentPos - _lastPos;
			_graphDrawer.translate(delta.x(), delta.y());

			_lastPos = currentPos;
			update();
		}
	}
}

void FrameHarta::mouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
	_pressedButton = Qt::NoButton;
}

void FrameHarta::wheelEvent(QWheelEvent * event)
{
	if (_sGraph)
	{
		double sx, sy;
		if (QPoint scrollValue = event->angleDelta(); scrollValue.y() > 0)
		{
			// scroll up
			sx = sy = 1.4;
		}
		else
		{
			// scroll down
			sx = sy = 0.6;
		}
		_graphDrawer.scalePoint(event->posF(), sx, sy);
		update();
	}
}

void FrameHarta::resizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
	if (_sGraph)
	{
		_graphDrawer.setHeight(height());
		_graphDrawer.setWidth(width());

		_drawingTimer.start(200);
	}
}

void FrameHarta::paintEvent(QPaintEvent * event)
{
	Q_UNUSED(event);

	if (_sGraph)
	{

		QPainter painter(this);
		if (_drawingTimer.isActive() == false)
		{
			_graphDrawer.drawGraph(painter);

			if (_gps.isStartSet())
			{
				_graphDrawer.drawCircle(painter, _gps.startID(), 12.0);
			}

			if (_gps.isStopSet())
			{
				_graphDrawer.drawCircle(painter, _gps.stopID(), 10.0);

				_graphDrawer.drawPath(painter, _pathPair.first.get());
			}

		}

	}
}

void FrameHarta::calculateAndUpdate()
{
	_graphDrawer.initDrawablePoints();
	update();
}

void FrameHarta::handleReadingResult(Graph *graph)
{
	std::shared_ptr<Graph> s{ graph };
	_sGraph = s;
	emit setAnimation(false);
	
	_graphDrawer.setGraph(_sGraph.get());
	_graphDrawer.setHeight(height());
	_graphDrawer.setWidth(width());

	_gps.setGraph(_sGraph.get());

	calculateAndUpdate();
}
#include "Graf/node.h"

Node::Node(int id)
    : _id{id}
{

}

int Node::getId() const
{
    return _id;
}

auto Node::getSuccessors() -> std::vector<std::pair<int, int>>&
{
    return _successors;
}

void Node::addSuccessor(int idSuccessor, int cost)
{
    auto newSuccessor = std::make_pair(idSuccessor, cost);
	auto comp = [&newSuccessor](const std::pair<int, int> &pair1, const std::pair<int, int> &pair2) {
		return pair1.first == pair2.first;
	};
	if (std::none_of(_successors.begin(), _successors.end(),
		std::bind(comp, std::placeholders::_1, newSuccessor)))
	{
		_successors.push_back(std::move(newSuccessor));
	}
}

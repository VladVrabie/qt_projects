#ifndef GRAPH_H
#define GRAPH_H

#include <QPointF>
#include <QVector>

#include <exception>
#include "Graf/node.h"

class Graph
{
public:
	Graph() = default;
	~Graph() = default;

	QVector<QPointF>& getInitialPoints();
	QVector<Node>& getNodes();

	void addNode(int idNode, int x, int y);
	void addArc(int idStart, int idStop, int cost);

private:
	QVector<QPointF> _initPoints;
	QVector<Node> _nodes;
};

#endif // GRAPH_H

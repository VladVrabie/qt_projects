#ifndef NODE_H
#define NODE_H

#include <vector>
#include <utility>
#include <algorithm>
#include <functional>

class Node
{
public:
    Node(int id = -1);
	~Node() = default;

    int getId() const;
    auto getSuccessors() -> std::vector<std::pair<int, int>>& ;

    void addSuccessor(int idSuccessor, int cost);

private:
    int _id;
    std::vector<std::pair<int, int>> _successors;
};

#endif // NODE_H

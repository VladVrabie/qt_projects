#include "graphdrawer.h"


void GraphDrawer::setGraph(Graph * graph)
{
	_graf = graph;
}

void GraphDrawer::setHeight(int height)
{
	_windowHeight = height;
}

void GraphDrawer::setWidth(int width)
{
	_windowWidth = width;
}

void GraphDrawer::translate(double dx, double dy)
{
	_transform.translatie(dx, dy);
}

void GraphDrawer::scalePoint(const QPointF & p, double sx, double sy)
{
	_transform.scalarePunct(p, sx, sy);
}

void GraphDrawer::resetMap()
{
	_transform.toIdentity();
}

void GraphDrawer::drawGraph(QPainter & painter)
{
	if (_graf != nullptr)
	{
		_transform.aplicaTransformare(_drawablePoints, _pointsToDraw);

		for (auto & nod : _graf->getNodes())
		{
			for (auto&[idSuccesor, cost] : nod.getSuccessors())
			{
				Q_UNUSED(cost);
				painter.drawLine(_pointsToDraw[nod.getId()], _pointsToDraw[idSuccesor]);
			}
		}

	}
}

void GraphDrawer::drawPath(QPainter & painter, std::deque<int> *path)
{
	painter.save();

	QPen pen;
	pen.setColor(Qt::red);
	pen.setWidth(3);

	painter.setPen(pen);

	unsigned size = static_cast<unsigned>(path->size());
	for (unsigned i = 1; i < size; ++i)
		painter.drawLine(_pointsToDraw[ (*path)[i] ], _pointsToDraw[ (*path)[i - 1u] ]);

	painter.restore();
}

void GraphDrawer::drawCircle(QPainter & painter, unsigned pointID, double radius)
{
	painter.save();

	QPen pen;
	pen.setColor(Qt::darkGreen);

	painter.setPen(pen);
	if (pointID < static_cast<unsigned>(_pointsToDraw.size()))
		painter.drawEllipse(_pointsToDraw[pointID], radius, radius);

	painter.restore();
}

void GraphDrawer::initDrawablePoints()
{
	if (_graf != nullptr && _windowHeight != 0 && _windowWidth != 0)
	{
		Transform transform;
		auto & initialPoints = _graf->getInitialPoints();
		_drawablePoints = initialPoints;

		_pointsToDraw.resize(_drawablePoints.size());

		// Pasul 1
		{
			QPointF *xMinPoint = std::min_element(initialPoints.begin(), initialPoints.end(),
				[](const QPointF & p1, const QPointF & p2)
			{
				return p1.x() < p2.x();
			});

			QPointF *yMinPoint = std::min_element(initialPoints.begin(), initialPoints.end(),
				[](const QPointF & p1, const QPointF & p2)
			{
				return p1.y() < p2.y();
			});

			transform.translatie(-xMinPoint->x(), -yMinPoint->y());
			transform.aplicaTransformare(initialPoints, _drawablePoints);
		}


		double _s3_xMax;
		double _s3_yMax;

		// Pasul 2
		{
			QPointF *xMaxPoint = std::max_element(_drawablePoints.begin(), _drawablePoints.end(),
				[](const QPointF & p1, const QPointF & p2)
			{
				return p1.x() < p2.x();
			});

			QPointF *yMaxPoint = std::max_element(_drawablePoints.begin(), _drawablePoints.end(),
				[](const QPointF & p1, const QPointF & p2)
			{
				return p1.y() < p2.y();
			});

			double Sx = _windowWidth / xMaxPoint->x();
			double Sy = _windowHeight / yMaxPoint->y();
			double S = std::min(Sx, Sy);

			_s3_xMax = S * xMaxPoint->x();
			_s3_yMax = S * yMaxPoint->y();

			transform.scalareOrig(S, S);
		}

		// Pasul 3
		{
			double dx = (_windowWidth - _s3_xMax) / 2.0;
			double dy = (_windowHeight - _s3_yMax) / 2.0;

			_drawablePoints = initialPoints;
			transform.translatie(dx, dy);
			transform.aplicaTransformare(initialPoints, _drawablePoints);
		}

		// Pasul 4
		{
			for (auto& point : _drawablePoints)
				point.setY(_windowHeight - point.y());
		}

	}
}

unsigned GraphDrawer::closestPoint(const QPointF & position) const
{
	auto comp = [position](const QPointF &p1, const QPointF &p2)
	{
		double diffX = p1.x() - position.x();
		double diffY = p1.y() - position.y();
		double distance1 = diffX * diffX + diffY * diffY;

		diffX = p2.x() - position.x();
		diffY = p2.y() - position.y();
		double distance2 = diffX * diffX + diffY * diffY;

		return distance1 < distance2;
	};

	int minIndex = 0;
	for (int i = 1, size = _pointsToDraw.size(); i < size; ++i)
	{
		if (comp(_pointsToDraw[i], _pointsToDraw[minIndex]))
			minIndex = i;
	}
	
	return static_cast<unsigned>(minIndex);
}

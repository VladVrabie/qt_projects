#include "Graf/graphreader.h"

GraphReader::XmlHandler::XmlHandler(Graph *graph)
	: QXmlDefaultHandler(), _graph{graph}
{
}

GraphReader::XmlHandler::~XmlHandler()
{
}

bool GraphReader::XmlHandler::startElement(const QString & namespaceURI, const QString & localName, 
										   const QString & qName, const QXmlAttributes & atts)
{
	Q_UNUSED(namespaceURI);
	Q_UNUSED(qName);

	if (localName == QStringLiteral("node"))
	{
		bool ok = true;
		
		int idNode = atts.value(QStringLiteral("id")).toInt(&ok);
		if (!ok)
			throw std::exception("Parsing Xml: Conversion to int of Node id not successful");

		int y = atts.value(QStringLiteral("longitude")).toInt(&ok);
		if (!ok)
			throw std::exception("Parsing Xml: Conversion to int of Node longitude not successful");

		int x = atts.value(QStringLiteral("latitude")).toInt(&ok);
		if (!ok)
			throw std::exception("Parsing Xml: Conversion to int of Node latitude not successful");

		_graph->addNode(idNode, x, y);
	}

	if (localName == QStringLiteral("arc"))
	{
		bool ok = true;

		int from = atts.value(QStringLiteral("from")).toInt(&ok);
		if (!ok)
			throw std::exception("Parsing Xml: Conversion to int of Arc from not successful");

		int to = atts.value(QStringLiteral("to")).toInt(&ok);
		if (!ok)
			throw std::exception("Parsing Xml: Conversion to int of Arc to not successful");
		
		int length = atts.value(QStringLiteral("length")).toInt(&ok);
		if (!ok)
			throw std::exception("Parsing Xml: Conversion to int of Arc length not successful");

		_graph->addArc(from, to, length);
	}

	return true;
}
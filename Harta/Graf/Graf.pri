HEADERS += \
    $$PWD/node.h \
    $$PWD/graph.h \
    $$PWD/graphreader.h \
    $$PWD/graphdrawer.h

SOURCES += \
    $$PWD/node.cpp \
    $$PWD/graph.cpp \
    $$PWD/graphreader.cpp \
    $$PWD/xmlhandler.cpp \
    $$PWD/graphdrawer.cpp
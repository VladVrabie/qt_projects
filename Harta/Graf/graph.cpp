#include "Graf/graph.h"


QVector<QPointF>& Graph::getInitialPoints()
{
	return _initPoints;
}

QVector<Node>& Graph::getNodes()
{
	return _nodes;
}

void Graph::addNode(int idNode, int x, int y)
{
	if (idNode != _nodes.size())
		throw std::exception("Graph class: Id-ul nodului introdus nu respecta secventialitatea datelor.");
	_nodes.push_back(std::move(Node(idNode)));
	_initPoints.push_back(std::move(QPointF(x, y)));
}

void Graph::addArc(int idStart, int idStop, int cost)
{
	_nodes[idStart].addSuccessor(idStop, cost);
}

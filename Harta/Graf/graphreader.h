#ifndef GRAPHREADER_H
#define GRAPHREADER_H

#include <QString>
#include <QFile>
#include <QXmlInputSource>
#include <QXmlDefaultHandler>
#include <QXmlSimpleReader>
#include <QXmlAttributes>

#include <memory>
#include <exception>
#include <Graf/graph.h>

class GraphReader : public QObject
{
	Q_OBJECT

	class XmlHandler : public QXmlDefaultHandler
	{
	public:
		XmlHandler(Graph *graph);
		~XmlHandler();

		virtual bool startElement(const QString &namespaceURI, const QString &localName, 
								  const QString &qName, const QXmlAttributes &atts) override;

	private:
		Graph * _graph = nullptr;
	};

public:
	GraphReader() = default;
	~GraphReader() = default;

signals:
	void resultReady(Graph *graph);

public slots:
	void read(const QString &fileName);

};

#endif // GRAPHREADER_H
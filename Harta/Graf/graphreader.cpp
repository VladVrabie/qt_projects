#include "Graf/graphreader.h"

void GraphReader::read(const QString & fileName)
{
	Graph *sGraph = new Graph();

	std::unique_ptr<QFile> file = std::make_unique<QFile>(fileName);
	std::unique_ptr<QXmlInputSource> inputSource = std::make_unique<QXmlInputSource>(file.get());

	std::unique_ptr<XmlHandler> xmlHandler = std::make_unique<XmlHandler>(sGraph);

	QXmlSimpleReader xmlReader;
	xmlReader.setContentHandler(xmlHandler.get());
	xmlReader.setErrorHandler(xmlHandler.get());

	bool ok = xmlReader.parse(inputSource.get());
	if (!ok)
		throw std::exception("GraphReader: Parse not successful.");


	emit resultReady(sGraph);
}
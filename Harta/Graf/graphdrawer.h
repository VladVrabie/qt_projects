#ifndef GRAPHDRAWER_H
#define GRAPHDRAWER_H

#include <QPainter>
#include <QPen>

#include <algorithm>
#include <deque>
#include "Graf/graph.h"
#include "transform.h"

class GraphDrawer
{
public:
	GraphDrawer() = default;
	~GraphDrawer() = default;

	void setGraph(Graph *graph);

	void setHeight(int height);
	void setWidth(int width);

	void translate(double dx, double dy);
	void scalePoint(const QPointF &p, double sx, double sy);
	void resetMap();

	void drawGraph(QPainter &painter);
	void drawPath(QPainter &painter, std::deque<int> *path);
	void drawCircle(QPainter &painter, unsigned pointID, double radius);

	void initDrawablePoints();

	unsigned closestPoint(const QPointF &position) const;

private: 
	QVector<QPointF> _drawablePoints;
	QVector<QPointF> _pointsToDraw;

	int _windowHeight = 0;
	int _windowWidth = 0;

	Transform _transform;
	Graph *_graf = nullptr;
};

#endif // GRAPHDRAWER_H
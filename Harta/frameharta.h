#ifndef FRAMEHARTA_H
#define FRAMEHARTA_H

#include <QFrame>
#include <QPainter>
#include <QTimer>
#include <QMouseEvent>
#include <QResizeEvent>
#include <QPaintEvent>
#include <QWheelEvent>
#include <QString>
#include <QThread>

#include <memory>
#include "Graf/graph.h"
#include "Graf/graphreader.h"
#include "Graf/graphdrawer.h"
#include "GPS/gps.h"

class FrameHarta : public QFrame
{
	Q_OBJECT

public:
    FrameHarta(QWidget *parent = nullptr);
	~FrameHarta();

	void readFromFile();

protected:
	virtual void mousePressEvent(QMouseEvent *event) override;
	virtual void mouseMoveEvent(QMouseEvent *event) override;
	virtual void mouseReleaseEvent(QMouseEvent *event) override;
	virtual void wheelEvent(QWheelEvent *event) override;
	virtual void resizeEvent(QResizeEvent *event) override;
	virtual void paintEvent(QPaintEvent *event) override;

signals:
	void startChanged(QString value);
	void stopChanged(QString value);
	void distanceChanged(QString value);
	void setAnimation(bool start);

	void startReading(QString fileName);

public slots :
	void calculateAndUpdate();

	void handleReadingResult(Graph *graph);


private:
	std::shared_ptr<Graph> _sGraph;
	GraphDrawer _graphDrawer;
	GPS _gps;

	GPS::pathDistancePair _pathPair;
	QTimer _drawingTimer;

	QPointF _lastPos;
	Qt::MouseButton _pressedButton = Qt::NoButton;

	QThread _workerThread;
};

#endif // FRAMEHARTA_H

#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include <any>

class Model
{
public:
	Model() = default;
	~Model() = default;

	// Element access
    std::vector<std::any>& getData();

	std::any& operator[] (int index);

	template<class T>
	T& get(int index) // sa faca *any_cast
	{
		return *std::any_cast<T>(&_data[index]);
	}

	template<class T>
	T* getP(int index) // sa faca any_cast
	{
		return std::any_cast<T>(&_data[index]);
	}

    
	
	// Iterators
	auto begin()->std::vector<std::any>::iterator;
	auto end()->std::vector<std::any>::iterator;


	// Capacity
	bool isEmpty() const;
	int size() const;


	// Modifiers
	void setData(const std::vector<std::any> & data);
    void setData(std::vector<std::any>&& data);
    void clearData();

    template<class T>
    void appendData(T&& data)
    {
        this->_data.push_back(std::make_any<T>(data));
    }


private:
    std::vector<std::any> _data;
};

#endif // MODEL_H

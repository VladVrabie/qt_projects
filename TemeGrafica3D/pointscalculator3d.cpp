#include "pointscalculator3d.h"

WireframeModel PointsCalculator3D::computeCurve(const fPtr1 & f, const fPtr1 & g, const fPtr1 & h, double start, double end, unsigned resolution)
{
	WireframeModel result;

	double resolutionStep = (end - start) / resolution;

	for (unsigned i = 0u; i < resolution; ++i)
	{
		double u = start + i * resolutionStep;
		result << QVector3D(f(u), g(u), h(u));
		result << std::make_pair(i, i + 1);
	}
	result << QVector3D(f(end), g(end), h(end));
	return result;
}

WireframeModel PointsCalculator3D::computeSurface(const fPtr2 & f, const fPtr2 & g, const fPtr2 & h, double start1, double end1, unsigned resolution1, 
													double start2, double end2, unsigned resolution2)
{
	WireframeModel result;

	double resolutionStep1 = (end1 - start1) / resolution1;
	double resolutionStep2 = (end2 - start2) / resolution2;

	for (unsigned i = 0u; i < resolution1; ++i)
	{
		double u = start1 + i * resolutionStep1;
		for (unsigned j = 0u; j < resolution2; ++j)
		{
			double v = start2 + j * resolutionStep2;
			result << QVector3D(f(u, v), g(u, v), h(u, v));
		}
		result << QVector3D(f(u, end2), g(u, end2), h(u, end2));
	}
	for (unsigned j = 0u; j < resolution2; ++j)
	{
		double v = start2 + j * resolutionStep2;
		result << QVector3D(f(end1, v), g(end1, v), h(end1, v));
	}
	result << QVector3D(f(end1, end2), g(end1, end2), h(end1, end2));

	// Adding connections
	for (unsigned i = 0u; i <= resolution1; ++i)
		for (unsigned j = 0u; j < resolution2; ++j)
			result << std::make_pair(i * (resolution2 + 1) + j, i * (resolution2 + 1) + j + 1);

	for (unsigned j = 0u; j <= resolution2; ++j)
		for (unsigned i = 0u; i < resolution1; ++i)
			result << std::make_pair(i * (resolution2 + 1) + j, (i + 1) * (resolution2 + 1) + j);

	return result;
}

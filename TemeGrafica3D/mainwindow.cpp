#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

	connect(ui->tabParametrice3D, &parametrice3d::ManagerTab::sendWireframeModelTab, ui->tabCasa3d, &casa3d::ManagerTab::receiveWireframeModel);
}

MainWindow::~MainWindow()
{
    delete ui;
}

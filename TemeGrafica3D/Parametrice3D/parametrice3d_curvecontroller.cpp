#include "Parametrice3D/parametrice3d_curvecontroller.h"

parametrice3d::CurveController::CurveController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void parametrice3d::CurveController::setHeightWidth(double newHeight, double newWidth)
{
	height = newHeight;
	width = newWidth;
}

void parametrice3d::CurveController::myMousePressEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void parametrice3d::CurveController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void parametrice3d::CurveController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void parametrice3d::CurveController::myWheelEvent(QWheelEvent * event)
{
	Q_UNUSED(event);
}

void parametrice3d::CurveController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void parametrice3d::CurveController::myResizeEvent(QResizeEvent * event)
{
	height = event->size().height();
	width = event->size().width();
}

void parametrice3d::CurveController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	auto f = _model->getP<PointsCalculator3D::fPtr1>(0);
	auto g = _model->getP<PointsCalculator3D::fPtr1>(1);
	auto h = _model->getP<PointsCalculator3D::fPtr1>(2);
	auto& start = _model->get<double>(3);
	auto& end = _model->get<double>(4);
	auto& resolution = _model->get<unsigned>(5);

	if (f && g && h && start != end) // alte verificari?
	{
		auto wm = calculator.computeCurve(*f, *g, *h, start, end, resolution);
		emit sendWireframeModel(wm);
		auto points2d = wm.perspectiveProjection(height, width, zp);

		for (auto& connection : wm.getConnections())
			painter.drawLine(
				points2d[connection.first],
				points2d[connection.second]
			);
	}
}

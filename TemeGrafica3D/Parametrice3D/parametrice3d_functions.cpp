#include "Parametrice3D/parametrice3d_functions.h"

// Curbe parametrice
const double parametrice3d::Functions::razaCurbaElice = 100.0;
const double parametrice3d::Functions::c = 100.0;
const double parametrice3d::Functions::d = -1000.0;

// Suprafete parametrice
const double parametrice3d::Functions::p = 300.0;
const double parametrice3d::Functions::q = 250.0;
const double parametrice3d::Functions::r = 350.0;
const double parametrice3d::Functions::s = -200.0;
#include "Parametrice3D/parametrice3d_managertab.h"

parametrice3d::ManagerTab::ManagerTab(QWidget *parent)
	: QWidget(parent)
{
	setModelValues(
		PointsCalculator3D::fPtr1(Functions::xCurbaElice),
		PointsCalculator3D::fPtr1(Functions::yCurbaElice),
		PointsCalculator3D::fPtr1(Functions::zCurbaElice),
		-10.0,
		10.0,
		200u,
		0.0,
		0.0,
		0u
	);
}

void parametrice3d::ManagerTab::functionSelectionChanged(int index)
{
	switch (index)
	{
	case 0:
		setModelValues(
			PointsCalculator3D::fPtr1(),
			PointsCalculator3D::fPtr1(),
			PointsCalculator3D::fPtr1(),
			0.0,
			0.0,
			0u,
			0.0,
			0.0,
			0u
		);
		frame->getSwitcher()->switchToController(id::curveController);

		setModelValues(
			PointsCalculator3D::fPtr1(Functions::xCurbaElice),
			PointsCalculator3D::fPtr1(Functions::yCurbaElice),
			PointsCalculator3D::fPtr1(Functions::zCurbaElice),
			-10.0,
			10.0,
			200u,
			0.0,
			0.0,
			0u
		);

		setViewValues(-10.0, 10.0, 200);
		break;

	case 1:
		setModelValues(
			PointsCalculator3D::fPtr1(),
			PointsCalculator3D::fPtr1(),
			PointsCalculator3D::fPtr1(),
			0.0,
			0.0,
			0u,
			0.0,
			0.0,
			0u
		);
		frame->getSwitcher()->switchToController(id::curveController);

		setModelValues(
			PointsCalculator3D::fPtr1(Functions::xSpirala3D),
			PointsCalculator3D::fPtr1(Functions::ySpirala3D),
			PointsCalculator3D::fPtr1(Functions::zSpirala3D),
			0.0,
			10.0,
			200u,
			0.0,
			0.0,
			0u
		);

		setViewValues(0.0, 10.0, 200);
		break;

	case 2:
		setModelValues(
			PointsCalculator3D::fPtr2(),
			PointsCalculator3D::fPtr2(),
			PointsCalculator3D::fPtr2(),
			0.0,
			0.0,
			0u,
			0.0,
			0.0,
			0u
		);
		frame->getSwitcher()->switchToController(id::surfaceController);
		
		setModelValues(
			PointsCalculator3D::fPtr2(Functions::xElipsoid),
			PointsCalculator3D::fPtr2(Functions::yElipsoid),
			PointsCalculator3D::fPtr2(Functions::zElipsoid),
			-M_PI_2,
			M_PI_2,
			20u,
			-M_PI,
			M_PI,
			20u
		);

		setViewValues(-M_PI_2, M_PI_2, 20, -M_PI, M_PI, 20);
		break;

	case 3:
		setModelValues(
			PointsCalculator3D::fPtr2(),
			PointsCalculator3D::fPtr2(),
			PointsCalculator3D::fPtr2(),
			0.0,
			0.0,
			0u,
			0.0,
			0.0,
			0u
		);
		frame->getSwitcher()->switchToController(id::surfaceController);

		setModelValues(
			PointsCalculator3D::fPtr2(Functions::xCofrag),
			PointsCalculator3D::fPtr2(Functions::yCofrag),
			PointsCalculator3D::fPtr2(Functions::zCofrag),
			-10.0,
			10.0,
			20u,
			-10.0,
			10.0,
			20u
		);

		setViewValues(-10.0, 10.0, 20, -10.0, 10.0, 20);
		break;

	default:
		break;
	}

	frame->update();
}

void parametrice3d::ManagerTab::start1Changed(double value)
{
	(*uModel)[3] = value;
	frame->update();
}

void parametrice3d::ManagerTab::end1Changed(double value)
{
	(*uModel)[4] = value;
	frame->update();
}

void parametrice3d::ManagerTab::resolution1Changed(int value)
{
	(*uModel)[5] = static_cast<unsigned>(value);
	frame->update();
}

void parametrice3d::ManagerTab::start2Changed(double value)
{
	(*uModel)[6] = value;
	frame->update();
}

void parametrice3d::ManagerTab::end2Changed(double value)
{
	(*uModel)[7] = value;
	frame->update();
}

void parametrice3d::ManagerTab::resolution2Changed(int value)
{
	(*uModel)[8] = static_cast<unsigned>(value);
	frame->update();
}

void parametrice3d::ManagerTab::receiveWireframeModel(WireframeModel wm)
{
	emit sendWireframeModelTab(wm);
}

void parametrice3d::ManagerTab::childEvent(QChildEvent *event)
{
	if (event->type() == QEvent::ChildAdded ||
		event->type() == QEvent::ChildPolished)
	{
		QObject *objChild = event->child();

		if (tryInitFrame(objChild) == false)
			if (tryConnectComboBox(objChild) == false)
				if (tryConnectDoubleSpinBox(objChild) == false)
					tryConnectSpinBox(objChild);
	}
}

void parametrice3d::ManagerTab::setViewValues(double start1, double end1, int resolution1, double start2, double end2, int resolution2)
{
	start1SpinBox->setValue(start1);
	end1SpinBox->setValue(end1);
	resolution1SpinBox->setValue(resolution1);
	start2SpinBox->setValue(start2);
	end2SpinBox->setValue(end2);
	resolution2SpinBox->setValue(resolution2);
}

bool parametrice3d::ManagerTab::tryInitFrame(QObject *child)
{
	MyFrame *tryFrame = qobject_cast<MyFrame *>(child);

	if (tryFrame != nullptr)
	{
		frame = tryFrame;

		CurveController *curveController = new CurveController(frame, uModel.get());
		curveController->setHeightWidth(frame->height(), frame->width());
		frame->getSwitcher()->addController(id::curveController, curveController);

		ControllerSwitcher::ConnectionArgs curveControllerUpdateArgs{ curveController, SIGNAL(updatePainting()), frame, SLOT(update()) };
		frame->getSwitcher()->addConnection(id::curveController, curveControllerUpdateArgs);

		ControllerSwitcher::ConnectionArgs curveControllerWMTransmissionArgs{ 
			curveController, 
			SIGNAL(sendWireframeModel(WireframeModel)), 
			this, 
			SLOT(receiveWireframeModel(WireframeModel)) 
		};
		frame->getSwitcher()->addConnection(id::curveController, curveControllerWMTransmissionArgs);


		// surface controller
		SurfaceController *surfaceController = new SurfaceController(frame, uModel.get());
		surfaceController->setHeightWidth(frame->height(), frame->width());
		frame->getSwitcher()->addController(id::surfaceController, surfaceController);

		ControllerSwitcher::ConnectionArgs surfaceControllerUpdateArgs{ surfaceController, SIGNAL(updatepainting()), frame, SLOT(update()) };
		frame->getSwitcher()->addConnection(id::surfaceController, surfaceControllerUpdateArgs);

		ControllerSwitcher::ConnectionArgs surfaceControllerWMTransmissionArgs{
			surfaceController,
			SIGNAL(sendWireframeModel(WireframeModel)),
			this,
			SLOT(receiveWireframeModel(WireframeModel))
		};
		frame->getSwitcher()->addConnection(id::surfaceController, surfaceControllerWMTransmissionArgs);


		frame->getSwitcher()->switchToController(id::curveController);

		return true;
	}
	return false;
}

bool parametrice3d::ManagerTab::tryConnectComboBox(QObject * child)
{
	QComboBox *comboBox = qobject_cast<QComboBox *>(child);

	if (comboBox != nullptr)
	{
		connect(comboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &ManagerTab::functionSelectionChanged);
		return true;
	}
	return false;
}

bool parametrice3d::ManagerTab::tryConnectDoubleSpinBox(QObject * child)
{
	QDoubleSpinBox *spinBox = qobject_cast<QDoubleSpinBox *>(child);

	if (spinBox != nullptr)
	{
		if (spinBox->objectName() == QStringLiteral("doubleSpinBoxStart1"))
		{
			start1SpinBox = spinBox;
			spinBox->setValue(-10.0);
			connect(spinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &ManagerTab::start1Changed);
		}
		else if (spinBox->objectName() == QStringLiteral("doubleSpinBoxEnd1"))
		{
			end1SpinBox = spinBox;
			spinBox->setValue(10.0);
			connect(spinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &ManagerTab::end1Changed);
		}
		else if (spinBox->objectName() == QStringLiteral("doubleSpinBoxStart2"))
		{
			start2SpinBox = spinBox;
			spinBox->setValue(0.0);
			connect(spinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &ManagerTab::start2Changed);
		}
		else if (spinBox->objectName() == QStringLiteral("doubleSpinBoxEnd2"))
		{
			end2SpinBox = spinBox;
			spinBox->setValue(0.0);
			connect(spinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &ManagerTab::end2Changed);
		}

		return true;
	}
	return false;
}

bool parametrice3d::ManagerTab::tryConnectSpinBox(QObject *child)
{
	QSpinBox *spinBox = qobject_cast<QSpinBox *>(child);

	if (spinBox != nullptr)
	{
		if (spinBox->objectName() == QStringLiteral("spinBoxResolution1"))
		{
			resolution1SpinBox = spinBox;
			spinBox->setValue(200);
			connect(spinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, &ManagerTab::resolution1Changed);
		}
		else if (spinBox->objectName() == QStringLiteral("spinBoxResolution2"))
		{
			resolution2SpinBox = spinBox;
			spinBox->setValue(0);
			connect(spinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, &ManagerTab::resolution2Changed);
		}

		return true;
	}
	return false;
}

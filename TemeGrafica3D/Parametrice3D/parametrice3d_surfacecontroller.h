#pragma once

#include "controller.h"
#include "pointscalculator3d.h"

namespace parametrice3d
{

	class SurfaceController : public Controller
	{
		Q_OBJECT

	public:
		explicit SurfaceController(QObject *parent = nullptr, Model *model = nullptr);
		virtual ~SurfaceController() = default;

		void setHeightWidth(double newHeight, double newWidth);

		// Inherited via Controller
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myWheelEvent(QWheelEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;

	signals:
		void sendWireframeModel(WireframeModel wm);

	private:
		PointsCalculator3D calculator;
		double height = 100.0;
		double width = 100.0;
		const double zp = 400.0;
	};

}
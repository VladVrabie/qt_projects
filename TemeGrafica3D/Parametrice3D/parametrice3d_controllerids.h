#pragma once

#include <string>

namespace parametrice3d::id
{
	inline std::string curveController{ "curvecontroller" };
	inline std::string surfaceController{ "surfacecontroller" };
}
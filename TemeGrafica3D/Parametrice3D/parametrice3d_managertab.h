#pragma once

#include <QWidget>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QSpinBox>

#include <memory>
#include "myframe.h"
#include "Parametrice3D/parametrice3d_functions.h"
#include "Parametrice3D/parametrice3d_controllerids.h"
#include "Parametrice3D/parametrice3d_curvecontroller.h"
#include "Parametrice3D/parametrice3d_surfacecontroller.h"

namespace parametrice3d
{

	class ManagerTab : public QWidget
	{
		Q_OBJECT

	public:
		explicit ManagerTab(QWidget *parent = nullptr);
		virtual ~ManagerTab() = default;

	public slots:
		void functionSelectionChanged(int index);

		void start1Changed(double value);
		void end1Changed(double value);
		void resolution1Changed(int value);

		void start2Changed(double value);
		void end2Changed(double value);
		void resolution2Changed(int value);

		void receiveWireframeModel(WireframeModel wm);

	signals:
		void sendWireframeModelTab(WireframeModel wm);

	protected:
		virtual void childEvent(QChildEvent *event) override;

	private:
		template <class... Ts>
		void setModelValues(Ts... args)
		{
			uModel->getData() = { args... };
		}

		void setViewValues(double start1, double end1, int resolution1, double start2 = 0.0, double end2 = 0.0, int resolution2 = 0);
		bool tryInitFrame(QObject *child);
		bool tryConnectComboBox(QObject *child);
		bool tryConnectDoubleSpinBox(QObject *child);
		bool tryConnectSpinBox(QObject *child);


		MyFrame *frame = nullptr;

		QDoubleSpinBox *start1SpinBox = nullptr;
		QDoubleSpinBox *end1SpinBox = nullptr;
		QSpinBox *resolution1SpinBox = nullptr;

		QDoubleSpinBox *start2SpinBox = nullptr;
		QDoubleSpinBox *end2SpinBox = nullptr;
		QSpinBox *resolution2SpinBox = nullptr;

		std::unique_ptr<Model> uModel = std::make_unique<Model>();
	};

}
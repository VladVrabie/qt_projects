#include "Parametrice3D/parametrice3d_surfacecontroller.h"

parametrice3d::SurfaceController::SurfaceController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void parametrice3d::SurfaceController::setHeightWidth(double newHeight, double newWidth)
{
	height = newHeight;
	width = newWidth;
}

void parametrice3d::SurfaceController::myMousePressEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void parametrice3d::SurfaceController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void parametrice3d::SurfaceController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void parametrice3d::SurfaceController::myWheelEvent(QWheelEvent * event)
{
	Q_UNUSED(event);
}

void parametrice3d::SurfaceController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void parametrice3d::SurfaceController::myResizeEvent(QResizeEvent * event)
{
	height = event->size().height();
	width = event->size().width();
}

void parametrice3d::SurfaceController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	auto f = _model->getP<PointsCalculator3D::fPtr2>(0);
	auto g = _model->getP<PointsCalculator3D::fPtr2>(1);
	auto h = _model->getP<PointsCalculator3D::fPtr2>(2);
	auto& start1 = _model->get<double>(3);
	auto& end1 = _model->get<double>(4);
	auto& resolution1 = _model->get<unsigned>(5);
	auto& start2 = _model->get<double>(6);
	auto& end2 = _model->get<double>(7);
	auto& resolution2 = _model->get<unsigned>(8);

	if (f && g && h && start1 != end1 && start2 != end2) // alte verificari?
	{
		auto wm = calculator.computeSurface(*f, *g, *h, start1, end1, resolution1, start2, end2, resolution2);
		emit sendWireframeModel(wm);
		auto points2d = wm.perspectiveProjection(height, width, zp);

		for (auto& connection : wm.getConnections())
			painter.drawLine(
				points2d[connection.first],
				points2d[connection.second]
			);
	}
}

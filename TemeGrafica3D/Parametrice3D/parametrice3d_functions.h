#pragma once

#include <QtMath>

namespace parametrice3d
{

	
	class Functions
	{
	public:
		// Curbe parametrice
		static const double razaCurbaElice;
		static const double c;
		static const double d;

		// Suprafete parametrice
		static const double p;
		static const double q;
		static const double r;
		static const double s;


		Functions() = delete;
		~Functions() = delete;
		
		static double xCurbaElice(double u)
		{
			return razaCurbaElice * qCos(u);
		}
		
		static double yCurbaElice(double u)
		{
			return razaCurbaElice * qSin(u);
		}
		
		static double zCurbaElice(double u)
		{
			return c * u + d;
		}
		
		static double xSpirala3D(double u)
		{
			return razaCurbaElice * u * qCos(u);
		}
		
		static double ySpirala3D(double u)
		{
			return razaCurbaElice * u * qSin(u);
		}
		
		static double zSpirala3D(double u)
		{
			return c * u + d;
		}
		
		static double xElipsoid(double u, double v)
		{
			return p * qCos(u) * qCos(v);
		}

		static double yElipsoid(double u, double v)
		{
			return q * qCos(u) * qSin(v);
		}
		
		static double zElipsoid(double u, double v)
		{
			Q_UNUSED(v)
			return r * qSin(u) + s;
		}
		
		static double xCofrag(double u, double v)
		{
			return p * qSin(u) * qSin(v);
		}
		
		static double yCofrag(double u, double v)
		{
			Q_UNUSED(v)
			return q * u;
		}
		
		static double zCofrag(double u, double v)
		{
			Q_UNUSED(u)
			return r * v + s;
		}
		
	};
	
}

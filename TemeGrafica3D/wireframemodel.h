#pragma once

#include <QPointF>
#include <QVector3D>
#include <QVector>

#include <algorithm>
#include <utility>

class WireframeModel
{
public:
	typedef std::pair<unsigned, unsigned> connection_t;

	WireframeModel() = default;
	WireframeModel(const WireframeModel &other);
	~WireframeModel() = default;
	WireframeModel& operator= (const WireframeModel &other);

	auto getVertices() -> QVector<QVector3D> &;
	auto getConnections() -> QVector<connection_t> &;

	auto vertexAt(unsigned index) -> QVector3D &;
	auto connectionAt(unsigned index) -> connection_t &;

	WireframeModel& operator<< (const QVector3D &vertex);
	WireframeModel& operator<< (QVector3D &&vertex);
	WireframeModel& operator<< (const QVector<QVector3D> &verticesVector);

	WireframeModel& operator<< (const connection_t &connection);
	WireframeModel& operator<< (connection_t &&connection);
	WireframeModel& operator<< (const QVector<connection_t> &connectionsVector);

	static auto perspectiveProjection(const QVector3D &vertex, double height, double width, double zp) -> QPointF;
	auto perspectiveProjection(double height, double width, double zp) const -> QVector<QPointF>;

	auto getCenter() const -> QVector3D;

private:
	QVector<QVector3D> vertices;
	QVector<connection_t> connections;
};

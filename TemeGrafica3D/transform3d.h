#pragma once

#include <QMatrix4x4>

#include "wireframemodel.h"

class Transform3d
{
public:
	Transform3d() = default;
	~Transform3d() = default;

	WireframeModel apply(WireframeModel &model) const;

	void translate(double x, double y, double z);

	void rotateOx(double angle);
	void rotateOy(double angle);
	void rotateOz(double angle);

	void rotateOx(double sinAngle, double cosAngle);
	void rotateOy(double sinAngle, double cosAngle);
	void rotateOz(double sinAngle, double cosAngle);

	void rotateAxis(const QVector3D &point, const QVector3D &axis, double angle);

	void scaleAtOrigin(double sx, double sy, double sz);
	void scaleAtPoint(const QVector3D &point, double sx, double sy, double sz);


	void toIdentity();

private:
	QMatrix4x4 transformMatrix;
};

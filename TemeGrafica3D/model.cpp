#include "model.h"

std::vector<std::any>& Model::getData()
{
    return _data;
}

std::any & Model::operator[](int index)
{
	return _data[static_cast<std::size_t>(index)];
}

auto Model::begin() -> std::vector<std::any>::iterator
{
	return _data.begin();
}

auto Model::end() -> std::vector<std::any>::iterator
{
	return _data.end();
}

bool Model::isEmpty() const
{
	return _data.empty();
}

int Model::size() const
{
	return static_cast<int>(_data.size());
}

void Model::setData(const std::vector<std::any> &data)
{
    _data = data;
}

void Model::setData(std::vector<std::any>&& data)
{
    _data = std::move(data);
}

void Model::clearData()
{
    _data.clear();
}

cmake_minimum_required (VERSION 3.0)

# set project name
project (TemeGrafica3D)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /SUBSYSTEM:WINDOWS /ENTRY:mainCRTStartup")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")

# add project directory to include path
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(CMAKE_INCLUDE_CURRENT_DIR ON)


# set include and source root for files
set(INCROOT ${PROJECT_SOURCE_DIR})
set(SRCROOT ${PROJECT_SOURCE_DIR})

# all source files
file(GLOB MAIN_INCS "${INCROOT}/*.h")
file(GLOB MAIN_SRCS "${SRCROOT}/*.cpp")
file(GLOB MAIN_UIS "${SRCROOT}/*.ui")

file(GLOB CASA3D_INCS "${INCROOT}/Casa3D/*.h")
file(GLOB CASA3D_SRCS "${SRCROOT}/Casa3D/*.cpp")

file(GLOB PARAMETRICE3D_INCS "${INCROOT}/Parametrice3D/*.h")
file(GLOB PARAMETRICE3D_SRCS "${SRCROOT}/Parametrice3D/*.cpp")


# Setting up visual studio filters
source_group("Common\\Headers" FILES ${MAIN_INCS})
source_group("Common\\Sources" FILES ${MAIN_SRCS})
source_group("Common\\UI files" FILES ${MAIN_UIS})

source_group("Casa3D\\Headers" FILES ${CASA3D_INCS})
source_group("Casa3D\\Sources" FILES ${CASA3D_SRCS})

source_group("Parametrice3D\\Headers" FILES ${PARAMETRICE3D_INCS})
source_group("Parametrice3D\\Sources" FILES ${PARAMETRICE3D_SRCS})

#set Qt path
set(Qt5_DIR "C:\\Qt\\5.10.1\\msvc2017_64\\lib\\cmake\\Qt5")

# Find the QtWidgets library
find_package(Qt5 COMPONENTS Core Widgets Gui REQUIRED)

# For moc compilation and *.ui files
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)


# We need add -DQT_WIDGETS_LIB when using QtWidgets in Qt 5
add_definitions(${Qt5Widgets_DEFINITIONS})

add_executable(TemeGrafica3D  ${MAIN_SRCS} ${MAIN_INCS} ${MAIN_UIS}
							  ${CASA3D_INCS} ${CASA3D_SRCS}
							  ${PARAMETRICE3D_INCS} ${PARAMETRICE3D_SRCS}
							  )

target_link_libraries(TemeGrafica3D Qt5::Widgets Qt5::Core Qt5::Gui)


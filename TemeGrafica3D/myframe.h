#ifndef MYFRAME_H
#define MYFRAME_H

#include <QFrame>

#include "controllerswitcher.h"

class MyFrame : public QFrame
{
    Q_OBJECT

public:
    explicit MyFrame(QWidget *parent = nullptr);
	virtual ~MyFrame() = default;

    ControllerSwitcher* getSwitcher();

public slots:
    void setController(Controller *controller);

protected:
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
    virtual void mouseReleaseEvent(QMouseEvent *event) override;
	virtual void wheelEvent(QWheelEvent *event) override;
    virtual void keyPressEvent(QKeyEvent *event) override;
	virtual void resizeEvent(QResizeEvent *event) override;
    virtual void paintEvent(QPaintEvent *event) override;

    ControllerSwitcher *_switcher;

private:
    Controller *_controller = nullptr;
};

#endif // MYFRAME_H

#include "controllerswitcher.h"

ControllerSwitcher::ConnectionArgs::ConnectionArgs(const QObject *sender, const char *signal,
                                                   const QObject *receiver, const char *method)
    : _sender{sender}, _signal{signal}, _receiver{receiver}, _method{method}
{
}

ControllerSwitcher::ConnectionArgs::ConnectionArgs(const ControllerSwitcher::ConnectionArgs & other)
	: ControllerSwitcher::ConnectionArgs(other._sender, other._signal.c_str(), other._receiver, other._method.c_str())
{
}

auto ControllerSwitcher::ConnectionArgs::asTuple() const -> std::tuple<const QObject *, const char *, const QObject *, const char *, Qt::ConnectionType>
{
    return std::make_tuple(_sender, _signal.c_str(), _receiver, _method.c_str(), Qt::AutoConnection);
}

bool operator== (const ControllerSwitcher::ConnectionArgs &lhs,
                 const ControllerSwitcher::ConnectionArgs &rhs)
{
    return std::make_tuple(lhs._sender, lhs._signal, lhs._receiver, lhs._method)
         == std::make_tuple(rhs._sender, rhs._signal, rhs._receiver, rhs._method) ;
}


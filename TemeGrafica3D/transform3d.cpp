#include "transform3d.h"

WireframeModel Transform3d::apply(WireframeModel & model) const
{
	WireframeModel result;
	
	for (auto& vertex : model.getVertices())
		result << transformMatrix * vertex;

	return result << model.getConnections();
}

void Transform3d::translate(double x, double y, double z)
{
	QMatrix4x4 tMatrix;
	tMatrix(0, 3) = x;
	tMatrix(1, 3) = y;
	tMatrix(2, 3) = z;

	transformMatrix = tMatrix * transformMatrix;
}

void Transform3d::rotateOx(double angle)
{
	if (angle)
		rotateOx(std::sin(angle), std::cos(angle));
}

void Transform3d::rotateOy(double angle)
{
	if (angle)
		rotateOy(std::sin(angle), std::cos(angle));
}

void Transform3d::rotateOz(double angle)
{
	if (angle)
		rotateOz(std::sin(angle), std::cos(angle));
}

void Transform3d::rotateOx(double sinAngle, double cosAngle)
{
	QMatrix4x4 tMatrix; // ok 
	tMatrix(1, 1) = cosAngle;
	tMatrix(1, 2) = -sinAngle;
	tMatrix(2, 1) = sinAngle;
	tMatrix(2, 2) = cosAngle;

	transformMatrix = tMatrix * transformMatrix;
}

void Transform3d::rotateOy(double sinAngle, double cosAngle)
{
	QMatrix4x4 tMatrix; 
	tMatrix(0, 0) = cosAngle;
	tMatrix(0, 2) = -sinAngle;
	tMatrix(2, 0) = sinAngle;
	tMatrix(2, 2) = cosAngle;

	transformMatrix = tMatrix * transformMatrix;
}

void Transform3d::rotateOz(double sinAngle, double cosAngle)
{
	QMatrix4x4 tMatrix; // ok
	tMatrix(0, 0) = cosAngle;
	tMatrix(0, 1) = -sinAngle;
	tMatrix(1, 0) = sinAngle;
	tMatrix(1, 1) = cosAngle;

	transformMatrix = tMatrix * transformMatrix;
}

void Transform3d::rotateAxis(const QVector3D & point, const QVector3D & axis, double angle)
{
	if (angle)
	{
		translate(-point.x(), -point.y(), -point.z());

		double hypotenuse1 = std::sqrt(axis.x() * axis.x() + axis.z() * axis.z());
		double sinAlpha = 0.0; // default values if
		double cosAlpha = 1.0; // hypotenuse1 == 0.0
		if (hypotenuse1 != 0.0)
		{
			sinAlpha = axis.z() / hypotenuse1;
			cosAlpha = axis.x() / hypotenuse1;
		}

		rotateOy(sinAlpha, cosAlpha);

		double hypotenuse2 = std::sqrt(axis.x() * axis.x() + axis.y() * axis.y() + axis.z() * axis.z());
		double sinBeta = 0.0;
		double cosBeta = 1.0; 
		if (hypotenuse2 != 0.0)
		{
			sinBeta = axis.y() / hypotenuse2;
			cosBeta = hypotenuse1 / hypotenuse2; // eu asa zic 
		}

		rotateOz(-sinBeta, cosBeta);

		rotateOx(angle);

		rotateOz(sinBeta, cosBeta);
		rotateOy(-sinAlpha, cosAlpha);
		translate(point.x(), point.y(), point.z());
	}
}

void Transform3d::scaleAtOrigin(double sx, double sy, double sz)
{
	QMatrix4x4 tMatrix; // ok
	tMatrix(0, 0) = sx;
	tMatrix(1, 1) = sy;
	tMatrix(2, 2) = sz;

	transformMatrix = tMatrix * transformMatrix;
}

void Transform3d::scaleAtPoint(const QVector3D & point, double sx, double sy, double sz)
{
	translate(-point.x(), -point.y(), -point.z());
	scaleAtOrigin(sx, sy, sz);
	translate(point.x(), point.y(), point.z());
}

void Transform3d::toIdentity()
{
	transformMatrix.setToIdentity();
}

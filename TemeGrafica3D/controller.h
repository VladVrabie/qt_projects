#ifndef ICONTROLLER_H
#define ICONTROLLER_H

#include <QObject>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QKeyEvent>
#include <QResizeEvent>
#include <QPaintEvent>
#include <QPainter>

#include <string>
#include "model.h"

class Controller : public QObject
{
    Q_OBJECT

public:
    explicit Controller(QObject *parent = nullptr, Model *model = nullptr);
	virtual ~Controller() = default;

    virtual void setModel(Model *model);

    virtual void myMousePressEvent(QMouseEvent *event) = 0;
    virtual void myMouseMoveEvent(QMouseEvent *event) = 0;
    virtual void myMouseReleaseEvent(QMouseEvent *event) = 0;
	virtual void myWheelEvent(QWheelEvent *event) = 0;
    virtual void myKeyPressEvent(QKeyEvent *event) = 0;
	virtual void myResizeEvent(QResizeEvent *event) = 0;
    virtual void myPaintEvent(QPaintEvent *event, QPainter & painter) = 0;

	// virtual void beforeConnect() = 0;
	// virtual void afterConnect() = 0;
	// virtual void beforeDisconnect() = 0;
	// virtual void afterDisconnect() = 0;

signals:
    void updatePainting();
    void switchController(std::string idController);

protected:
    Model *_model;
};



#endif // ICONTROLLER_H

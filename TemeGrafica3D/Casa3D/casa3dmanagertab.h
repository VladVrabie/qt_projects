#pragma once

#include <QWidget>
#include <QSpinBox>
#include <QSlider>

#include <memory>
#include "myframe.h"
#include "Casa3D/casa3dids.h"
#include "Casa3D/casa3dcontroller.h"

namespace casa3d
{

	class ManagerTab : public QWidget
	{
		Q_OBJECT

	public:
		explicit ManagerTab(QWidget *parent = nullptr);
		virtual ~ManagerTab() = default;

	public slots:
		void zpChanged(int newValue);
		void xAxisAngleChanged(int newValue);
		void yAxisAngleChanged(int newValue);
		void zAxisAngleChanged(int newValue);

		void receiveWireframeModel(WireframeModel wm);
		void makeAHouse();

	signals:
		void resetControllerTransform();

	protected:
		virtual void childEvent(QChildEvent *event) override;

	private:
		void houseModel();
		bool tryInitFrame(QObject *child);
		bool tryConnectSpinBox(QObject *child);
		bool tryConnectSlider(QObject *child);


		MyFrame *frame = nullptr;

		int lastOxAngle = 0;
		int lastOyAngle = 0;
		int lastOzAngle = 0;

		std::unique_ptr<Model> uModel = std::make_unique<Model>();
	};

}

#include "Casa3D/casa3dcontroller.h"

casa3d::Casa3dController::Casa3dController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void casa3d::Casa3dController::setWidthHeight(double newWidth, double newHeight)
{
	height = newHeight;
	width = newWidth;
}

void casa3d::Casa3dController::myMousePressEvent(QMouseEvent * event)
{
	pressedButton = event->button();
	lastPos = event->localPos();
}

void casa3d::Casa3dController::myMouseMoveEvent(QMouseEvent * event)
{
	QPointF currentPos = event->localPos();

	switch (pressedButton)
	{
	case Qt::LeftButton:
	{
		QPointF delta = currentPos - lastPos;
		transform3d.translate(delta.x(), -delta.y(), 0.0);

		break;
	}
	case Qt::RightButton:
	{
		auto& wm = _model->get<WireframeModel>(0);
		auto wmToDraw = transform3d.apply(wm);
		auto centre3D = wmToDraw.getCenter();
		auto centre2D = WireframeModel::perspectiveProjection(centre3D, height, width, _model->get<double>(1));

		double currentDistance = qSqrt(
			  qPow((currentPos.x() - centre2D.x()), 2.0)
			+ qPow((currentPos.y() - centre2D.y()), 2.0)
		);

		double lastDistance = qSqrt(
			  qPow((lastPos.x() - centre2D.x()), 2.0)
			+ qPow((lastPos.y() - centre2D.y()), 2.0)
		);
		double s = currentDistance / lastDistance;

		transform3d.scaleAtPoint(centre3D, s, s, s);
		break;
	}
	case Qt::MiddleButton:
		break;

	default:
		break;
	}

	lastPos = currentPos;
	emit updatePainting();
}

void casa3d::Casa3dController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
	pressedButton = Qt::NoButton;
}

void casa3d::Casa3dController::myWheelEvent(QWheelEvent * event)
{
	QPoint degrees = event->angleDelta() / 8;
	transform3d.translate(0.0, 0.0, degrees.y());
	emit updatePainting();
	event->accept();
}

void casa3d::Casa3dController::myKeyPressEvent(QKeyEvent * event)
{
	auto key = event->key();

	switch (key)
	{
	case Qt::Key_R:
		transform3d.toIdentity();
		break;

	case Qt::Key_H:
		transform3d.toIdentity();
		emit iWantHouse();
		break;

	default:
		break;
	}

	emit updatePainting();
}

void casa3d::Casa3dController::myResizeEvent(QResizeEvent * event)
{
	height = event->size().height();
	width = event->size().width();
}

void casa3d::Casa3dController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	
	auto wm = _model->get<WireframeModel>(0);
	auto wmToDraw = transform3d.apply(wm);
	applyRotations(wmToDraw);
	wmToDraw = transform3d.apply(wm);

	double zp = _model->get<double>(1);
	auto points2D = wmToDraw.perspectiveProjection(height, width, zp);

	for (auto& connection : wmToDraw.getConnections())
		painter.drawLine(
			points2D[connection.first],
			points2D[connection.second]
		);
	
}

void casa3d::Casa3dController::resetTransform()
{
	transform3d.toIdentity();
}

void casa3d::Casa3dController::applyRotations(const WireframeModel &wm)
{
	auto centre = wm.getCenter();

	double xAngle = qDegreesToRadians(_model->get<double>(2));
	if (xAngle != 0.0)
	{
		transform3d.rotateAxis(centre, QVector3D(1.0f, 0.0f, 0.0f), xAngle);
		(*_model)[2] = 0.0;
	}

	double yAngle = qDegreesToRadians(_model->get<double>(3));
	if (yAngle != 0.0)
	{
		transform3d.rotateAxis(centre, QVector3D(0.0f, 1.0f, 0.0f), yAngle);
		(*_model)[3] = 0.0;
	}

	double zAngle = qDegreesToRadians(_model->get<double>(4));
	if (zAngle != 0.0)
	{
		transform3d.rotateAxis(centre, QVector3D(0.0f, 0.0f, 1.0f), zAngle);
		(*_model)[4] = 0.0;
	}
}

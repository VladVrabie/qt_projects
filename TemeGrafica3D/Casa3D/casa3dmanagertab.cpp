#include "Casa3D/casa3dmanagertab.h"

casa3d::ManagerTab::ManagerTab(QWidget * parent)
	: QWidget(parent)
{
	houseModel();
}

void casa3d::ManagerTab::zpChanged(int newValue)
{
	(*uModel)[1] = static_cast<double>(newValue);
	frame->update();
}

void casa3d::ManagerTab::xAxisAngleChanged(int newValue)
{
	
	(*uModel)[2] = static_cast<double>(newValue - lastOxAngle);
	lastOxAngle = newValue;
	frame->update();
}

void casa3d::ManagerTab::yAxisAngleChanged(int newValue)
{
	(*uModel)[3] = static_cast<double>(newValue - lastOyAngle);
	lastOyAngle = newValue;
	frame->update();
}

void casa3d::ManagerTab::zAxisAngleChanged(int newValue)
{
	(*uModel)[4] = static_cast<double>(newValue - lastOzAngle);
	lastOzAngle = newValue;
	frame->update();
}

void casa3d::ManagerTab::receiveWireframeModel(WireframeModel wm)
{
	// if size
	(*uModel)[0] = wm;
	emit resetControllerTransform();
	// if frame
	frame->update();
}

void casa3d::ManagerTab::makeAHouse()
{
	houseModel();
}

void casa3d::ManagerTab::childEvent(QChildEvent * event)
{
	if (event->type() == QEvent::ChildAdded ||
		event->type() == QEvent::ChildPolished)
	{
		QObject *objChild = event->child();

		if (tryInitFrame(objChild) == false)
			if (tryConnectSpinBox(objChild) == false)
				tryConnectSlider(objChild);
	}
}

void casa3d::ManagerTab::houseModel()
{
	uModel->clearData();

	auto wm = WireframeModel()
		<< QVector3D{ -50.0f, -50.0f, -1.0f }
		<< QVector3D{ 50.0f, -50.0f, -1.0f }
		<< QVector3D{ 50.0f,  50.0f, -1.0f }
		<< QVector3D{ -50.0f,  50.0f, -1.0f }
		<< QVector3D{ -50.0f, -50.0f, -101.0f }
		<< QVector3D{ 50.0f, -50.0f, -101.0f }
		<< QVector3D{ 50.0f,  50.0f, -101.0f }
		<< QVector3D{ -50.0f,  50.0f, -101.0f }
		<< std::make_pair(0u, 1u)
		<< std::make_pair(1u, 2u)
		<< std::make_pair(2u, 3u)
		<< std::make_pair(3u, 0u)
		<< std::make_pair(4u, 5u)
		<< std::make_pair(5u, 6u)
		<< std::make_pair(6u, 7u)
		<< std::make_pair(7u, 4u)
		<< std::make_pair(0u, 4u)
		<< std::make_pair(1u, 5u)
		<< std::make_pair(2u, 6u)
		<< std::make_pair(3u, 7u)
		<< QVector3D{ 0.0f, 130.0f, -51.0f }
		<< std::make_pair(8, 2)
		<< std::make_pair(8, 3)
		<< std::make_pair(8, 6)
		<< std::make_pair(8, 7);

	uModel->appendData(wm);
	uModel->appendData(400.0); // zp
	uModel->appendData(0.0);   // x axis rotation
	uModel->appendData(0.0);   // y axis rotation
	uModel->appendData(0.0);   // z axis rotation
}

bool casa3d::ManagerTab::tryInitFrame(QObject * child)
{
	MyFrame *tryFrame = qobject_cast<MyFrame *>(child);
	if (tryFrame != nullptr)
	{
		frame = tryFrame;

		Casa3dController *casa3dcontroller = new Casa3dController(frame, uModel.get());
		casa3dcontroller->setWidthHeight(frame->width(), frame->height());
		frame->getSwitcher()->addController(id::casa3dController, casa3dcontroller);

		ControllerSwitcher::ConnectionArgs casa3dControllerUpdateArgs{ casa3dcontroller, SIGNAL(updatePainting()), frame, SLOT(update()) };
		frame->getSwitcher()->addConnection(id::casa3dController, casa3dControllerUpdateArgs);

		ControllerSwitcher::ConnectionArgs casa3dControllerMakeHouseArgs{ casa3dcontroller, SIGNAL(iWantHouse()), this, SLOT(makeAHouse()) };
		frame->getSwitcher()->addConnection(id::casa3dController, casa3dControllerMakeHouseArgs);

		ControllerSwitcher::ConnectionArgs casa3dControllerResetTransformArgs{ this, SIGNAL(resetControllerTransform()), casa3dcontroller, SLOT(resetTransform()) };
		frame->getSwitcher()->addConnection(id::casa3dController, casa3dControllerResetTransformArgs);


		frame->getSwitcher()->switchToController(id::casa3dController);

		return true;
	}
	return false;
}

bool casa3d::ManagerTab::tryConnectSpinBox(QObject * child)
{
	QSpinBox *spinBox = qobject_cast<QSpinBox *>(child);
	if (spinBox != nullptr)
	{
		connect(spinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, &ManagerTab::zpChanged);

		return true;
	}
	return false;
}

bool casa3d::ManagerTab::tryConnectSlider(QObject * child)
{
	QSlider *slider = qobject_cast<QSlider *>(child);
	if (slider != nullptr)
	{
		if (slider->objectName() == QStringLiteral("sliderXAxis"))
		{
			connect(slider, &QSlider::valueChanged, this, &ManagerTab::xAxisAngleChanged);
		}
		else if (slider->objectName() == QStringLiteral("sliderYAxis"))
		{
			connect(slider, &QSlider::valueChanged, this, &ManagerTab::yAxisAngleChanged);
		}
		else if (slider->objectName() == QStringLiteral("sliderZAxis"))
		{
			connect(slider, &QSlider::valueChanged, this, &ManagerTab::zAxisAngleChanged);
		}

		return true;
	}
	return false;
}

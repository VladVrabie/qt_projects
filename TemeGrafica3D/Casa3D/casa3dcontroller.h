#pragma once

#include <QtMath>
#include "controller.h"
#include "wireframemodel.h"
#include "transform3d.h"

namespace casa3d
{

	class Casa3dController : public Controller
	{
		Q_OBJECT

	public:
		explicit Casa3dController(QObject *parent = nullptr, Model *model = nullptr);
		virtual ~Casa3dController() = default;

		void setWidthHeight(double newWidth, double newHeight);

		// Inherited via Controller
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myWheelEvent(QWheelEvent *event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;

	public slots:
		void resetTransform();

	signals:
		void iWantHouse();

	private:
		void applyRotations(const WireframeModel &wm);

		double height = 100.0;
		double width = 100.0;
		Transform3d transform3d;
		Qt::MouseButton pressedButton;
		QPointF lastPos;
	};

}
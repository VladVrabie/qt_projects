#pragma once

#include <functional>
#include "wireframemodel.h"

class PointsCalculator3D
{
public:
	PointsCalculator3D() = default;
	~PointsCalculator3D() = default;

	typedef std::function<double(double)> fPtr1;
	typedef std::function<double(double, double)> fPtr2;

	WireframeModel computeCurve(const fPtr1 &f,
								const fPtr1 &g,
								const fPtr1 &h,
								double start,
								double end,
								unsigned resolution);

	WireframeModel computeSurface(const fPtr2 &f,
								  const fPtr2 &g,
								  const fPtr2 &h,
								  double start1,
								  double end1,
								  unsigned resolution1,
								  double start2,
								  double end2,
								  unsigned resolution2);

};

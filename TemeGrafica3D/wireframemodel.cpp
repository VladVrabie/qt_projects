#include "wireframemodel.h"

WireframeModel::WireframeModel(const WireframeModel & other)
{
	*this = other;
}

WireframeModel & WireframeModel::operator=(const WireframeModel & other)
{
	if (this != &other)
	{
		vertices = other.vertices;
		connections = other.connections;
	}
	return *this;
}

auto WireframeModel::getVertices() -> QVector<QVector3D> &
{
	return vertices;
}

auto WireframeModel::getConnections() -> QVector<WireframeModel::connection_t> &
{
	return connections;
}

auto WireframeModel::vertexAt(unsigned index) -> QVector3D &
{
	return vertices[index];
}

auto WireframeModel::connectionAt(unsigned index) -> WireframeModel::connection_t &
{
	return connections[index];
}

WireframeModel & WireframeModel::operator<<(const QVector3D & vertex)
{
	vertices.append(vertex);
	return *this;
}

WireframeModel & WireframeModel::operator<<(QVector3D && vertex)
{
	vertices.append(vertex);
	return *this;
}

WireframeModel & WireframeModel::operator<<(const QVector<QVector3D>& verticesVector)
{
	vertices.append(verticesVector);
	return *this;
}

WireframeModel & WireframeModel::operator<<(const connection_t & connection)
{
	connections.append(connection);
	return *this;
}

WireframeModel & WireframeModel::operator<<(connection_t && connection)
{
	connections.append(connection);
	return *this;
}

WireframeModel& WireframeModel::operator<< (const QVector<connection_t> &connectionsVector)
{
	connections.append(connectionsVector);
	return *this;
}

auto WireframeModel::perspectiveProjection(double height, double width, double zp) const -> QVector<QPointF>
{
	QVector<QPointF> points2D;
	
	std::for_each(vertices.begin(), vertices.end(), [&](const QVector3D &vertex) {
		points2D.append(perspectiveProjection(vertex, height, width, zp)); 
	});
	return points2D;
}

auto WireframeModel::perspectiveProjection(const QVector3D &vertex, double height, double width, double zp) -> QPointF
{
	double ca = zp / (zp - vertex.z());
	return QPointF(width / 2 + ca * vertex.x(), height / 2 - ca * vertex.y());
}

auto WireframeModel::getCenter() const -> QVector3D
{
	QVector3D center;
	for (auto& vertex : vertices)
		center += vertex;
	return center / vertices.size();
}

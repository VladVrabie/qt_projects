#include "Newton/newton_equation.h"

newton::Equation::Equation()
{

}

newton::Equation::~Equation()
{

}

void newton::Equation::setModel(Model *model)
{
    _model = model;
    auto &modelRef = *_model;
    int nrPuncte = modelRef.size();

    QVector<QVector<double>> matrix;
    matrix.resize(nrPuncte);

    for (int i = 0; i < nrPuncte; ++i)
        matrix[i].append(std::any_cast<QPointF>(&modelRef.getData()[i])->y());

    for (int j = 1; j < nrPuncte; ++j)
        for (int i = 0; i < nrPuncte - j; ++i)
        {
            matrix[i].append((matrix[i+1][j-1] - matrix[i][j-1])
                    / (std::any_cast<QPointF>(&modelRef.getData()[j + i])->x()
                    - std::any_cast<QPointF>(&modelRef.getData()[i])->x()));
        }

    _coeficienti = std::move(matrix[0]);
}

std::function<double (double)> newton::Equation::getXFunctionPtr()
{
    std::function<double(double)> fPtr = [](double u){
        return u;
    };
    return fPtr;
}

std::function<double (double)> newton::Equation::getYFunctionPtr()
{
    std::function<double(double)> fPtr = [this](double u){
        return this->applyNewton(u);
    };
    return fPtr;
}

double newton::Equation::getStart() const
{
    return std::any_cast<QPointF>(&_model->getData().front())->x();
}

double newton::Equation::getEnd() const
{
    return std::any_cast<QPointF>(&_model->getData().back())->x();
}

unsigned newton::Equation::getResolution() const
{
    return 500u;
}

double newton::Equation::applyNewton(double x)
{
    double produs = 1.0;
    double result = 0.0;
    auto &modelRef = *_model;
    int nrPuncte = modelRef.size();

    for (int i = 0; i < nrPuncte - 1; ++i)
    {
        result += _coeficienti[i] * produs;
        produs *= (x - std::any_cast<QPointF>(&modelRef.getData()[i])->x());
    }
    return result + _coeficienti[nrPuncte - 1] * produs;
}

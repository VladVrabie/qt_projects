#ifndef NEWTON_EQUATION_H
#define NEWTON_EQUATION_H

#include <QVector>
#include <QPointF>
#include <functional>
#include "model.h"

namespace newton
{

    class Equation
    {
    public:
        Equation();
        ~Equation();

        void setModel(Model *model);

        std::function<double(double)> getXFunctionPtr();
        std::function<double(double)> getYFunctionPtr();

        double getStart() const;
        double getEnd() const;
        unsigned getResolution() const;

    private:
        double applyNewton(double);

        Model *_model;
        QVector<double> _coeficienti;
    };

}

#endif // NEWTON_EQUATION_H

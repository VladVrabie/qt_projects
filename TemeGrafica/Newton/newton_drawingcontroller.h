#ifndef NEWTON_DRAWINGCONTROLLER_H
#define NEWTON_DRAWINGCONTROLLER_H

#include <QPainterPath>

#include <algorithm>
#include "controller.h"
#include "curvepointscalculator.h"
#include "Newton/newton_equation.h"
#include "Newton/newton_controllerids.h"

namespace newton
{

    class DrawingController : public Controller
    {
        Q_OBJECT

    public:
        explicit DrawingController(QObject *parent = nullptr, Model *model = nullptr);
        ~DrawingController();

        // Controller interface
        virtual void myMousePressEvent(QMouseEvent *event) override;
        virtual void myMouseMoveEvent(QMouseEvent *event) override;
        virtual void myMouseReleaseEvent(QMouseEvent *event) override;
        virtual void myKeyPressEvent(QKeyEvent *event) override;
		virtual void myResizeEvent(QResizeEvent *event) override;
        virtual void myPaintEvent(QPaintEvent *event, QPainter & painter) override;

    private:
        Equation newton;
        CurvePointsCalculator calc;
    };

}

#endif // NEWTON_DRAWINGCONTROLLER_H

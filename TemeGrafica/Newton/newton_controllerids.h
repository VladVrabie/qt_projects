#ifndef NEWTON_CONTROLLERIDS_H
#define NEWTON_CONTROLLERIDS_H

#include <string>

namespace newton::id
{
	inline std::string inputController{ "inputController" };
	inline std::string drawingController{ "drawingController" };
}

#endif // NEWTON_CONTROLLERIDS_H

#include "Newton/newton_drawingcontroller.h"

newton::DrawingController::DrawingController(QObject *parent, Model *model)
    : Controller(parent, model)
{

}

newton::DrawingController::~DrawingController()
{

}

void newton::DrawingController::myMousePressEvent(QMouseEvent *event)
{
    Qt::MouseButton pressedButton = event->button();
	QPointF localPos = event->localPos();
    switch (pressedButton)
    {
	case Qt::LeftButton:
		_model->appendData(localPos);
		emit updatePainting();
		break;
    case Qt::RightButton:
        _model->clearData();
        emit switchController(id::inputController);
        break;
    }
}

void newton::DrawingController::myMouseMoveEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void newton::DrawingController::myMouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void newton::DrawingController::myKeyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event);
}

void newton::DrawingController::myResizeEvent(QResizeEvent *event)
{
	Q_UNUSED(event);
}

void newton::DrawingController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
    Q_UNUSED(event);
    for (auto& anyPoint : *_model)
        painter.drawEllipse(std::any_cast<QPointF>(anyPoint), 5.0, 5.0);

    newton.setModel(_model);

    QVector<QPointF> points = std::move(calc.calculatePoints(newton.getXFunctionPtr(),
															 newton.getYFunctionPtr(),
															 newton.getStart(),
															 newton.getEnd(),
															 newton.getResolution()));

    QPainterPath path(points[0]);
    //for (int i = 1; i < points.size(); ++i)
    //    path.lineTo(points[i]);
	std::for_each(points.begin() + 1, points.end(), [&path](const QPointF &point) { path.lineTo(point); });

    painter.drawPath(path);
}

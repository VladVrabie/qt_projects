#ifndef NEWTON_FRAME_H
#define NEWTON_FRAME_H

#include <memory>
#include "myframe.h"
#include "Newton/newton_drawingcontroller.h"
#include "Newton/newton_inputcontroller.h"

namespace newton
{

    class Frame : public MyFrame
    {
		Q_OBJECT

    public:
        explicit Frame(QWidget *parent = nullptr);
        ~Frame();

    private:
        std::unique_ptr<Model> model = std::make_unique<Model>();
    };

}

#endif // NEWTON_FRAME_H

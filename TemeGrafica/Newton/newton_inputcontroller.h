#ifndef NEWTON_INPUTCONTROLLER_H
#define NEWTON_INPUTCONTROLLER_H

#include <algorithm>
#include "controller.h"
#include "Newton/newton_controllerids.h"

namespace newton
{

    class InputController : public Controller
    {
        Q_OBJECT

    public:
        explicit InputController(QObject *parent = nullptr, Model *model = nullptr);
        ~InputController();

        // Controller interface
		virtual void myMousePressEvent(QMouseEvent *event) override;
		virtual void myMouseMoveEvent(QMouseEvent *event) override;
		virtual void myMouseReleaseEvent(QMouseEvent *event) override;
		virtual void myKeyPressEvent(QKeyEvent *event) override;
		virtual void myResizeEvent(QResizeEvent *event) override;
		virtual void myPaintEvent(QPaintEvent *event, QPainter & painter) override;

    };

}

#endif // NEWTON_INPUTCONTROLLER_H

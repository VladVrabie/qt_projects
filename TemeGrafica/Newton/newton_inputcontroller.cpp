#include "Newton/newton_inputcontroller.h"

newton::InputController::InputController(QObject *parent, Model *model)
    : Controller(parent, model)
{

}

newton::InputController::~InputController()
{

}

void newton::InputController::myMousePressEvent(QMouseEvent *event)
{
    Qt::MouseButton pressedButton = event->button();
    QPointF localPos = event->localPos();
    switch (pressedButton)
    {
    case Qt::LeftButton:
        if (_model->isEmpty())
            _model->appendData(localPos);
        else
            if (localPos.x() > std::any_cast<QPointF>(&_model->getData().back())->x())
                _model->appendData(localPos);
        emit updatePainting();
        break;
    case Qt::MiddleButton:
        _model->clearData();
        emit updatePainting();
        break;
    case Qt::RightButton:
        if (_model->size() > 2)
            emit switchController(id::drawingController);
        break;
    }
}

void newton::InputController::myMouseMoveEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void newton::InputController::myMouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void newton::InputController::myKeyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event);
}

void newton::InputController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void newton::InputController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
    Q_UNUSED(event);
    //for (auto& anyPoint : *_model)
    //    painter.drawEllipse(std::any_cast<QPointF>(anyPoint), 5.0, 5.0);
	//for (int i = 0; i < _model.size(); ++i)
	//	  painter.drawEllipse(_model.get<QPointF>(i), 5.0, 5.0);
	//std::for_each(_model->begin<QPointF>(), _model->end<QPointF>(), [&painter](const QPointF &point) {
	//	painter.drawEllipse(point, 5.0, 5.0);
	//});
	std::for_each(_model->begin(), _model->end(), [&painter](std::any &anyPoint) {
		painter.drawEllipse(std::any_cast<QPointF>(anyPoint), 5.0, 5.0);
	});
}


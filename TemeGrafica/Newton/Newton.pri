HEADERS += \
    $$PWD/newton_equation.h \
    $$PWD/newton_inputcontroller.h \
    $$PWD/newton_drawingcontroller.h \
    $$PWD/newton_frame.h \
    $$PWD/newton_controllerids.h

SOURCES += \
    $$PWD/newton_equation.cpp \
    $$PWD/newton_inputcontroller.cpp \
    $$PWD/newton_drawingcontroller.cpp \
    $$PWD/newton_frame.cpp
	
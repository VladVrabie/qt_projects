#include "controller.h"


Controller::Controller(QObject *parent, Model *model)
    : QObject(parent), _model{model}
{

}

Controller::~Controller()
{

}

void Controller::setModel(Model *model)
{
    this->_model = model;
}

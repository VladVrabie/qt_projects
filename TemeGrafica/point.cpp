#include "point.h"

Point::Point(QPointF point)
	: _point{ point }
{
}

Point::Point(double x, double y)
	: Point(QPointF(x, y))
{
}

Point::~Point()
{
}

Point & Point::operator=(const QPointF & point)
{
	_point = point;
	return *this;
}

double Point::x() const
{
	return _point.x();
}

double Point::y() const
{
	return _point.y();
}

void Point::setX(double x)
{
	_point.setX(x);
}

void Point::setY(double y)
{
	_point.setX(y);
}

Point::setterFPtr Point::setterToPoint(const QPointF & mousePosition)
{
	setterFPtr result(nullptr);
	if (insideCircle(mousePosition))
	{
		result = [this](const QPointF &point) {
			*this = point;
		};
	}
	return result;
}

void Point::drawPoint(QPainter & painter, bool withCircle) const
{
	painter.drawPoint(_point);
	if (withCircle)
		painter.drawEllipse(_point, circleRadius, circleRadius);
}

bool Point::isNull() const
{
	return _point.isNull();
}

void Point::setToNull()
{
	_point.setX(0.0);
	_point.setY(0.0);
}

bool Point::insideCircle(const QPointF & point) const
{
	double x = point.x() - _point.x();
	double y = point.y() - _point.y();

	return x * x + y * y <= circleRadius * circleRadius;
}
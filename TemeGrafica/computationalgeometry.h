#ifndef COMPUTATIONALGEOMETRY_H
#define COMPUTATIONALGEOMETRY_H

#include <QPointF>
#include <QVector>
#include <algorithm>
#include <memory>
#include <utility>
#include <iterator>

class ComputationalGeometry
{
public:
    ComputationalGeometry() = default;

    static bool sameSign(int x, int y);

    static double F(const QPointF &A, const QPointF &B, const QPointF &C);

    static bool leftTurn(const QPointF &A, const QPointF &B, const QPointF &C);
    static bool rightTurn(const QPointF &A, const QPointF &B, const QPointF &C);
    static bool collinearPoints(const QPointF &A, const QPointF &B, const QPointF &C);

    static bool separateHalfPlanes(const QPointF &A, const QPointF &B,
                                  const QPointF &startLine, const QPointF &endLine);

    static bool convexQuadrilateral(const QPointF &A, const QPointF &B, const QPointF &C, const QPointF &D);

    static bool convexPolygon(const QVector<QPointF> &points);

    static bool pointInSegment(const QPointF &point, const QPointF &start, const QPointF &end);

	static unsigned linePolygonNbIntersections(const QPointF &start, const QPointF &end, const QVector<QPointF> &points);
	static unsigned linePolygonNbIntersections2(const QPointF &start, const QPointF &end, const QVector<QPointF> &points);

    static bool pointInPolygon(const QPointF &point, const QVector<QPointF> &points);

	static std::vector<std::pair<QPointF, QPointF>> triangulatePolygon(const QVector<QPointF> &points);
	static bool polygonEar(int middlePointIndex, const QVector<QPointF> &points, const QVector<QPointF> &origPoints, bool clock);
	static bool polygonDiagonal(const QPointF &A, const QPointF &B, const QVector<QPointF> &points);
	static bool interiorDiagonal(int startIndex, int endIndex, const QVector<QPointF> &points, bool clock);

	static QVector<QPointF> convexHull(const QVector<QPointF> &points);
	static QVector<QPointF> quickHull(const QPointF &start, const QPointF &end, const QVector<QPointF> &points);

	static bool clockwisePolygon(const QVector<QPointF> &points);
};

#endif // COMPUTATIONALGEOMETRY_H

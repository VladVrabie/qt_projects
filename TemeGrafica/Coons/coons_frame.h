#ifndef COON_SFRAME_H
#define COON_SFRAME_H

#include <memory>
#include "myframe.h"
#include "Coons/coons_inputcontroller.h"
#include "Coons/coons_drawingcontroller.h"

namespace coons
{

    class Frame : public MyFrame
    {
        Q_OBJECT

    public:
        explicit Frame(QWidget *parent = nullptr);
        ~Frame();

    private:
        std::unique_ptr<Model> uModel = std::make_unique<Model>();
    };

}

#endif // COONS_FRAME_H

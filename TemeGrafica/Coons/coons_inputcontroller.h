#ifndef COONS_INPUTCONTROLLER_H
#define COONS_INPUTCONTROLLER_H

#include <algorithm>
#include "controller.h"
#include "arrow.h"
#include "Coons/coons_controllerids.h"

namespace coons
{

    class InputController : public Controller
    {
        Q_OBJECT

    public:
        explicit InputController(QObject *parent = nullptr, Model *model = nullptr);
        ~InputController();

        // Controller interface
		virtual void myMousePressEvent(QMouseEvent *event) override;
		virtual void myMouseMoveEvent(QMouseEvent *event) override;
		virtual void myMouseReleaseEvent(QMouseEvent *event) override;
		virtual void myKeyPressEvent(QKeyEvent *event) override;
		virtual void myResizeEvent(QResizeEvent *event) override;
		virtual void myPaintEvent(QPaintEvent *event, QPainter &painter) override;

    private:
        void setArrowPoints(const QPointF &position);

        Arrow arrow;
    };

}

#endif // COONS_INPUTCONTROLLER_H

#include "Coons/coons_drawingcontroller.h"

coons::DrawingController::DrawingController(QObject *parent, Model *model)
    : Controller(parent, model)
{

}

coons::DrawingController::~DrawingController()
{

}

void coons::DrawingController::myMousePressEvent(QMouseEvent *event)
{
    Qt::MouseButton pressedButton = event->button();
    switch (pressedButton)
    {
    case Qt::LeftButton:
        for (auto& anyArrow : *_model)
        {
            pointSetter = std::any_cast<Arrow>(&anyArrow)->setterToPoint(event->localPos());
            if (pointSetter != nullptr)
                break;
        }
        break;
    case Qt::RightButton:
        _model->clearData();
        emit switchController(id::inputController);
        break;
    }
}

void coons::DrawingController::myMouseMoveEvent(QMouseEvent *event)
{
    if (pointSetter != nullptr)
    {
        pointSetter(event->localPos());
        emit updatePainting();
    }
}

void coons::DrawingController::myMouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
    pointSetter = nullptr;
}

void coons::DrawingController::myKeyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event);
}

void coons::DrawingController::myResizeEvent(QResizeEvent *event)
{
	Q_UNUSED(event);
}

void coons::DrawingController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
    Q_UNUSED(event);
    for (auto& anyArrow : _model->getData())
        std::any_cast<Arrow>(&anyArrow)->drawArrow(painter);

	QVector<QPointF> points;

	for (int i = 0; i < _model->size() - 1; ++i)
	{

		Model *model1 = new Model();
		model1->appendData(_model->get<Arrow>(i));
		model1->appendData(_model->get<Arrow>(i + 1));

		coonsEquation.setModel(model1);

		points.append(std::move(calc.calculatePoints(coonsEquation.getXFunction(),
			coonsEquation.getYFunction(),
			coonsEquation.getStart(),
			coonsEquation.getStop(),
			coonsEquation.getResolution())));

	}

	Model *model2 = new Model();
	model2->appendData(_model->get<Arrow>(_model->size() - 1));
	model2->appendData(_model->get<Arrow>(0));

	coonsEquation.setModel(model2);

	points.append(calc.calculatePoints(coonsEquation.getXFunction(),
									   coonsEquation.getYFunction(),
									   coonsEquation.getStart(),
									   coonsEquation.getStop(),
									   coonsEquation.getResolution()));

    QPainterPath path(points[0]);
    for (int i = 1; i < points.size(); ++i)
        path.lineTo(points[i]);

    painter.drawPath(path);
}

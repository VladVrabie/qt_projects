#include "Coons/coons_inputcontroller.h"

coons::InputController::InputController(QObject *parent, Model *model)
    : Controller(parent, model)
{

}

coons::InputController::~InputController()
{

}

void coons::InputController::myMousePressEvent(QMouseEvent *event)
{
    Qt::MouseButton pressedButton = event->button();
    switch (pressedButton)
    {
    case Qt::LeftButton:
        setArrowPoints(event->localPos());
        break;
    case Qt::MiddleButton:
        _model->clearData();
        arrow.setToNull();
        break;
    case Qt::RightButton:
        emit switchController(id::drawingController);
		break;
    }
    emit updatePainting();
}

void coons::InputController::myMouseMoveEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void coons::InputController::myMouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void coons::InputController::myKeyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event);
}

void coons::InputController::myResizeEvent(QResizeEvent *event)
{
	Q_UNUSED(event);
}

void coons::InputController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
    Q_UNUSED(event);
    for (auto& anyArrow : _model->getData())
        std::any_cast<Arrow>(&anyArrow)->drawArrow(painter, false);
}

void coons::InputController::setArrowPoints(const QPointF &position)
{
    if(arrow.isNull() == true)
    {
        arrow.setStart(position);
    }
    else
    {
        arrow.setStop(position);
        _model->appendData(arrow);
        arrow.setToNull();
    }
}

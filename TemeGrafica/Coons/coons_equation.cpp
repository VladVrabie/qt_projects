#include "Coons/coons_equation.h"

coons::Equation::Equation(Model *model)
    : _model{model}
{

}

coons::Equation::~Equation()
{

}

void coons::Equation::setModel(Model *model)
{
    this->_model = model;
}

double coons::Equation::computeX(double t) const
{
	double xa = _model->getP<Arrow>(0)->x1();
	double xb = _model->getP<Arrow>(1)->x1();
    double ax = _model->getP<Arrow>(0)->dx();
    double bx = _model->getP<Arrow>(1)->dx();
    return f1(t) * xa + f2(t) * xb +
            f3(t) * ax + f4(t) * bx;
}

double coons::Equation::computeY(double t) const
{
	double ya = _model->getP<Arrow>(0)->y1();
	double yb = _model->getP<Arrow>(1)->y1();
	double ay = _model->getP<Arrow>(0)->dy();
	double by = _model->getP<Arrow>(1)->dy();
    return f1(t) * ya + f2(t) * yb +
            f3(t) * ay + f4(t) * by;
}

std::function<double (double)> coons::Equation::getXFunction()
{
    std::function<double(double)> fPtr = [this](double t){
        return this->computeX(t);
    };
    return fPtr;
}

std::function<double (double)> coons::Equation::getYFunction()
{
    std::function<double(double)> fPtr = [this](double t){
        return this->computeY(t);
    };
    return fPtr;
}

double coons::Equation::getStart() const
{
    return 0.0;
}

double coons::Equation::getStop() const
{
    return 1.0;
}

unsigned coons::Equation::getResolution() const
{
    return 500u;
}

double coons::Equation::f1(double t) const
{
    double t2 = t*t;
    double t3 = t2*t;
    return 2.0 * t3 - 3.0 * t2 + 1.0;
}

double coons::Equation::f2(double t) const
{
    double t2 = t*t;
    double t3 = t2*t;
    return - 2.0 * t3 + 3.0 * t2;
}

double coons::Equation::f3(double t) const
{
    double t2 = t*t;
    double t3 = t2*t;
    return t3 - 2.0 * t2 + t;
}

double coons::Equation::f4(double t) const
{
    double t2 = t*t;
    double t3 = t2*t;
    return t3 - t2;
}

#ifndef COONS_EQUATION_H
#define COONS_EQUATION_H

#include "model.h"
#include "arrow.h"

namespace coons
{

    class Equation
    {
    public:
        Equation(Model *model = nullptr);
        ~Equation();

        void setModel(Model *model);

        std::function<double(double)> getXFunction();
        std::function<double(double)> getYFunction();

        double computeX(double) const;
        double computeY(double) const;

        double getStart() const;
        double getStop() const;
        unsigned getResolution() const;

    private:
        double f1(double) const;
        double f2(double) const;
        double f3(double) const;
        double f4(double) const;


        Model *_model;
    };

}
#endif // COONS_EQUATION_H

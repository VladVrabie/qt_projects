HEADERS += \
    $$PWD/coons_equation.h \
    $$PWD/coons_inputcontroller.h \
    $$PWD/coons_drawingcontroller.h \
    $$PWD/coons_frame.h \
    $$PWD/coons_controllerids.h

SOURCES += \
    $$PWD/coons_equation.cpp \
    $$PWD/coons_inputcontroller.cpp \
    $$PWD/coons_drawingcontroller.cpp \
    $$PWD/coons_frame.cpp

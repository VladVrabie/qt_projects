#ifndef COONS_CONTROLLERIDS_H
#define COONS_CONTROLLERIDS_H

#include <string>

namespace coons::id
{
	inline std::string inputController{ "inputController" };
	inline std::string drawingController{ "drawingController" };
}

#endif // COONS_CONTROLLERIDS_H

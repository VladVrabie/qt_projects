#ifndef COONS_DRAWINGCONTROLLER_H
#define COONS_DRAWINGCONTROLLER_H

#include "controller.h"
#include "arrow.h"
#include "curvepointscalculator.h"
#include "Coons/coons_equation.h"
#include "Coons/coons_controllerids.h"

namespace coons
{

    class DrawingController : public Controller
    {
        Q_OBJECT

    public:
        explicit DrawingController(QObject *parent = nullptr, Model *model = nullptr);
        ~DrawingController();

        // Controller interface
        virtual void myMousePressEvent(QMouseEvent *event) override;
        virtual void myMouseMoveEvent(QMouseEvent *event) override;
        virtual void myMouseReleaseEvent(QMouseEvent *event) override;
        virtual void myKeyPressEvent(QKeyEvent *event) override;
		virtual void myResizeEvent(QResizeEvent *event) override;
		virtual void myPaintEvent(QPaintEvent *event, QPainter &painter) override;

    private:
        CurvePointsCalculator calc;
        Equation coonsEquation;
        Arrow::setterFPtr pointSetter = nullptr;
    };

}

#endif // COONS_DRAWINGCONTROLLER_H

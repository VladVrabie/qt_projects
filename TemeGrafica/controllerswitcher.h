#ifndef CONTROLLERSWITCHER_H
#define CONTROLLERSWITCHER_H

#include <QObject>

#include <map>
#include <tuple>
#include <utility>
#include <algorithm>
#include <functional>
#include "controller.h"

class ControllerSwitcher : public QObject
{
    Q_OBJECT

public:
    class ConnectionArgs
    {
    public:
        ConnectionArgs();
        ConnectionArgs(const QObject *sender, const char *signal,
                       const QObject *receiver, const char *method);
		ConnectionArgs(const ConnectionArgs &other);
        ~ConnectionArgs();

        auto getSender() const;
        auto getSignal() const;
        auto getReceiver() const;
        auto getMethod() const;

        void setSender(const QObject *sender);
        void setSignal(const char *signal);
        void setReceiver(const QObject *receiver);
        void setMethod(const char *method);

        auto asTuple() const -> std::tuple<const QObject *, const char *, const QObject *, const char *, Qt::ConnectionType>;

        friend bool operator== (const ConnectionArgs &lhs, const ConnectionArgs &rhs);

    private:
        const QObject *_sender = nullptr;
        std::string _signal;
        const QObject *_receiver = nullptr;
        std::string _method;
	};

public:
    explicit ControllerSwitcher(QObject *parent = nullptr);
    ~ControllerSwitcher();

	// Add a new controller to be managed by this switcher. If the id is already used, function does nothing
    void addController(const std::string &id, Controller *controller);

	// Add a new connection to a controller when switched to. If the id is unknown or the arguments are already present, function does nothing
    void addConnection(const std::string &idController, const ConnectionArgs &arguments);

//    void removeController(const std::string &id);
//    void removeConnection(const std::string &idController, const ConnectionArgs &arguments);

    //void setModelToAll(Model *model);

signals:
    void controllerChanged(Controller *newController);

public slots:
	// Switches to the parameter controller. If the id is unknown, function does nothing
	void switchToController(std::string idController);

private:
    std::string currentController;
    std::map<std::string, Controller *> controllers;
    std::map<std::string, std::vector<ConnectionArgs>> connections;
};

#endif // CONTROLLERSWITCHER_H

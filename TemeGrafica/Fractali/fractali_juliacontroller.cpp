#include "Fractali/fractali_juliacontroller.h"

fractali::JuliaController::JuliaController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void fractali::JuliaController::myMousePressEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::JuliaController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::JuliaController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::JuliaController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void fractali::JuliaController::myResizeEvent(QResizeEvent * event)
{
	(*_model)[1] = event->size().width();
	(*_model)[2] = event->size().height();
}

void fractali::JuliaController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);

	unsigned nbColors = _model->get<unsigned>(0);
	nbColors = nbColors > 2u ? nbColors : 2u; // minim 2
	int width = _model->get<int>(1);
	int height = _model->get<int>(2);

	populateVectorOfColors(nbColors);

	for (int i = 0; i < width; ++i)
		for (int j = 0; j < height; ++j)
		{
			std::complex<double> current = createComplex(i, j, width, height);
			int n = testComplex(nbColors - 1u, 2.0, current, std::complex<double>(-0.4, 0.6));

			painter.setPen(_colors[n]);
			painter.drawPoint(i, j);
		}
}

void fractali::JuliaController::populateVectorOfColors(unsigned nbColors)
{
	_colors.clear();
	_colors.push_front(QColor{ Qt::black });

	nbColors -= 2u;
	for (unsigned i = 0u; i < nbColors; ++i)
	{
		_colors.push_front(QColor::fromHslF(static_cast<double>(i) / nbColors, 1.0, 0.5));
	}

	_colors.push_front(QColor{ Qt::white });
}

std::complex<double> fractali::JuliaController::createComplex(int x, int y, int width, int height) const
{
	return std::complex<double>((4.0 * x) / width - 2.0, (2.0 * y) / height - 1.0);
}

int fractali::JuliaController::testComplex(int nMax, double s, std::complex<double> z, std::complex<double> c)
{
	int i = 0;
	for (; i < nMax; ++i)
	{
		z = complexFunction(z, c);
		if (std::abs(z) > s)
			break;
	}

	return i;
}

std::complex<double> fractali::JuliaController::complexFunction(std::complex<double> z, std::complex<double> c)
{
	return z * z + c;
}

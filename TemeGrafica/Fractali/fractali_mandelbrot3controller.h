#ifndef FRACTALI_MANDELBROT3CONTROLLER_H
#define FRACTALI_MANDELBROT3CONTROLLER_H

#include "Fractali/fractali_mandelbrotcontroller.h"

namespace fractali
{

	class Mandelbrot3Controller : public MandelbrotController
	{
	public:
		explicit Mandelbrot3Controller(QObject *parent = nullptr, Model *model = nullptr);
		~Mandelbrot3Controller() = default;

	protected:
		virtual std::complex<double> complexFunction(std::complex<double> z, std::complex<double> c) override;
	};

}

#endif // FRACTALI_MANDELBROT3CONTROLLER_H
#include "Fractali/fractali_cantorcontroller.h"


fractali::CantorController::CantorController(QObject *parent, Model *model)
    : Controller(parent, model)
{
}

void fractali::CantorController::myMousePressEvent(QMouseEvent *event)
{
	Q_UNUSED(event);
}

void fractali::CantorController::myMouseMoveEvent(QMouseEvent *event)
{
	Q_UNUSED(event);
}

void fractali::CantorController::myMouseReleaseEvent(QMouseEvent *event)
{
	Q_UNUSED(event);
}

void fractali::CantorController::myKeyPressEvent(QKeyEvent *event)
{
	Q_UNUSED(event);
}

void fractali::CantorController::myResizeEvent(QResizeEvent *event)
{
	Q_UNUSED(event);
}

void fractali::CantorController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
	Q_UNUSED(event);
	unsigned nbRecursions = _model->get<unsigned>(0);
	double length = _model->get<double>(1);

	_turtle.set(100.0, 300.0);
	draw(painter, length, nbRecursions);	
}

void fractali::CantorController::draw(QPainter & painter, double length, unsigned nbRecursions)
{
	if (nbRecursions == 0u)
	{
		_turtle.moveAhead(painter, length);
		return;
	}

	length /= 3.0;
	
	draw(painter, length, nbRecursions - 1);
	_turtle.moveAhead(painter, length, false);
	draw(painter, length, nbRecursions - 1);
}

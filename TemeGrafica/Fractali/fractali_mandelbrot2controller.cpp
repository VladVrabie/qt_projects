#include "Fractali/fractali_mandelbrot2controller.h"

fractali::Mandelbrot2Controller::Mandelbrot2Controller(QObject * parent, Model * model)
	: MandelbrotController(parent, model)
{
}

std::complex<double> fractali::Mandelbrot2Controller::complexFunction(std::complex<double> z, std::complex<double> c)
{
	auto z2 = z * z;
	return z2 * z2 + c;
}

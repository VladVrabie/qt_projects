#ifndef FRACTALI_DRAGONCONTROLLER_H
#define FRACTALI_DRAGONCONTROLLER_H

#include "controller.h"
#include "Fractali/fractali_turtle.h"

namespace fractali
{

	class DragonController : public Controller
	{
	public:
		explicit DragonController(QObject *parent = nullptr, Model *model = nullptr);
		~DragonController() = default;

		// Controller interface
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;

	private:
		void draw(QPainter &painter, double length, unsigned nbRecursions, int sign);

		Turtle _turtle;
	};

}

#endif // FRACTALI_DRAGONCONTROLLER_H
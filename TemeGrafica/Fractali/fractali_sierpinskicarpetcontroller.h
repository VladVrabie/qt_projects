#ifndef FRACTALI_SIERPINSKICARPETCONTROLLER_H
#define FRACTALI_SIERPINSKICARPETCONTROLLER_H

#include <QBrush>
#include <QPen>
#include "controller.h"

namespace fractali
{

	class SierpinskiCarpetController : public Controller
	{
	public:
		explicit SierpinskiCarpetController(QObject *parent = nullptr, Model *model = nullptr);
		~SierpinskiCarpetController() = default;

		// Controller interface
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;

	private:
		void draw(QPainter &painter, double topLeftX, double topLeftY, double width, double height, unsigned nbRecursions);

		double _width = 200.0;
		double _height = 200.0;
	};

}

#endif // FRACTALI_SIERPINSKICARPETCONTROLLER_H
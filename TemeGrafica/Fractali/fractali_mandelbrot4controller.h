#ifndef FRACTALI_MANDELBROT4CONTROLLER_H
#define FRACTALI_MANDELBROT4CONTROLLER_H

#include "Fractali/fractali_mandelbrotcontroller.h"

namespace fractali
{

	class Mandelbrot4Controller : public MandelbrotController
	{
	public:
		explicit Mandelbrot4Controller(QObject *parent = nullptr, Model *model = nullptr);
		~Mandelbrot4Controller() = default;

	protected:
		virtual std::complex<double> complexFunction(std::complex<double> z, std::complex<double> c) override;
	};

}

#endif // FRACTALI_MANDELBROT4CONTROLLER_H
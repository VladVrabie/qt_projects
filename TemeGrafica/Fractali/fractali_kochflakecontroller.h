#ifndef FRACTALI_KOCHFLAKECONTROLLER_H
#define FRACTALI_KOCHFLAKECONTROLLER_H

#include "controller.h"
#include "Fractali/fractali_turtle.h"

namespace fractali
{

	class KochFlakeController : public Controller
	{
	public:
		explicit KochFlakeController(QObject *parent = nullptr, Model *model = nullptr);
		~KochFlakeController() = default;

		// Controller interface
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;

	private:
		void drawKoch(QPainter &painter, double length, unsigned nbRecursions);
		void draw(QPainter &painter, double length, unsigned nbRecursions);

		Turtle _turtle;
	};

}
#endif // FRACTALI_KOCHFLAKECONTROLLER_H
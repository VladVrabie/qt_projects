#include "fractali_kochflakecontroller.h"

fractali::KochFlakeController::KochFlakeController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void fractali::KochFlakeController::myMousePressEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::KochFlakeController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::KochFlakeController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::KochFlakeController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void fractali::KochFlakeController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void fractali::KochFlakeController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	unsigned nbRecursions = _model->get<unsigned>(0);
	double length = _model->get<double>(1);

	_turtle.set(200.0, 400.0);
	draw(painter, length, nbRecursions);
}

void fractali::KochFlakeController::drawKoch(QPainter & painter, double length, unsigned nbRecursions)
{
	if (nbRecursions == 0u)
	{
		_turtle.moveAhead(painter, length);
		return;
	}

	length /= 3.0;

	drawKoch(painter, length, nbRecursions - 1);
	_turtle.rotateBy(-M_PI / 3.0);
	drawKoch(painter, length, nbRecursions - 1);
	_turtle.rotateBy(2.0 * M_PI / 3.0);
	drawKoch(painter, length, nbRecursions - 1);
	_turtle.rotateBy(-M_PI / 3.0);
	drawKoch(painter, length, nbRecursions - 1);
}

void fractali::KochFlakeController::draw(QPainter & painter, double length, unsigned nbRecursions)
{
	_turtle.rotateBy(-M_PI / 3.0);
	drawKoch(painter, length, nbRecursions);
	_turtle.rotateBy(2.0 * M_PI / 3.0);
	drawKoch(painter, length, nbRecursions);
	_turtle.rotateBy(2.0 * M_PI / 3.0);
	drawKoch(painter, length, nbRecursions);
}
#ifndef FRACTALI_PLANTCONTROLLER_H
#define FRACTALI_PLANTCONTROLLER_H

#include "controller.h"
#include "Fractali/fractali_turtle.h"

namespace fractali
{

	class PlantController : public Controller
	{
	public:
		explicit PlantController(QObject *parent = nullptr, Model *model = nullptr);
		~PlantController() = default;

		// Controller interface
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;

	private:
		void draw(QPainter &painter, double length, unsigned nbRecursions);

		Turtle _turtle;
	};

}

#endif // FRACTALI_PLANTCONTROLLER_H
#include "Fractali/fractali_managertab.h"


fractali::ManagerTab::ManagerTab(QWidget *parent)
    : QWidget(parent)
{
}

void fractali::ManagerTab::childEvent(QChildEvent *event)
{
	if (event->type() == QEvent::ChildAdded ||
		event->type() == QEvent::ChildPolished)
	{
		QObject *objChild = event->child();

		if (tryInitFrame(objChild) == false)
			if (tryConnectComboBox(objChild) == false)
				tryConnectSpinBox(objChild);
	}
}

void fractali::ManagerTab::setController(int index)
{
	switch (index)
	{
	case 0:
		_uModel->getData().resize(2u);
		(*_uModel)[0] = 0u;
		(*_uModel)[1] = 900.0;

		_spinBoxNbRecursions->setValue(0);

		_frame->getSwitcher()->switchToController(id::cantorController);
		break;

	case 1:
		_uModel->getData().resize(2u);
		(*_uModel)[0] = 0u;
		(*_uModel)[1] = 600.0;

		_spinBoxNbRecursions->setValue(0);

		_frame->getSwitcher()->switchToController(id::kochController);
		break;

	case 2:
		_uModel->getData().resize(2u);
		(*_uModel)[0] = 0u;
		(*_uModel)[1] = 400.0;

		_spinBoxNbRecursions->setValue(0);

		_frame->getSwitcher()->switchToController(id::kochFlakeController);
		break;

	case 3:
		_uModel->getData().resize(2u);
		(*_uModel)[0] = 0u;
		(*_uModel)[1] = 450.0;

		_spinBoxNbRecursions->setValue(0);

		_frame->getSwitcher()->switchToController(id::levyController);
		break;

	case 4:
		_uModel->getData().resize(2u);
		(*_uModel)[0] = 0u;
		(*_uModel)[1] = 400.0;

		_spinBoxNbRecursions->setValue(0);

		_frame->getSwitcher()->switchToController(id::dragonController);
		break;

	case 5:
		_uModel->getData().resize(2u);
		(*_uModel)[0] = 1u;
		(*_uModel)[1] = 150.0;

		_spinBoxNbRecursions->setValue(1);

		_frame->getSwitcher()->switchToController(id::plantController);
		break;

	case 6:
		_uModel->getData().resize(2u);
		(*_uModel)[0] = 1u;
		(*_uModel)[1] = 100.0;

		_spinBoxNbRecursions->setValue(1);

		_frame->getSwitcher()->switchToController(id::plantRandomController);
		break;

	case 7:
		_uModel->getData().resize(1u);
		(*_uModel)[0] = 0u;

		_spinBoxNbRecursions->setValue(0);

		_frame->getSwitcher()->switchToController(id::sierpinskiCarpetController);
		break;

	case 8:
		_uModel->getData().resize(3u);
		(*_uModel)[0] = 0u;
		(*_uModel)[1] = QPointF{ 100.0, 400.0 };
		(*_uModel)[2] = 400.0;

		_spinBoxNbRecursions->setValue(0);

		_frame->getSwitcher()->switchToController(id::sierpinskiTriangleController);
		break;

	case 9:
		_uModel->getData().resize(3u);
		(*_uModel)[0] = 10u;
		(*_uModel)[1] = _frame->width(); // atentie la poineri nuli
		(*_uModel)[2] = _frame->height();

		_spinBoxNbRecursions->setValue(10);

		_frame->getSwitcher()->switchToController(id::mandelbrotController);
		break;

	case 10:
		_uModel->getData().resize(3u);
		(*_uModel)[0] = 10u;
		(*_uModel)[1] = _frame->width(); // atentie la poineri nuli
		(*_uModel)[2] = _frame->height();

		_spinBoxNbRecursions->setValue(10);

		_frame->getSwitcher()->switchToController(id::mandelbrot2Controller);
		break;

	case 11:
		_uModel->getData().resize(3u);
		(*_uModel)[0] = 10u;
		(*_uModel)[1] = _frame->width(); // atentie la poineri nuli
		(*_uModel)[2] = _frame->height();

		_spinBoxNbRecursions->setValue(10);

		_frame->getSwitcher()->switchToController(id::mandelbrot3Controller);
		break;

	case 12:
		_uModel->getData().resize(3u);
		(*_uModel)[0] = 10u;
		(*_uModel)[1] = _frame->width(); // atentie la poineri nuli
		(*_uModel)[2] = _frame->height();

		_spinBoxNbRecursions->setValue(10);

		_frame->getSwitcher()->switchToController(id::mandelbrot4Controller);
		break;

	case 13:
		_uModel->getData().resize(3u);
		(*_uModel)[0] = 10u;
		(*_uModel)[1] = _frame->width(); // atentie la poineri nuli
		(*_uModel)[2] = _frame->height();

		_spinBoxNbRecursions->setValue(10);

		_frame->getSwitcher()->switchToController(id::juliaController);
		break;

	default:
		break;
	}
}

void fractali::ManagerTab::setNbRecursions(int value)
{
	(*_uModel)[0] = static_cast<unsigned>(value);
	_frame->update();
}

bool fractali::ManagerTab::tryInitFrame(QObject *child)
{
	MyFrame *myFrame = qobject_cast<MyFrame *>(child);

	if (myFrame != nullptr)
	{
		_frame = myFrame;

		// Cantor Controller
		_cantorController = new CantorController(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::cantorController, _cantorController);

		ControllerSwitcher::ConnectionArgs cantorUpdateArgs{ _cantorController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::cantorController, cantorUpdateArgs);


		// Koch Controller
		_kochController = new KochController(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::kochController, _kochController);

		ControllerSwitcher::ConnectionArgs kochUpdateArgs{ _kochController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::kochController, kochUpdateArgs);


		// Koch Flake Controller
		_kochFlakeController = new KochFlakeController(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::kochFlakeController, _kochFlakeController);

		ControllerSwitcher::ConnectionArgs kochFlakeUpdateArgs{ _kochFlakeController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::kochFlakeController, kochFlakeUpdateArgs);


		// Levy Controller
		_levyController = new LevyController(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::levyController, _levyController);

		ControllerSwitcher::ConnectionArgs levyUpdateArgs{ _levyController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::levyController, levyUpdateArgs);


		// Dragon Controller
		_dragonController = new DragonController(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::dragonController, _dragonController);

		ControllerSwitcher::ConnectionArgs dragonUpdateArgs{ _dragonController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::dragonController, dragonUpdateArgs);


		// Plant Controller
		_plantController = new PlantController(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::plantController, _plantController);

		ControllerSwitcher::ConnectionArgs plantUpdateArgs{ _plantController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::plantController, plantUpdateArgs);


		// Plant Random Controller
		_plantRandomController = new PlantRandomController(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::plantRandomController, _plantRandomController);

		ControllerSwitcher::ConnectionArgs plantRandomUpdateArgs{ _plantRandomController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::plantRandomController, plantRandomUpdateArgs);


		// Sierpinski Carpet Controller
		_sierpinskiCarpetController = new SierpinskiCarpetController(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::sierpinskiCarpetController, _sierpinskiCarpetController);

		ControllerSwitcher::ConnectionArgs sierpinskiCarpetUpdateArgs{ _sierpinskiCarpetController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::sierpinskiCarpetController, sierpinskiCarpetUpdateArgs);


		// Sierpinki Triangle Controller
		_sierpinskiTriangleController = new SierpinskiTriangleController(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::sierpinskiTriangleController, _sierpinskiTriangleController);

		ControllerSwitcher::ConnectionArgs sierpinskiTriangleUpdateArgs{ _sierpinskiTriangleController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::sierpinskiTriangleController, sierpinskiTriangleUpdateArgs);


		// Mandelbrot Controller
		_mandelbrotController = new MandelbrotController(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::mandelbrotController, _mandelbrotController);

		ControllerSwitcher::ConnectionArgs mandelbrotUpdateArgs{ _mandelbrotController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::mandelbrotController, mandelbrotUpdateArgs);
		

		// Mandelbrot 2 Controller
		_mandelbrot2Controller = new Mandelbrot2Controller(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::mandelbrot2Controller, _mandelbrot2Controller);

		ControllerSwitcher::ConnectionArgs mandelbrot2UpdateArgs{ _mandelbrot2Controller, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::mandelbrot2Controller, mandelbrot2UpdateArgs);


		// Mandelbrot 3 Controller
		_mandelbrot3Controller = new Mandelbrot3Controller(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::mandelbrot3Controller, _mandelbrot3Controller);

		ControllerSwitcher::ConnectionArgs mandelbrot3UpdateArgs{ _mandelbrot3Controller, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::mandelbrot3Controller, mandelbrot3UpdateArgs);


		// Mandelbrot 4 Controller
		_mandelbrot4Controller = new Mandelbrot4Controller(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::mandelbrot4Controller, _mandelbrot4Controller);

		ControllerSwitcher::ConnectionArgs mandelbrot4UpdateArgs{ _mandelbrot4Controller, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::mandelbrot4Controller, mandelbrot4UpdateArgs);


		// Julia Controller
		_juliaController = new JuliaController(_frame, _uModel.get());
		_frame->getSwitcher()->addController(id::juliaController, _juliaController);

		ControllerSwitcher::ConnectionArgs juliaUpdateArgs{ _juliaController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::juliaController, juliaUpdateArgs);


		// Initializing data in order to switch to CantorController
		_uModel->getData().resize(2u);
		(*_uModel)[0] = 0u;
		(*_uModel)[1] = 900.0;

		_frame->getSwitcher()->switchToController(id::cantorController);

		return true;
	}
	return false;
}

bool fractali::ManagerTab::tryConnectComboBox(QObject *child)
{
	QComboBox *comboBoxFractali = qobject_cast<QComboBox *>(child);

	if (comboBoxFractali != nullptr)
	{
		connect(comboBoxFractali, QOverload<int>::of(&QComboBox::currentIndexChanged),
				this, &ManagerTab::setController);

		return true;
	}
	return false;
}

bool fractali::ManagerTab::tryConnectSpinBox(QObject *child)
{
	QSpinBox *spinBoxNbRecursions = qobject_cast<QSpinBox *>(child);

	if (spinBoxNbRecursions != nullptr)
	{
		_spinBoxNbRecursions = spinBoxNbRecursions;

		connect(_spinBoxNbRecursions, QOverload<int>::of(&QSpinBox::valueChanged),
				this, &ManagerTab::setNbRecursions);

		return true;
	}
	return false;
}

#ifndef FRACTALI_CONTROLLERIDS_H
#define FRACTALI_CONTROLLERIDS_H

#include <string>

namespace fractali::id
{
	inline std::string cantorController{ "cantorController" };
	inline std::string kochController{ "kochController" };
	inline std::string kochFlakeController{ "kochFlakeController" };
	inline std::string levyController{ "levyController" };
	inline std::string dragonController{ "dragonController" };
	inline std::string plantController{ "plantController" };
	inline std::string plantRandomController{ "plantRandomController" };
	inline std::string sierpinskiCarpetController{ "sierpinskiCarpetController" };
	inline std::string sierpinskiTriangleController{ "sierpinskiTriangleController" };
	inline std::string mandelbrotController{ "mandelbrotController" };
	inline std::string mandelbrot2Controller{ "mandelbrot2Controller" };
	inline std::string mandelbrot3Controller{ "mandelbrot3Controller" };
	inline std::string mandelbrot4Controller{ "mandelbrot4Controller" };
	inline std::string juliaController{ "juliaController" };

}

#endif // FRACTALI_CONTROLLERIDS_H

#include "fractali_mandelbrotcontroller.h"

fractali::MandelbrotController::MandelbrotController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void fractali::MandelbrotController::myMousePressEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::MandelbrotController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::MandelbrotController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::MandelbrotController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void fractali::MandelbrotController::myResizeEvent(QResizeEvent * event)
{
	(*_model)[1] = event->size().width();
	(*_model)[2] = event->size().height();
}

void fractali::MandelbrotController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);

	unsigned nbColors = _model->get<unsigned>(0);
	nbColors = nbColors > 2u ? nbColors : 2u; // minim 2
	int width = _model->get<int>(1);
	int height = _model->get<int>(2);

	populateVectorOfColors(nbColors);

	for (int i = 0; i < width; ++i)
		for (int j = 0; j < height; ++j)
		{
			std::complex<double> current = createComplex(i, j, width, height);
			int n = testComplex(current, nbColors - 1u, 2.0); // nu sunt sigur ce parametri vin

			painter.setPen(_colors[n]);
			painter.drawPoint(i, j);
		}
}

void fractali::MandelbrotController::populateVectorOfColors(unsigned nbColors)
{
	_colors.clear();
	_colors.push_front(QColor{ Qt::black });

	nbColors -= 2u;
	for (unsigned i = 0u; i < nbColors; ++i)
	{
		_colors.push_front(QColor::fromHslF(static_cast<double>(i) / nbColors, 1.0, 0.5));
	}

	_colors.push_front(QColor{ Qt::white });
}

std::complex<double> fractali::MandelbrotController::createComplex(int x, int y, int width, int height) const
{
	return std::complex<double>((3.0 * x) / width - 2.0, (3.0 * y) / height - 1.5);
}

int fractali::MandelbrotController::testComplex(std::complex<double> nb, int nMax, double s)
{
	std::complex<double> anterior; // (0, 0)

	int i = 0;
	for (; i < nMax; ++i)
	{
		anterior = complexFunction(anterior, nb);
		if (std::abs(anterior) > s)
			break;
	}

	return i;
}

std::complex<double> fractali::MandelbrotController::complexFunction(std::complex<double> z, std::complex<double> c)
{
	auto z2 = z * z;
	return z2 + c;
}
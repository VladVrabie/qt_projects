#include "Fractali/fractali_mandelbrot4controller.h"

fractali::Mandelbrot4Controller::Mandelbrot4Controller(QObject * parent, Model * model)
	: MandelbrotController(parent, model)
{
}

std::complex<double> fractali::Mandelbrot4Controller::complexFunction(std::complex<double> z, std::complex<double> c)
{
	auto z2 = z * z;
	return z2 * z2 + z2 + c;
}

#include "fractali_sierpinskitrianglecontroller.h"

fractali::SierpinskiTriangleController::SierpinskiTriangleController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void fractali::SierpinskiTriangleController::myMousePressEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::SierpinskiTriangleController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::SierpinskiTriangleController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::SierpinskiTriangleController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void fractali::SierpinskiTriangleController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void fractali::SierpinskiTriangleController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	unsigned nbRecursions = _model->get<unsigned>(0);
	QPointF startPoint = _model->get<QPointF>(1);
	double length = _model->get<double>(2);
	painter.save();

	QBrush redBrush{ Qt::red };
	QBrush yellowBrush{ Qt::yellow };

	painter.setBrush(redBrush);
	painter.setPen(QColor{ Qt::red });
	
	QPolygonF triangle;
	triangle << startPoint << QPointF{startPoint.x() + length / 2.0, startPoint.y() - length * sqrt3 / 2.0}
			 << QPointF{ startPoint.x() + length, startPoint.y() };

	painter.drawPolygon(triangle);

	painter.setBrush(yellowBrush);
	painter.setPen(QColor{ Qt::yellow });
	draw(painter, startPoint, length, nbRecursions);

	painter.restore();
}

void fractali::SierpinskiTriangleController::draw(QPainter & painter, const QPointF &startPoint, double length, unsigned nbRecursions)
{
	if (nbRecursions == 0)
		return;

	QPointF start1{ startPoint.x() + length / 4.0, startPoint.y() - length * sqrt3 / 4.0 };
	QPointF auxPoint{ startPoint.x() + 3 * length / 4.0, startPoint.y() - length * sqrt3 / 4.0 };
	QPointF start3{ startPoint.x() + length / 2.0, startPoint.y() };

	QPolygonF triangle;
	triangle << start1 << auxPoint << start3;

	painter.drawPolygon(triangle);

	length /= 2.0;
	draw(painter, start1, length, nbRecursions - 1);
	draw(painter, startPoint, length, nbRecursions - 1);
	draw(painter, start3, length, nbRecursions - 1);
}

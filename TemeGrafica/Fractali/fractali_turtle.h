#ifndef FRACTALI_TURTLE_H
#define FRACTALI_TURTLE_H

#include <QPainter>
#include <QtMath>

namespace fractali
{

    class Turtle
    {
    public:
		Turtle(double x = 0.0, double y = 0.0, double angle = 0.0);
        ~Turtle() = default;

		void rotateBy(double angle);
		void moveAhead(QPainter &painter, double length, bool trail = true);

		void set(double x = 0.0, double y = 0.0, double angle = 0.0);

    private:
		double _x;
		double _y;
		double _angle;
    };

}

#endif // FRACTALI_TURTLE_H

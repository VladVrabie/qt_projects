#include "fractali_dragoncontroller.h"

fractali::DragonController::DragonController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void fractali::DragonController::myMousePressEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::DragonController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::DragonController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::DragonController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void fractali::DragonController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void fractali::DragonController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	unsigned nbRecursions = _model->get<unsigned>(0);
	double length = _model->get<double>(1);

	_turtle.set(300.0, 400.0);
	draw(painter, length, nbRecursions, 1); // -1 il deseneaza simetric
}

void fractali::DragonController::draw(QPainter & painter, double length, unsigned nbRecursions, int sign)
{
	if (nbRecursions == 0)
	{
		_turtle.moveAhead(painter, length);
		return;
	}

	length *= M_SQRT2 / 2.0;

	_turtle.rotateBy(-M_PI_4 * sign);
	draw(painter, length, nbRecursions - 1, 1);
	_turtle.rotateBy(M_PI_2 * sign);
	draw(painter, length, nbRecursions - 1, -1);
	_turtle.rotateBy(-M_PI_4 * sign);
}

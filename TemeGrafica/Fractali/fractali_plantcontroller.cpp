#include "fractali_plantcontroller.h"

fractali::PlantController::PlantController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void fractali::PlantController::myMousePressEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::PlantController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::PlantController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::PlantController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void fractali::PlantController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void fractali::PlantController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	unsigned nbRecursions = _model->get<unsigned>(0);
	double length = _model->get<double>(1);

	_turtle.set(300.0, 450.0, -M_PI_2);
	draw(painter, length, nbRecursions);
}

void fractali::PlantController::draw(QPainter & painter, double length, unsigned nbRecursions)
{
	if (nbRecursions == 0)
		return;
	
	_turtle.moveAhead(painter, length);

	Turtle turtleCopy = _turtle;

	_turtle.rotateBy(- M_PI / 6.0);
	draw(painter, 2.0 * length / 3.0, nbRecursions - 1);

	_turtle = turtleCopy;

	_turtle.rotateBy(M_PI / 6.0);
	draw(painter, 2.0 * length / 3.0, nbRecursions - 1);
}

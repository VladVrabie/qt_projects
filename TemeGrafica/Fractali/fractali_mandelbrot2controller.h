#ifndef FRACTALI_MANDELBROT2CONTROLLER_H
#define FRACTALI_MANDELBROT2CONTROLLER_H

#include "Fractali/fractali_mandelbrotcontroller.h"

namespace fractali
{

	class Mandelbrot2Controller : public MandelbrotController
	{
	public:
		explicit Mandelbrot2Controller(QObject *parent = nullptr, Model *model = nullptr);
		~Mandelbrot2Controller() = default;

	protected:
		virtual std::complex<double> complexFunction(std::complex<double> z, std::complex<double> c) override;
	};

}

#endif // FRACTALI_MANDELBROT2CONTROLLER_H
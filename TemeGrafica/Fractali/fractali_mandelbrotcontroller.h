#ifndef FRACTALI_MANDELBROTCONTROLLER_H
#define FRACTALI_MANDELBROTCONTROLLER_H

#include <QVector>
#include <QColor>

#include <complex>
#include "controller.h"

namespace fractali
{

	class MandelbrotController : public Controller
	{
	public:
		explicit MandelbrotController(QObject *parent = nullptr, Model *model = nullptr);
		~MandelbrotController() = default;

		// Controller interface
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;

	protected:
		virtual std::complex<double> complexFunction(std::complex<double> z, std::complex<double> c);
	
	private:
	 	void populateVectorOfColors(unsigned nbColors);
		std::complex<double> createComplex(int x, int y, int width, int height) const;
		int testComplex(std::complex<double> nb, int nMax, double s);



		QVector<QColor> _colors;
	};


}

#endif // FRACTALI_MANDELBROTCONTROLLER_H
#include "fractali_levycontroller.h"

fractali::LevyController::LevyController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void fractali::LevyController::myMousePressEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::LevyController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::LevyController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::LevyController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void fractali::LevyController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void fractali::LevyController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	unsigned nbRecursions = _model->get<unsigned>(0);
	double length = _model->get<double>(1);

	_turtle.set(300.0, 475.0);
	draw(painter, length, nbRecursions);
}

void fractali::LevyController::draw(QPainter & painter, double length, unsigned nbRecursions)
{
	if (nbRecursions == 0)
	{
		_turtle.moveAhead(painter, length);
		return;
	}

	length *= M_SQRT2 / 2.0;

	_turtle.rotateBy(- M_PI_4);
	draw(painter, length, nbRecursions - 1);
	_turtle.rotateBy(M_PI_2);
	draw(painter, length, nbRecursions - 1);
	_turtle.rotateBy(- M_PI_4);
}

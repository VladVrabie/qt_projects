HEADERS += \
    $$PWD/fractali_managertab.h \
    $$PWD/fractali_controllerids.h \
    $$PWD/fractali_turtle.h \
    $$PWD/fractali_cantorcontroller.h \
    $$PWD/fractali_kochcontroller.h \
    $$PWD/fractali_kochflakecontroller.h \
    $$PWD/fractali_levycontroller.h \
    $$PWD/fractali_dragoncontroller.h \
    $$PWD/fractali_plantcontroller.h \
    $$PWD/fractali_plantrandomcontroller.h \
    $$PWD/fractali_sierpinskicarpetcontroller.h \
    $$PWD/fractali_sierpinskitrianglecontroller.h \
    $$PWD/fractali_mandelbrotcontroller.h \
    $$PWD/fractali_mandelbrot2controller.h \
    $$PWD/fractali_mandelbrot3controller.h \
    $$PWD/fractali_mandelbrot4controller.h \
    $$PWD/fractali_juliacontroller.h

SOURCES += \
    $$PWD/fractali_managertab.cpp \
    $$PWD/fractali_turtle.cpp \
    $$PWD/fractali_cantorcontroller.cpp \
    $$PWD/fractali_kochcontroller.cpp \
    $$PWD/fractali_kochflakecontroller.cpp \
    $$PWD/fractali_levycontroller.cpp \
    $$PWD/fractali_dragoncontroller.cpp \
    $$PWD/fractali_plantcontroller.cpp \
    $$PWD/fractali_plantrandomcontroller.cpp \
    $$PWD/fractali_sierpinskicarpetcontroller.cpp \
    $$PWD/fractali_sierpinskitrianglecontroller.cpp \
    $$PWD/fractali_mandelbrotcontroller.cpp \
    $$PWD/fractali_mandelbrot2controller.cpp \
    $$PWD/fractali_mandelbrot3controller.cpp \
    $$PWD/fractali_mandelbrot4controller.cpp \
    $$PWD/fractali_juliacontroller.cpp

#include "Fractali/fractali_turtle.h"

fractali::Turtle::Turtle(double x, double y, double angle)
	: _x{ x }, _y{ y }, _angle{ angle }
{
}

void fractali::Turtle::rotateBy(double angle)
{
	_angle += angle;
}

void fractali::Turtle::moveAhead(QPainter & painter, double length, bool trail)
{
	double x2 = _x + length * qCos(_angle);
	double y2 = _y + length * qSin(_angle);

	if (trail == true)
		painter.drawLine(_x, _y, x2, y2);

	_x = x2;
	_y = y2;
}

void fractali::Turtle::set(double x, double y, double angle)
{
	_x = x;
	_y = y;
	_angle = angle;
}

#include "Fractali/fractali_kochcontroller.h"

fractali::KochController::KochController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void fractali::KochController::myMousePressEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::KochController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::KochController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::KochController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void fractali::KochController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void fractali::KochController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	unsigned nbRecursions = _model->get<unsigned>(0);
	double length = _model->get<double>(1);

	_turtle.set(100.0, 300.0);
	draw(painter, length, nbRecursions);
}

void fractali::KochController::draw(QPainter & painter, double length, unsigned nbRecursions)
{
	if (nbRecursions == 0u)
	{
		_turtle.moveAhead(painter, length);
		return;
	}

	length /= 3.0;

	draw(painter, length, nbRecursions - 1);
	_turtle.rotateBy(- M_PI / 3.0);
	draw(painter, length, nbRecursions - 1);
	_turtle.rotateBy(2.0 * M_PI / 3.0);
	draw(painter, length, nbRecursions - 1);
	_turtle.rotateBy(- M_PI / 3.0);
	draw(painter, length, nbRecursions - 1);
}
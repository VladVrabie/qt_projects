#include "fractali_plantrandomcontroller.h"

fractali::PlantRandomController::PlantRandomController(QObject * parent, Model * model)
	: Controller(parent, model), _mt(_rd())
{
}

void fractali::PlantRandomController::myMousePressEvent(QMouseEvent * event)
{
	Qt::MouseButton pressedButton = event->button();
	if (pressedButton == Qt::LeftButton)
		emit updatePainting();
}

void fractali::PlantRandomController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::PlantRandomController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::PlantRandomController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void fractali::PlantRandomController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void fractali::PlantRandomController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	unsigned nbRecursions = _model->get<unsigned>(0);
	double length = _model->get<double>(1);

	_turtle.set(400.0, 500.0, -M_PI_2);
	draw(painter, length, nbRecursions);
}

void fractali::PlantRandomController::draw(QPainter & painter, double length, unsigned nbRecursions)
{
	if (nbRecursions == 0)
		return;

	_turtle.moveAhead(painter, length);

	Turtle turtleCopy = _turtle;

	 unsigned nbBranches = _mt() % 4 + 2;

	 for (unsigned i = 0u; i < nbBranches; ++i)
	 {
		 _turtle = turtleCopy;

		 double angle = (_mt() / static_cast<double>(UINT32_MAX) * 2.0 - 1.0) * M_PI_4;
		 _turtle.rotateBy(angle);

		 double newLength = (_mt() / static_cast<double>(UINT32_MAX) / 2.0 + 0.5) * length;
		 draw(painter, newLength, nbRecursions - 1);
	 }
}

#include "fractali_mandelbrot3controller.h"

fractali::Mandelbrot3Controller::Mandelbrot3Controller(QObject * parent, Model * model)
	: MandelbrotController(parent, model)
{
}

std::complex<double> fractali::Mandelbrot3Controller::complexFunction(std::complex<double> z, std::complex<double> c)
{
	auto z2 = z * z;
	return z2 * z2 + z2 * z + c;
}

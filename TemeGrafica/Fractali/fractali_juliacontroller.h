#ifndef FRACTALI_JULIACONTROLLER_H
#define FRACTALI_JULIACONTROLLER_H

#include <QVector>
#include <QColor>

#include <complex>
#include "controller.h"

namespace fractali
{

	class JuliaController : public Controller
	{
	public:
		explicit JuliaController(QObject *parent = nullptr, Model *model = nullptr);
		~JuliaController() = default;

		// Controller interface
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;


	private:
		void populateVectorOfColors(unsigned nbColors);
		std::complex<double> createComplex(int x, int y, int width, int height) const;
		int testComplex(int nMax, double s, std::complex<double> z, std::complex<double> c);
		std::complex<double> complexFunction(std::complex<double> z, std::complex<double> c);

		QVector<QColor> _colors;
	};

}

#endif // FRACTALI_JULIACONTROLLER_H
#ifndef FRACTALI_MANAGERTAB_H
#define FRACTALI_MANAGERTAB_H

#include <QChildEvent>
#include <QComboBox>
#include <QSpinBox>

#include <memory>
#include "myframe.h"
#include "model.h"
#include "Fractali/fractali_controllerids.h"
#include "Fractali/fractali_cantorcontroller.h"
#include "Fractali/fractali_kochcontroller.h"
#include "Fractali/fractali_kochflakecontroller.h"
#include "Fractali/fractali_levycontroller.h"
#include "Fractali/fractali_dragoncontroller.h"
#include "Fractali/fractali_plantcontroller.h"
#include "Fractali/fractali_plantrandomcontroller.h"
#include "Fractali/fractali_sierpinskicarpetcontroller.h"
#include "Fractali/fractali_sierpinskitrianglecontroller.h"
#include "Fractali/fractali_mandelbrotcontroller.h"
#include "Fractali/fractali_mandelbrot2controller.h"
#include "Fractali/fractali_mandelbrot3controller.h"
#include "Fractali/fractali_mandelbrot4controller.h"
#include "Fractali/fractali_juliacontroller.h"

namespace fractali
{

    class ManagerTab : public QWidget
    {
        Q_OBJECT

    public:
        explicit ManagerTab(QWidget *parent = nullptr);
        ~ManagerTab() = default;

	protected:
		virtual void childEvent(QChildEvent *event) override;

    signals:

	public slots :
		void setController(int index);
		void setNbRecursions(int value);

    private:
		bool tryInitFrame(QObject *child);
		bool tryConnectComboBox(QObject *child);
		bool tryConnectSpinBox(QObject *child);


		MyFrame * _frame = nullptr;
		QSpinBox *_spinBoxNbRecursions = nullptr;

		CantorController * _cantorController = nullptr;
		KochController *_kochController = nullptr;
		KochFlakeController *_kochFlakeController = nullptr;
		LevyController *_levyController = nullptr;
		DragonController *_dragonController = nullptr;
		PlantController *_plantController = nullptr;
		PlantRandomController *_plantRandomController = nullptr;
		SierpinskiCarpetController *_sierpinskiCarpetController = nullptr;
		SierpinskiTriangleController *_sierpinskiTriangleController = nullptr;
		MandelbrotController *_mandelbrotController = nullptr;
		Mandelbrot2Controller *_mandelbrot2Controller = nullptr;
		Mandelbrot3Controller *_mandelbrot3Controller = nullptr;
		Mandelbrot4Controller *_mandelbrot4Controller = nullptr;
		JuliaController *_juliaController = nullptr;

		std::unique_ptr<Model> _uModel = std::make_unique<Model>();
    };

}

#endif // FRACTALI_MANAGERTAB_H

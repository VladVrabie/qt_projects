#include "fractali_sierpinskicarpetcontroller.h"

fractali::SierpinskiCarpetController::SierpinskiCarpetController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void fractali::SierpinskiCarpetController::myMousePressEvent(QMouseEvent * event)
{
	Qt::MouseButton pressedButton = event->button();

	switch (pressedButton)
	{
	case Qt::LeftButton:
		_width = event->localPos().x();
		_height = event->localPos().y();
		emit updatePainting();
		break;

	case Qt::MiddleButton:
		_width = 200.0;
		_height = 200.0;
		emit updatePainting();
		break;

	default:
		break;
	}
}

void fractali::SierpinskiCarpetController::myMouseMoveEvent(QMouseEvent * event)
{
	Qt::MouseButtons pressedButtons = event->buttons();

	if (pressedButtons & Qt::LeftButton)
	{
		_width = event->localPos().x();
		_height = event->localPos().y();
		emit updatePainting();
	}
}

void fractali::SierpinskiCarpetController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void fractali::SierpinskiCarpetController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void fractali::SierpinskiCarpetController::myResizeEvent(QResizeEvent * event)
{
	_width = event->size().width();
	_height = event->size().height();
	emit updatePainting();
}

void fractali::SierpinskiCarpetController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	unsigned nbRecursions = _model->get<unsigned>(0);
	painter.save();

	QBrush redBrush{ Qt::red };
	QBrush yellowBrush{ Qt::yellow };

	painter.setBrush(redBrush);
	painter.setPen(QColor{ Qt::red });
	painter.drawRect(0, 0, static_cast<int>(_width), static_cast<int>(_height));

	painter.setBrush(yellowBrush);
	painter.setPen(QColor{ Qt::yellow });
	draw(painter, 0.0, 0.0, _width, _height, nbRecursions);

	painter.restore();
}

void fractali::SierpinskiCarpetController::draw(QPainter & painter, double topLeftX, double topLeftY, double width, double height, unsigned nbRecursions)
{
	if (nbRecursions == 0)
		return;

	width /= 3.0;
	height /= 3.0;

	double width1 = topLeftX + width;
	double width2 = topLeftX + 2.0 * width;

	double height1 = topLeftY + height;
	double height2 = topLeftY + 2.0 * height;

	painter.drawRect(width2, height1, width, height); // 4
	painter.drawRect(topLeftX, height1, width, height); // 8


	draw(painter, topLeftX, topLeftY, width, height, nbRecursions - 1u);  // 1
	draw(painter, width1, topLeftY, width, height, nbRecursions - 1u);  // 2
	draw(painter, width2, topLeftY, width, height, nbRecursions - 1u);  // 3
	//draw(painter, width2, height1, width, height, nbRecursions - 1u);  // 4
	draw(painter, width2, height2, width, height, nbRecursions - 1u);  // 5
	draw(painter, width1, height2, width, height, nbRecursions - 1u);  // 6
	draw(painter, topLeftX, height2, width, height, nbRecursions - 1u);  // 7
	//draw(painter, topLeftX, height1, width, height, nbRecursions - 1u);  // 8
	draw(painter, width1, height1, width, height, nbRecursions - 1u);
}
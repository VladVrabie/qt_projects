#ifndef FRACTALI_SIERPINSKITRIANGLECONTROLLER_H
#define FRACTALI_SIERPINSKITRIANGLECONTROLLER_H

#include <QPointF>
#include <QPolygonF>
#include <QBrush>

#include "controller.h"

namespace fractali
{

	class SierpinskiTriangleController : public Controller
	{
	public:
		explicit SierpinskiTriangleController(QObject *parent = nullptr, Model *model = nullptr);
		~SierpinskiTriangleController() = default;



		// Controller interface
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;

	private:
		void draw(QPainter &painter, const QPointF &startPoint, double length, unsigned nbRecursions);

		const double sqrt3 = std::sqrt(3.0);
	};

}

#endif // FRACTALI_SIERPINSKITRIANGLECONTROLLER_H
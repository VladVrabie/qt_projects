cmake_minimum_required (VERSION 3.0)

# set project name
project (TemeGrafica)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /SUBSYSTEM:WINDOWS /ENTRY:mainCRTStartup")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")

# add project directory to include path
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(CMAKE_INCLUDE_CURRENT_DIR ON)


# set include and source root for files
set(INCROOT ${PROJECT_SOURCE_DIR})
set(SRCROOT ${PROJECT_SOURCE_DIR})

# all source files
file(GLOB MAIN_INCS "${INCROOT}/*.h")
file(GLOB MAIN_SRCS "${SRCROOT}/*.cpp")
file(GLOB MAIN_UIS "${SRCROOT}/*.ui")

file(GLOB POLIGON_INCS "${INCROOT}/Poligon/*.h")
file(GLOB POLIGON_SRCS "${SRCROOT}/Poligon/*.cpp")

file(GLOB PARAMETRICE_INCS "${INCROOT}/Parametrice/*.h")
file(GLOB PARAMETRICE_SRCS "${SRCROOT}/Parametrice/*.cpp")

file(GLOB NEWTON_INCS "${INCROOT}/Newton/*.h")
file(GLOB NEWTON_SRCS "${SRCROOT}/Newton/*.cpp")

file(GLOB COONS_INCS "${INCROOT}/Coons/*.h")
file(GLOB COONS_SRCS "${SRCROOT}/Coons/*.cpp")

file(GLOB BEZIER_INCS "${INCROOT}/Bezier/*.h")
file(GLOB BEZIER_SRCS "${SRCROOT}/Bezier/*.cpp")

file(GLOB BRESENHAM_INCS "${INCROOT}/Bresenham/*.h")
file(GLOB BRESENHAM_SRCS "${SRCROOT}/Bresenham/*.cpp")

file(GLOB FRACTALI_INCS "${INCROOT}/Fractali/*.h")
file(GLOB FRACTALI_SRCS "${SRCROOT}/Fractali/*.cpp")

file(GLOB GEOMCOMP_INCS "${INCROOT}/GeomComp/*.h")
file(GLOB GEOMCOMP_SRCS "${SRCROOT}/GeomComp/*.cpp")


# Setting up visual studio filters
source_group("Common\\Headers" FILES ${MAIN_INCS})
source_group("Common\\Sources" FILES ${MAIN_SRCS})
source_group("Common\\UI files" FILES ${MAIN_UIS})

source_group("Poligon\\Headers" FILES ${POLIGON_INCS})
source_group("Poligon\\Sources" FILES ${POLIGON_SRCS})

source_group("Parametrice\\Headers" FILES ${PARAMETRICE_INCS})
source_group("Parametrice\\Sources" FILES ${PARAMETRICE_SRCS})

source_group("Newton\\Headers" FILES ${NEWTON_INCS})
source_group("Newton\\Sources" FILES ${NEWTON_SRCS})

source_group("Coons\\Headers" FILES ${COONS_INCS})
source_group("Coons\\Sources" FILES ${COONS_SRCS})

source_group("Bezier\\Headers" FILES ${BEZIER_INCS})
source_group("Bezier\\Sources" FILES ${BEZIER_SRCS})

source_group("Bresenham\\Headers" FILES ${BRESENHAM_INCS})
source_group("Bresenham\\Sources" FILES ${BRESENHAM_SRCS})

source_group("Fractali\\Headers" FILES ${FRACTALI_INCS})
source_group("Fractali\\Sources" FILES ${FRACTALI_SRCS})

source_group("GeomComp\\Headers" FILES ${GEOMCOMP_INCS})
source_group("GeomComp\\Sources" FILES ${GEOMCOMP_SRCS})


#set Qt path
set(Qt5_DIR "C:\\Qt\\5.10.1\\msvc2017_64\\lib\\cmake\\Qt5")

# Find the QtWidgets library
find_package(Qt5 COMPONENTS Core Widgets Gui REQUIRED)

# For moc compilation and *.ui files
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)


# We need add -DQT_WIDGETS_LIB when using QtWidgets in Qt 5
add_definitions(${Qt5Widgets_DEFINITIONS})

add_executable(TemeGrafica  ${MAIN_SRCS} ${MAIN_INCS} ${MAIN_UIS}
							${POLIGON_INCS} ${POLIGON_SRCS}
							${PARAMETRICE_INCS} ${PARAMETRICE_SRCS}
							${NEWTON_INCS} ${NEWTON_SRCS}
							${COONS_INCS} ${COONS_SRCS}
							${BEZIER_INCS} ${BEZIER_SRCS}
							${BRESENHAM_INCS} ${BRESENHAM_SRCS}
							${FRACTALI_INCS} ${FRACTALI_SRCS}
							${GEOMCOMP_INCS} ${GEOMCOMP_SRCS}
							)

target_link_libraries(TemeGrafica Qt5::Widgets Qt5::Core Qt5::Gui)





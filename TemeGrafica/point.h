#ifndef POINT_H
#define POINT_H

#include <QPointF>
#include <QRectF>
#include <QPainter>
#include <functional>

class Point
{
public:
    Point(QPointF point = QPointF(0.0, 0.0));
	Point(double x, double y);
	~Point();
	Point& operator= (const QPointF &point);

	double x() const;
	double y() const;

	void setX(double x);
	void setY(double y);

	typedef std::function<void(const QPointF &)> setterFPtr;
	setterFPtr setterToPoint(const QPointF & mousePosition);

	void drawPoint(QPainter & painter, bool withCircle = true) const;

	bool isNull() const;
	void setToNull();

private:
	bool insideCircle(const QPointF & point) const;


	const float circleRadius = 5.0f;

	QPointF _point;
};

#endif // POINT_H
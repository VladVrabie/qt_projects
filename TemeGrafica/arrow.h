#ifndef ARROW_H
#define ARROW_H

#include <QPointF>
#include <QLineF>
#include <QRectF>
#include <QPainter>
#include <functional>

class Arrow
{
public:
    Arrow(QPointF start = QPointF(0.0, 0.0),
          QPointF stop = QPointF(0.0, 0.0));
    Arrow(double x1, double y1, double x2, double y2);
    ~Arrow();

    double x1() const;
    double y1() const;

    double x2() const;
    double y2() const;

    double dx() const;
    double dy() const;

    void setStart(const QPointF & start);
    void setStop(const QPointF & stop);

    typedef std::function<void(const QPointF &)> setterFPtr;
    setterFPtr setterToPoint(const QPointF & mousePosition);

    void drawArrow(QPainter & painter, bool withCircles = true) const;

    bool isNull() const;
    void setToNull();

private:
    void initArrowLegs();
    bool insideCircle(const QPointF & point, const QPointF & center) const;


    const float circleRadius = 5.0f;

    QLineF line;
    QLineF leftLeg; // when arrow is pointing up
    QLineF rightLeg;
};

#endif // ARROW_H

#include "Bresenham/bresenham_linecontroller.h"

bresenham::LineController::LineController(QObject *parent, Model *model)
    : Controller(parent, model)
{
	_image = model->getP<QImage>(0);
	_toDraw = *_image;
}

void bresenham::LineController::myMousePressEvent(QMouseEvent *event)
{
	Qt::MouseButton pressedButton = event->button();

	switch (pressedButton)
	{
	case Qt::LeftButton:
		_startPoint = event->pos();
		break;

	case Qt::MiddleButton:
		_image->fill(Qt::red);
		_toDraw = *_image;
		emit updatePainting();
		break;

	default:
		break;
	}
}

void bresenham::LineController::myMouseMoveEvent(QMouseEvent *event)
{
	Qt::MouseButtons pressedButtons = event->buttons();

	if (pressedButtons & Qt::LeftButton)
	{
		_toDraw = *_image;
		_stopPoint = event->pos();
		line();
		emit updatePainting();
	}
}

void bresenham::LineController::myMouseReleaseEvent(QMouseEvent *event)
{
	Q_UNUSED(event);
	*_image = _toDraw;
}

void bresenham::LineController::myKeyPressEvent(QKeyEvent *event)
{
	Q_UNUSED(event);
}

void bresenham::LineController::myResizeEvent(QResizeEvent *event)
{
	(*_model)[0] = _image->scaled(event->size());
	_image = _model->getP<QImage>(0);
	_toDraw = *_image;
}

void bresenham::LineController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
	Q_UNUSED(event);
	painter.drawImage(0, 0, _toDraw);
}

void bresenham::LineController::updateDrawing()
{
	_toDraw = *_image;
}

void bresenham::LineController::line1(int x0, int y0, int x1, int y1)
{
	int dx = x1 - x0;
	int dy = y1 - y0;

	int Sy = 1;
	if (dy < 0)
	{
		Sy = -1;
		dy = -dy;
	}

	int dx2 = dx + dx;
	int dy2 = dy + dy;

	int D = dy2 - dx;

	int y = y0;

	for (int x = x0; x < x1; ++x)
	{
		_toDraw.setPixel(x, y, qRgb(0, 0, 0));

		if (D > 0)
		{
			y += Sy;
			D -= dx2;
		}
		D += dy2;
	}
}

void bresenham::LineController::line2(int x0, int y0, int x1, int y1)
{
	int dx = x1 - x0;
	int dy = y1 - y0;

	int Sx = 1;
	if (dx < 0)
	{
		Sx = -1;
		dx = -dx;
	}

	int dx2 = dx + dx;
	int dy2 = dy + dy;

	int D = dx2 - dy;

	int x = x0;

	for (int y = y0; y < y1; ++y)
	{
		_toDraw.setPixel(x, y, qRgb(0, 0, 0));

		if (D > 0)
		{
			x += Sx;
			D -= dy2;
		}
		D += dx2;
	}
}

void bresenham::LineController::line()
{
	int x0 = _startPoint.x();
	int y0 = _startPoint.y();
	int x1 = _stopPoint.x();
	int y1 = _stopPoint.y();

	if ( std::abs(y1 - y0) <= std::abs(x1 - x0))
	{
		if (x0 < x1)
			line1(x0, y0, x1, y1);
		else
			line1(x1, y1, x0, y0);
	}
	else
	{
		if (y0 < y1)
			line2(x0, y0, x1, y1);
		else
			line2(x1, y1, x0, y0);
	}
}

#include "Bresenham/bresenham_filledcirclecontroller.h"

bresenham::FilledCircleController::FilledCircleController(QObject *parent, Model *model)
    : Controller(parent, model)
{
	_image = model->getP<QImage>(0);
	_toDraw = *_image;
}

void bresenham::FilledCircleController::myMousePressEvent(QMouseEvent *event)
{
	Qt::MouseButton pressedButton = event->button();

	switch (pressedButton)
	{
	case Qt::LeftButton:
		_centre = event->pos();
		break;

	case Qt::MiddleButton:
		_image->fill(Qt::red);
		_toDraw = *_image;
		emit updatePainting();
		break;

	default:
		break;
	}
}

void bresenham::FilledCircleController::myMouseMoveEvent(QMouseEvent *event)
{
	Qt::MouseButtons pressedButtons = event->buttons();

	if (pressedButtons & Qt::LeftButton)
	{
		_toDraw = *_image;
		QLineF radius{ QPointF(_centre), event->localPos() };
		_radius = static_cast<int>(radius.length());
		filledCircle();
		emit updatePainting();
	}
}

void bresenham::FilledCircleController::myMouseReleaseEvent(QMouseEvent *event)
{
	Q_UNUSED(event);
	*_image = _toDraw;
}

void bresenham::FilledCircleController::myKeyPressEvent(QKeyEvent *event)
{
	Q_UNUSED(event);
}

void bresenham::FilledCircleController::myResizeEvent(QResizeEvent *event)
{
	(*_model)[0] = _image->scaled(event->size());
	_image = _model->getP<QImage>(0);
	_toDraw = *_image;
}

void bresenham::FilledCircleController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
	Q_UNUSED(event);
	painter.drawImage(0, 0, _toDraw);
}

void bresenham::FilledCircleController::updateDrawing()
{
	_toDraw = *_image;
}

void bresenham::FilledCircleController::filledCircle()
{
	int cx = _centre.x();
	int cy = _centre.y();

	int x = _radius - 1;
	int y = 0;
	int dx = 1;
	int dy = 1;
	int R2 = _radius + _radius;
	int err = dx - R2;

	while (x >= y)
	{
		//_toDraw.setPixel(cx + x, cy + y, qRgb(0, 0, 0)); // 1
		//_toDraw.setPixel(cx + x, cy - y, qRgb(0, 0, 0)); // 2
		//_toDraw.setPixel(cx - x, cy + y, qRgb(0, 0, 0)); // 3
		//_toDraw.setPixel(cx - x, cy - y, qRgb(0, 0, 0)); // 4

		//_toDraw.setPixel(cx + y, cy + x, qRgb(0, 0, 0)); // 5
		//_toDraw.setPixel(cx + y, cy - x, qRgb(0, 0, 0)); // 6
		//_toDraw.setPixel(cx - y, cy + x, qRgb(0, 0, 0)); // 7
		//_toDraw.setPixel(cx - y, cy - x, qRgb(0, 0, 0)); // 8

		int y_13 = cy + y;
		int y_24 = cy - y;

		for (int localX = cx - x, stopX = cx + x; localX <= stopX; ++localX)
		{
			// 1 cu 3 <=> cadranele 1 si 4
			_toDraw.setPixel(localX, y_13, qRgb(0, 0, 0));
			// 2 cu 4 <=> cadranele 5 si 8
			_toDraw.setPixel(localX, y_24, qRgb(0, 0, 0));
		}

		int y_57 = cy + x;
		int y_68 = cy - x;

		for (int localX = cx - y, stopX = cx + y; localX <= stopX; ++localX)
		{
			// 5 cu 7 <=> cadranele 2 si 3 
			_toDraw.setPixel(localX, y_57, qRgb(0, 0, 0));
			// 6 cu 8 <=> cadranele 6 si 7
			_toDraw.setPixel(localX, y_68, qRgb(0, 0, 0));
		}

		//// 1 cu 3 <=> cadranele 1 si 4
		//for (int localX = cx - x, stopX = cx + x, localY = cy + y; localX <= stopX; ++localX)
		//	_toDraw.setPixel(localX, localY, qRgb(0, 0, 0));

		//// 2 cu 4 <=> cadranele 5 si 8
		//for (int localX = cx - x, stopX = cx + x, localY = cy - y; localX <= stopX; ++localX)
		//	_toDraw.setPixel(localX, localY, qRgb(0, 0, 0));

		//// 5 cu 7 <=> cadranele 2 si 3
		//for (int localX = cx - y, stopX = cx + y, localY = cy + x; localX <= stopX; ++localX)
		//	_toDraw.setPixel(localX, localY, qRgb(0, 0, 0));

		//// 6 cu 8 <=> cadranele 6 si 7
		//for (int localX = cx - y, stopX = cx + y, localY = cy - x; localX <= stopX; ++localX)
		//	_toDraw.setPixel(localX, localY, qRgb(0, 0, 0));

		if (err <= 0)
		{
			++y;
			err += dy;
			dy += 2;
		}

		if (err > 0)
		{
			--x;
			dx += 2;
			err += dx - R2;
		}
	}
}

#include "Bresenham/bresenham_managertab.h"

bresenham::ManagerTab::ManagerTab(QWidget *parent)
    : QWidget(parent)
{
	_model->appendData<QImage>(std::move(QImage(100, 100, QImage::Format_RGB32)));
	_model->getP<QImage>(0)->fill(Qt::red);
}

void bresenham::ManagerTab::childEvent(QChildEvent *event)
{
	if (event->type() == QEvent::ChildAdded ||
		event->type() == QEvent::ChildPolished)
	{
		QObject *objChild = event->child();

		if (tryFrame(objChild) == false)
			tryRadioButton(objChild);
	}
}

void bresenham::ManagerTab::lineButtonClicked(bool checked)
{
	Q_UNUSED(checked);
	if (_frame)
	{
		_lineController->updateDrawing();
		_frame->getSwitcher()->switchToController(id::lineController);
	}
}

void bresenham::ManagerTab::circleButtonClicked(bool checked)
{
	Q_UNUSED(checked);
	if (_frame)
	{
		_circleController->updateDrawing();
		_frame->getSwitcher()->switchToController(id::circleController);
	}
}

void bresenham::ManagerTab::filledCircleClicked(bool checked)
{
	Q_UNUSED(checked);
	if (_frame)
	{
		_filledCircleController->updateDrawing();
		_frame->getSwitcher()->switchToController(id::filledCircleController);
	}
}

void bresenham::ManagerTab::floodFillClicked(bool checked)
{
	Q_UNUSED(checked);
	if (_frame)
	{
		_frame->getSwitcher()->switchToController(id::floodFillController);
	}
}

bool bresenham::ManagerTab::tryFrame(QObject *child)
{
	MyFrame *myFrame = qobject_cast<MyFrame *>(child);

	if (myFrame != nullptr)
	{
		this->_frame = myFrame;

		// Line Controller
		_lineController = new LineController(_frame, _model.get());
		_frame->getSwitcher()->addController(id::lineController, _lineController);

		ControllerSwitcher::ConnectionArgs lineUpdateArgs{ _lineController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::lineController, lineUpdateArgs);

		//ControllerSwitcher::ConnectionArgs lineSwitchArgs{ lineController, SIGNAL(switchController(std::string)),
		//	_frame->getSwitcher(), SLOT(switchToController(std::string)) };
		//_frame->getSwitcher()->addConnection(id::lineController, lineSwitchArgs);


		// Circle Controller
		_circleController = new CircleController(_frame, _model.get());
		_frame->getSwitcher()->addController(id::circleController, _circleController);

		ControllerSwitcher::ConnectionArgs circleUpdateArgs{ _circleController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::circleController, circleUpdateArgs);


		// Filled Circle Controller
		_filledCircleController = new FilledCircleController(_frame, _model.get());
		_frame->getSwitcher()->addController(id::filledCircleController, _filledCircleController);

		ControllerSwitcher::ConnectionArgs filledCircleUpdateArgs{ _filledCircleController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::filledCircleController, filledCircleUpdateArgs);


		// Flood Fill Controller
		_floodFillController = new FloodFillController(_frame, _model.get());
		_frame->getSwitcher()->addController(id::floodFillController, _floodFillController);

		ControllerSwitcher::ConnectionArgs floodFillUpdateArgs{ _floodFillController, SIGNAL(updatePainting()), _frame, SLOT(update()) };
		_frame->getSwitcher()->addConnection(id::floodFillController, floodFillUpdateArgs);

		// Switching to lineController
		_frame->getSwitcher()->switchToController(id::lineController);

		return true;
	}

	return false;
}

bool bresenham::ManagerTab::tryRadioButton(QObject * child)
{
	QRadioButton *radioButton = qobject_cast<QRadioButton *>(child);

	if (radioButton != nullptr)
	{
		if (radioButton->objectName() == "radioButtonLine")
		{
			connect(radioButton, &QRadioButton::clicked, this, &ManagerTab::lineButtonClicked);
		}
		else if (radioButton->objectName() == "radioButtonCircle")
		{
			connect(radioButton, &QRadioButton::clicked, this, &ManagerTab::circleButtonClicked);
		}
		else if (radioButton->objectName() == "radioButtonFilledCircle")
		{
			connect(radioButton, &QRadioButton::clicked, this, &ManagerTab::filledCircleClicked);
		}
		else if (radioButton->objectName() == "radioButtonFloodFill")
		{
			connect(radioButton, &QRadioButton::clicked, this, &ManagerTab::floodFillClicked);
		}

		return true;
	}

	return false;
}

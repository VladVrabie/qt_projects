#ifndef BRESENHAM_MANAGERTAB_H
#define BRESENHAM_MANAGERTAB_H

#include <QWidget>
#include <QChildEvent>
#include <QRadioButton>
#include <QImage>

#include <memory>
#include "myframe.h"
#include "Bresenham/bresenham_controllerids.h"
#include "Bresenham/bresenham_linecontroller.h"
#include "Bresenham/bresenham_circlecontroller.h"
#include "Bresenham/bresenham_filledcirclecontroller.h"
#include "Bresenham/bresenham_floodfillcontroller.h"


namespace bresenham
{

    class ManagerTab : public QWidget
    {
        Q_OBJECT

    public:
        explicit ManagerTab(QWidget *parent = nullptr);
        ~ManagerTab() = default;

	protected:
		virtual void childEvent(QChildEvent *event) override;

	public slots:
		void lineButtonClicked(bool checked);
		void circleButtonClicked(bool checked);
		void filledCircleClicked(bool checked);
		void floodFillClicked(bool checked);

	private:
		bool tryFrame(QObject *child);
		bool tryRadioButton(QObject *child);


		MyFrame * _frame = nullptr;

		LineController *_lineController = nullptr;
		CircleController *_circleController = nullptr;
		FilledCircleController *_filledCircleController = nullptr;
		FloodFillController *_floodFillController = nullptr;

		std::unique_ptr<Model> _model = std::make_unique<Model>();
    };

}

#endif // BRESENHAM_MANAGERTAB_H

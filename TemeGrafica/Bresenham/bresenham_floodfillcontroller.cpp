#include "Bresenham/bresenham_floodfillcontroller.h"

bresenham::FloodFillController::FloodFillController(QObject *parent, Model *model)
    : Controller(parent, model)
{
	_image = model->getP<QImage>(0);
}

void bresenham::FloodFillController::myMousePressEvent(QMouseEvent *event)
{
	Qt::MouseButton pressedButton = event->button();

	switch (pressedButton)
	{
	case Qt::LeftButton:
	{
		const QRgb black = qRgb(0, 0, 0);
		const QRgb red = qRgb(255, 0, 0);
		const QRgb green = qRgb(0, 255, 0);
		const QRgb blue = qRgb(0, 0, 255);

		const QRgb currentColor = _image->pixel(event->pos());

		switch (currentColor)
		{
		case red:
			floodFill(event->pos(), green, black);
			break;

		case green:
			floodFill(event->pos(), blue, black);
			break;

		case blue:
			floodFill(event->pos(), red, black);
			break;

		default:
			break;
		}
	}
		emit updatePainting();
		break;

	case Qt::MiddleButton:
		_image->fill(Qt::red);
		emit updatePainting();
		break;

	default:
		break;
	}
}

void bresenham::FloodFillController::myMouseMoveEvent(QMouseEvent *event)
{
	Q_UNUSED(event);
}

void bresenham::FloodFillController::myMouseReleaseEvent(QMouseEvent *event)
{
	Q_UNUSED(event);
}

void bresenham::FloodFillController::myKeyPressEvent(QKeyEvent *event)
{
	Q_UNUSED(event);
}

void bresenham::FloodFillController::myResizeEvent(QResizeEvent *event)
{
	(*_model)[0] = _image->scaled(event->size());
	_image = _model->getP<QImage>(0);
}

void bresenham::FloodFillController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
	Q_UNUSED(event);
	painter.drawImage(0, 0, *_image);
}

void bresenham::FloodFillController::floodFill(QPoint start, QRgb fillColor, QRgb contourColor)
{
	std::stack<QPoint> st;
	st.push(start);

	while (st.empty() == false)
	{
		QPoint currentPos = st.top();
		st.pop();

		QRgb currentColor = _image->pixel(currentPos);

		if (currentColor == fillColor || currentColor == contourColor)
			continue;

		_image->setPixel(currentPos, fillColor);

		int x = currentPos.x();
		int y = currentPos.y();

		int xmax = _image->width();
		int ymax = _image->height();

		if (y > 0 && y < ymax)
		{
			if (x > 0)
				st.push(QPoint(x - 1, y));

			if (x < xmax)
				st.push(QPoint(x + 1, y));
		}

		if (x > 0 && x < xmax)
		{
			if (y > 0)
				st.push(QPoint(x, y - 1));

			if (y < ymax)
				st.push(QPoint(x, y + 1));
		}
	}
}

#include "Bresenham/bresenham_circlecontroller.h"

bresenham::CircleController::CircleController(QObject *parent, Model *model)
    : Controller(parent, model)
{
	_image = model->getP<QImage>(0);
	_toDraw = *_image;
}

void bresenham::CircleController::myMousePressEvent(QMouseEvent *event)
{
	Qt::MouseButton pressedButton = event->button();

	switch (pressedButton)
	{
	case Qt::LeftButton:
		_centre = event->pos();
		break;

	case Qt::MiddleButton:
		_image->fill(Qt::red);
		_toDraw = *_image;
		emit updatePainting();
		break;

	default:
		break;
	}
}

void bresenham::CircleController::myMouseMoveEvent(QMouseEvent *event)
{
	Qt::MouseButtons pressedButtons = event->buttons();

	if (pressedButtons & Qt::LeftButton)
	{
		_toDraw = *_image;
		QLineF radius{ QPointF(_centre), event->localPos() };
		_radius = static_cast<int>(radius.length());
		circle();
		emit updatePainting();
	}
}

void bresenham::CircleController::myMouseReleaseEvent(QMouseEvent *event)
{
	Q_UNUSED(event);
	*_image = _toDraw;
}

void bresenham::CircleController::myKeyPressEvent(QKeyEvent *event)
{
	Q_UNUSED(event);
}

void bresenham::CircleController::myResizeEvent(QResizeEvent *event)
{
	(*_model)[0] = _image->scaled(event->size());
	_image = _model->getP<QImage>(0);
	_toDraw = *_image;
}

void bresenham::CircleController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
	Q_UNUSED(event);
	painter.drawImage(0, 0, _toDraw);
}

void bresenham::CircleController::updateDrawing()
{
	_toDraw = *_image;
}

void bresenham::CircleController::circle()
{
	int cx = _centre.x();
	int cy = _centre.y();

	int x = _radius - 1;
	int y = 0;
	int dx = 1;
	int dy = 1;
	int R2 = _radius + _radius;
	int err = dx - R2;

	while (x >= y)
	{
		_toDraw.setPixel(cx + x, cy + y, qRgb(0, 0, 0)); // 1
		_toDraw.setPixel(cx + x, cy - y, qRgb(0, 0, 0)); // 2
		_toDraw.setPixel(cx - x, cy + y, qRgb(0, 0, 0)); // 3
		_toDraw.setPixel(cx - x, cy - y, qRgb(0, 0, 0)); // 4

		_toDraw.setPixel(cx + y, cy + x, qRgb(0, 0, 0)); // 5
		_toDraw.setPixel(cx + y, cy - x, qRgb(0, 0, 0)); // 6
		_toDraw.setPixel(cx - y, cy + x, qRgb(0, 0, 0)); // 7
		_toDraw.setPixel(cx - y, cy - x, qRgb(0, 0, 0)); // 8

		if (err <= 0)
		{
			++y;
			err += dy;
			dy += 2;
		}

		if (err > 0)
		{
			--x;
			dx += 2;
			err += dx - R2;
		}
	}
}
HEADERS += \
    $$PWD/bresenham_managertab.h \
    $$PWD/bresenham_linecontroller.h \
    $$PWD/bresenham_circlecontroller.h \
    $$PWD/bresenham_filledcirclecontroller.h \
    $$PWD/bresenham_floodfillcontroller.h \
    $$PWD/bresenham_controllerids.h

SOURCES += \
    $$PWD/bresenham_managertab.cpp \
    $$PWD/bresenham_linecontroller.cpp \
    $$PWD/bresenham_circlecontroller.cpp \
    $$PWD/bresenham_filledcirclecontroller.cpp \
    $$PWD/bresenham_floodfillcontroller.cpp

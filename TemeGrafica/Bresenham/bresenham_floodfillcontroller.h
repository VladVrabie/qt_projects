#ifndef BRESENHAM_FLOODFILLCONTROLLER_H
#define BRESENHAM_FLOODFILLCONTROLLER_H

#include <QImage>
#include <QPoint>

#include <stack>
#include "controller.h"

namespace bresenham
{

    class FloodFillController : public Controller
    {
        Q_OBJECT

    public:
        explicit FloodFillController(QObject *parent = nullptr, Model *model = nullptr);
        ~FloodFillController() = default;

        // Controller interface
        virtual void myMousePressEvent(QMouseEvent *event) override;
        virtual void myMouseMoveEvent(QMouseEvent *event) override;
        virtual void myMouseReleaseEvent(QMouseEvent *event) override;
        virtual void myKeyPressEvent(QKeyEvent *event) override;
        virtual void myResizeEvent(QResizeEvent *event) override;
        virtual void myPaintEvent(QPaintEvent *event, QPainter &painter) override;

	private:
		void floodFill(QPoint start, QRgb fillColor, QRgb contourColor);

		QImage *_image;
    };

}

#endif // BRESENHAM_FLOODFILLCONTROLLER_H

#ifndef BRESENHAM_CONTROLLERIDS_H
#define BRESENHAM_CONTROLLERIDS_H

#include <string>

namespace bresenham::id
{

	inline std::string lineController{ "lineController" };
	inline std::string circleController{ "circleController" };
	inline std::string filledCircleController{ "filledCircleController" };
	inline std::string floodFillController{ "floodFillController" };

}

#endif // BRESENHAM_CONTROLLERIDS_H

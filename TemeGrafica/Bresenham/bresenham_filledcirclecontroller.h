#ifndef BRESENHAM_FILLEDCIRCLECONTROLLER_H
#define BRESENHAM_FILLEDCIRCLECONTROLLER_H

#include <QPoint>
#include <QPointF>
#include <QLineF>
#include <QImage>

#include "controller.h"

namespace bresenham
{

    class FilledCircleController : public Controller
    {
        Q_OBJECT

    public:
        explicit FilledCircleController(QObject *parent = nullptr, Model *model = nullptr);
        ~FilledCircleController() = default;

        // Controller interface
        virtual void myMousePressEvent(QMouseEvent *event) override;
        virtual void myMouseMoveEvent(QMouseEvent *event) override;
        virtual void myMouseReleaseEvent(QMouseEvent *event) override;
        virtual void myKeyPressEvent(QKeyEvent *event) override;
        virtual void myResizeEvent(QResizeEvent *event) override;
        virtual void myPaintEvent(QPaintEvent *event, QPainter &painter) override;

		void updateDrawing();

	private:
		void filledCircle();

		QImage *_image;
		QImage _toDraw;

		QPoint _centre;
		int _radius = 0;
    };

}

#endif // BRESENHAM_FILLEDCIRCLECONTROLLER_H

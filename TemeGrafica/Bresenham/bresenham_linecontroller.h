#ifndef BRESENHAM_LINECONTROLLER_H
#define BRESENHAM_LINECONTROLLER_H

#include <QImage>
#include <QPoint>

#include "controller.h"

namespace bresenham
{

    class LineController : public Controller
    {
        Q_OBJECT

    public:
        explicit LineController(QObject *parent = nullptr, Model *model = nullptr);
        ~LineController() = default;

        // Controller interface
        virtual void myMousePressEvent(QMouseEvent *event) override;
        virtual void myMouseMoveEvent(QMouseEvent *event) override;
        virtual void myMouseReleaseEvent(QMouseEvent *event) override;
        virtual void myKeyPressEvent(QKeyEvent *event) override;
        virtual void myResizeEvent(QResizeEvent *event) override;
        virtual void myPaintEvent(QPaintEvent *event, QPainter &painter) override;

		void updateDrawing();

	private:
		void line1(int x0, int y0, int x1, int y1);
		void line2(int x0, int y0, int x1, int y1);
		void line();


		QImage *_image;
		QImage _toDraw;

		QPoint _startPoint;
		QPoint _stopPoint;
    };

}

#endif // BRESENHAM_LINECONTROLLER_H

#ifndef BRESENHAM_CIRCLECONTROLLER_H
#define BRESENHAM_CIRCLECONTROLLER_H

#include <QPoint>
#include <QPointF>
#include <QLineF>
#include <QImage>

#include "controller.h"

namespace bresenham
{

    class CircleController : public Controller
    {
        Q_OBJECT

    public:
        explicit CircleController(QObject *parent = nullptr, Model *model = nullptr);
        ~CircleController() = default;

        // Controller interface
        virtual void myMousePressEvent(QMouseEvent *event) override;
        virtual void myMouseMoveEvent(QMouseEvent *event) override;
        virtual void myMouseReleaseEvent(QMouseEvent *event) override;
        virtual void myKeyPressEvent(QKeyEvent *event) override;
        virtual void myResizeEvent(QResizeEvent *event) override;
        virtual void myPaintEvent(QPaintEvent *event, QPainter &painter) override;

		void updateDrawing();

	private:
		void circle();

		QImage * _image;
		QImage _toDraw;

		QPoint _centre;
		int _radius = 0;
    };

}

#endif // BRESENHAM_CIRCLECONTROLLER_H

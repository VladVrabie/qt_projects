﻿#include "computationalgeometry.h"

bool ComputationalGeometry::sameSign(int x, int y)
{
	return (x >= 0) ^ (y < 0);
}

double ComputationalGeometry::F(const QPointF &A, const QPointF &B, const QPointF &C)
{
	return (B.x() - A.x()) * (C.y() - A.y()) - (B.y() - A.y()) * (C.x() - A.x());
}

bool ComputationalGeometry::leftTurn(const QPointF &A, const QPointF &B, const QPointF &C)
{
	return F(A, B, C) > 0.0;
}

bool ComputationalGeometry::rightTurn(const QPointF &A, const QPointF &B, const QPointF &C)
{
	return F(A, B, C) < 0.0;
}

bool ComputationalGeometry::collinearPoints(const QPointF &A, const QPointF &B, const QPointF &C)
{
	return F(A, B, C) == 0.0;
}

bool ComputationalGeometry::separateHalfPlanes(const QPointF &A, const QPointF &B,
	const QPointF &startLine, const QPointF &endLine)
{
	return F(startLine, endLine, A) * F(startLine, endLine, B) < 0.0;
}

bool ComputationalGeometry::convexQuadrilateral(const QPointF & A, const QPointF & B, const QPointF & C, const QPointF & D)
{
	return separateHalfPlanes(B, D, A, C) && separateHalfPlanes(A, C, B, D);
}

bool ComputationalGeometry::convexPolygon(const QVector<QPointF>& points)
{
	int first = F(points[0], points[1], points[2]);
	int size = points.size();
	for (int i = 1; i < points.size(); ++i)
		if (!sameSign(first, F(points[i], points[(i + 1) % size], points[(i + 2) % size])))
			return false;
	return true;
}

bool ComputationalGeometry::pointInSegment(const QPointF & point, const QPointF & start, const QPointF & end)
{
	return collinearPoints(point, start, end) &&
		std::min(start.x(), end.x()) < point.x() && point.x() < std::max(start.x(), end.x()) &&
		std::min(start.y(), end.y()) < point.y() && point.y() < std::max(start.y(), end.y());
}

unsigned ComputationalGeometry::linePolygonNbIntersections(const QPointF &startLine, const QPointF &endLine, const QVector<QPointF> &points)
{
	unsigned nbIntersections = 0u;
	int size = points.size();
	for (int i = 0; i < size; ++i)
	{
		auto &Ai = points[i];
		auto &Ai_1 = points[i - 1 < 0 ? size - 1 : i - 1];
		auto &Ai1 = points[(i + 1) % size];
		auto &Ai2 = points[(i + 2) % size];

		if (pointInSegment(Ai, startLine, endLine) && pointInSegment(Ai1, startLine, endLine))
		{// cazul particular 2
			if (separateHalfPlanes(Ai_1, Ai2, startLine, endLine))
				++nbIntersections;
			//++i;
		}
		else if (pointInSegment(Ai, startLine, endLine))
		{// cazul 1 particular
			if (separateHalfPlanes(Ai_1, Ai1, startLine, endLine))
				++nbIntersections;
		}
		else if (convexQuadrilateral(Ai, startLine, Ai1, endLine))
			++nbIntersections;
	}
	return nbIntersections;
}

unsigned ComputationalGeometry::linePolygonNbIntersections2(const QPointF &startLine, const QPointF &endLine, const QVector<QPointF> &points)
{
	unsigned nbIntersections = 0u;
	int size = points.size();
	for (int i = 0; i < size; ++i)
	{
		auto &Ai = points[i];
		auto &Ai1 = points[(i + 1) % size];

		if (pointInSegment(Ai, startLine, endLine) && pointInSegment(Ai1, startLine, endLine))
		{// cazul particular 2
			//if (separateHalfPlanes(Ai_1, Ai2, startLine, endLine))
				++nbIntersections;
			//++i;
		}
		else if (pointInSegment(Ai, startLine, endLine))
		{// cazul 1 particular
			//if (separateHalfPlanes(Ai_1, Ai1, startLine, endLine))
				++nbIntersections;
		}
		else if (convexQuadrilateral(Ai, startLine, Ai1, endLine))
			++nbIntersections;
	}
	return nbIntersections;
}

bool ComputationalGeometry::pointInPolygon(const QPointF &point, const QVector<QPointF> &points)
{
	auto xMax = std::max_element(points.begin(), points.end(), [](const QPointF &a, const QPointF &b) {
		return a.x() < b.x();
	})->x();
	auto yMax = std::max_element(points.begin(), points.end(), [](const QPointF &a, const QPointF &b) {
		return a.y() < b.y();
	})->y();
	
	QPointF exteriorPoint(xMax + 1, yMax + 1);

	return linePolygonNbIntersections(point, exteriorPoint, points) % 2u == 1u;
}

std::vector<std::pair<QPointF, QPointF>> ComputationalGeometry::triangulatePolygon(const QVector<QPointF> &points)
{
	bool isClockwisePolygon = clockwisePolygon(points);

	std::unique_ptr<QVector<QPointF>> auxPoints = std::make_unique<QVector<QPointF>>(points);

	std::vector<std::pair<QPointF, QPointF>> diagonalsArray;

	bool firsttry = true;
	int earsFound = 0;
	while (auxPoints->size() > 3)
	{
		earsFound = 0;
		for (int i = 0; i < auxPoints->size(); ++i)
		{
			auto& Ai_1 = auxPoints->at(i - 1 < 0 ? auxPoints->size() - 1 : i - 1);
			auto& Ai = auxPoints->at(i);
			auto& Ai1 = auxPoints->at((i + 1) % auxPoints->size());

			if (polygonEar(i, *auxPoints, points, isClockwisePolygon))
			{
				++earsFound; // trebuie sa gasesc una datorita Teoremei celor 2 urechi
				diagonalsArray.emplace_back(Ai_1, Ai1);
				auxPoints->removeOne(Ai);
				break;
			}
		}

		if (earsFound == 0)
		{
			if (firsttry)
				firsttry = false;
			else
				break;
		}
	}

	return diagonalsArray;
}

bool ComputationalGeometry::polygonEar(int middlePointIndex, const QVector<QPointF> &points, const QVector<QPointF> &origPoints, bool clock)
{
	int predecessorIndex = middlePointIndex - 1 < 0 ? points.size() - 1 : middlePointIndex - 1;
	int successorIndex = (middlePointIndex + 1) % points.size();
	auto& firstPredecessor = points[predecessorIndex];
	auto& firstSuccessor = points[successorIndex];

	// daca nu intersecteaza nicio latura a poligonului
	if (polygonDiagonal(firstPredecessor, firstSuccessor, origPoints)) 
	{
		//return interiorDiagonal(predecessorIndex, successorIndex, points, clock);
		
		return interiorDiagonal(predecessorIndex, successorIndex, points, clock)
			|| interiorDiagonal(successorIndex, predecessorIndex, points, clock);
		
	}
	return false;

	//auto& secondPredecessor = points[middlePointIndex - 2 < 0 ? points.size() - 2 : middlePointIndex - 2];
	//auto& firstPredecessor = points[middlePointIndex - 1 < 0 ? points.size() - 1 : middlePointIndex - 1];
	//auto& middlePoint = points[middlePointIndex];
	//auto& firstSuccessor = points[(middlePointIndex + 1) % points.size()];
	////auto& secondSuccessor = points[(middlePointIndex + 2) % points.size()];

	////auto previousTurn = F(secondPredecessor, firstPredecessor, middlePoint);
	////auto currentTurn = F(firstPredecessor, middlePoint, firstSuccessor);
	////auto nextTurn = F(middlePoint, firstSuccessor, secondSuccessor);

	//////auto test = F(firstSuccessor, firstPredecessor, middlePoint);

	////auto it = std::min_element(origPoints.begin(), origPoints.end(), [](const QPointF &a, const QPointF &b)
	////{
	////	return a.x() < b.x();
	////});
	////int indexMin = origPoints.indexOf(*it);
	////auto test = F(origPoints[indexMin - 1 < 0 ? origPoints.size() - 1 : indexMin - 1], origPoints[indexMin], origPoints[(indexMin + 1) % origPoints.size()]);

	////return polygonDiagonal(firstPredecessor, firstSuccessor, origPoints)
	////	&& sameSign(currentTurn, test);

	//auto hull = convexHull(points);

	////auto orientation = F(hull[0], hull[1], hull[2]); // iese mereu negativa pt ca punctele din hull sunt cc

	//int index1, index2, index3, i = -1;
	//do
	//{
	//	++i;
	//	index1 = points.indexOf(hull[i % hull.size()]);
	//	index2 = points.indexOf(hull[(i + 1) % hull.size()]);
	//	index3 = points.indexOf(hull[(i + 2) % hull.size()]);

	//} while (index1 > index2 && index2 < index3 || index1 < index2 && index2 > index3);

	//if (index1 < index2 && index2 < index3)
	//{// counterclockwise
	//	if (! leftTurn(firstPredecessor, middlePoint, secondPredecessor)) // convex vertex i think // i think there are other equivalent conditions
	//		//return separateHalfPlanes(secondPredecessor, middlePoint, firstPredecessor, firstSuccessor);
	//		return convexQuadrilateral(secondPredecessor, firstPredecessor, middlePoint, firstSuccessor);
	//	else
	//		return !convexQuadrilateral(secondPredecessor, firstPredecessor, middlePoint, firstSuccessor);
	//}
	//else if (index1 > index2 && index2 > index3)
	//{// clockwise
	//	if (! rightTurn(firstPredecessor, middlePoint, secondPredecessor))
	//		return convexQuadrilateral(secondPredecessor, firstPredecessor, middlePoint, firstSuccessor);
	//	else
	//		return !convexQuadrilateral(secondPredecessor, firstPredecessor, middlePoint, firstSuccessor);
	//}
	////else eroare
	//throw new std::exception("eroare la indexare");
}

bool ComputationalGeometry::polygonDiagonal(const QPointF & A, const QPointF & B, const QVector<QPointF> &points)
{
	return linePolygonNbIntersections2(A, B, points) == 0;
}

bool ComputationalGeometry::interiorDiagonal(int startIndex, int endIndex, const QVector<QPointF>& points, bool clock)
{
	auto& startPredecessor = points[startIndex - 1 < 0 ? points.size() - 1 : startIndex - 1];
	auto& startPoint = points[startIndex];
	auto& startSuccessor = points[(startIndex + 1) % points.size()];
	auto& endPoint = points[endIndex];

	if (!clock)
	{//counterclock
		if (!leftTurn(startPoint, startSuccessor, startPredecessor)) // convex vertex // nu conteaza ordinea cred
			return convexQuadrilateral(startPredecessor, startPoint, startSuccessor, endPoint);
		else // concave vertex
			return !convexQuadrilateral(startPredecessor, startPoint, startSuccessor, endPoint);
	}
	else
	{//clock
		if (!rightTurn(startPoint, startSuccessor, startPredecessor)) // convex vertex
			return convexQuadrilateral(startPredecessor, startPoint, startSuccessor, endPoint);
		else
			return !convexQuadrilateral(startPredecessor, startPoint, startSuccessor, endPoint);
	}
}

QVector<QPointF> ComputationalGeometry::convexHull(const QVector<QPointF>& points)
{
	auto minmax = std::minmax_element(
		points.begin(), 
		points.end(), 
		[](const QPointF &lhs, const QPointF &rhs) 
		{
			return lhs.x() < rhs.x();
		});

	auto leftMostPoint = *(minmax.first);
	auto rightMostPoint = *(minmax.second);

	auto uAuxPoints = std::make_unique<QVector<QPointF>>();
	(*uAuxPoints) = points;
	uAuxPoints->removeOne(leftMostPoint);
	uAuxPoints->removeOne(rightMostPoint);

	auto uLeftOfLine = std::make_unique<QVector<QPointF>>();
	auto uRightOfLine = std::make_unique<QVector<QPointF>>();
	uLeftOfLine->resize(uAuxPoints->size());
	uRightOfLine->resize(uAuxPoints->size());

	auto lastElemItPair = std::partition_copy(
		uAuxPoints->begin(),
		uAuxPoints->end(),
		stdext::make_checked_array_iterator(uLeftOfLine->begin(), uLeftOfLine->size()),
		stdext::make_checked_array_iterator(uRightOfLine->begin(), uRightOfLine->size()),
		[&leftMostPoint, &rightMostPoint](const QPointF &point)
		{
			return ComputationalGeometry::leftTurn(leftMostPoint, rightMostPoint, point);
		});

	uLeftOfLine->erase(lastElemItPair.first.base(), uLeftOfLine->end());
	uRightOfLine->erase(lastElemItPair.second.base(), uRightOfLine->end());

	auto c1 = quickHull(leftMostPoint, rightMostPoint, *uLeftOfLine);
	auto c2 = quickHull(rightMostPoint, leftMostPoint, *uRightOfLine);

	return QVector<QPointF>() << leftMostPoint << c1 << rightMostPoint << c2;
}

QVector<QPointF> ComputationalGeometry::quickHull(const QPointF & start, const QPointF & end, const QVector<QPointF>& points)
{
	if (points.size() == 0)
		return QVector<QPointF>();

	auto it = std::max_element(
		points.begin(),
		points.end(),
		[&start, &end](const QPointF &lhs, const QPointF &rhs)
		{
			auto distanceToLine = [](const QPointF &point, const QPointF &start, const QPointF &end)
			{
				double a = end.y() - start.y();
				double b = start.x() - end.x();
				double c = start.y() * -b - start.x() * a;

				double numerator = std::abs(a * point.x() + b * point.y() + c);
				double denominator = std::sqrt(a * a + b * b);

				return numerator / denominator;
			};
			double d1 = distanceToLine(lhs, start, end);
			double d2 = distanceToLine(rhs, start, end);

			return d1 < d2;
		}
	);

	auto furthestPoint = *it;

	auto uAuxPoints = std::make_unique<QVector<QPointF>>(points);
	uAuxPoints->removeOne(furthestPoint);

	auto uLeftOfFurthestPoint = std::make_unique<QVector<QPointF>>();
	auto uRightOfFurthestPoint = std::make_unique<QVector<QPointF>>();
	uLeftOfFurthestPoint->resize(uAuxPoints->size());
	uRightOfFurthestPoint->resize(uAuxPoints->size());
	
	auto lastElemItLeft = std::copy_if(
		uAuxPoints->begin(),
		uAuxPoints->end(),
		stdext::make_checked_array_iterator(uLeftOfFurthestPoint->begin(), uLeftOfFurthestPoint->size()),
		[&start, &furthestPoint] (const QPointF & point)
		{
			return ComputationalGeometry::leftTurn(start, furthestPoint, point);
		}
	);

	auto lastElemItRight = std::copy_if(
		uAuxPoints->begin(),
		uAuxPoints->end(),
		stdext::make_checked_array_iterator(uRightOfFurthestPoint->begin(), uRightOfFurthestPoint->size()),
		[&end, &furthestPoint](const QPointF & point)
		{
			return ComputationalGeometry::rightTurn(end, furthestPoint, point);
		}
	);

	uLeftOfFurthestPoint->erase(lastElemItLeft.base(), uLeftOfFurthestPoint->end());
	uRightOfFurthestPoint->erase(lastElemItRight.base(), uRightOfFurthestPoint->end());

	auto c1 = quickHull(start, furthestPoint, *uLeftOfFurthestPoint);
	auto c2 = quickHull(furthestPoint, end, *uRightOfFurthestPoint);

	return QVector<QPointF>() << c1 << furthestPoint << c2;
}

bool ComputationalGeometry::clockwisePolygon(const QVector<QPointF>& points)
{
	auto hull = convexHull(points);
	int index1, index2, index3, i = -1;
	do
	{
		++i;
		index1 = points.indexOf(hull[i % hull.size()]);
		index2 = points.indexOf(hull[(i + 1) % hull.size()]);
		index3 = points.indexOf(hull[(i + 2) % hull.size()]);

	} while (index1 > index2 && index2 < index3 || index1 < index2 && index2 > index3);

	//bool counterclock = index1 < index2 && index2 < index3;
	//bool clock = index1 > index2 && index2 > index3;
	return index1 > index2 && index2 > index3;
}

#include "Bezier/bezier_inputcontroller.h"

bezier::InputController::InputController(QObject *parent, Model *model)
	: Controller(parent, model)
{

}

bezier::InputController::~InputController()
{

}

void bezier::InputController::myMousePressEvent(QMouseEvent * event)
{
	Qt::MouseButton pressedButton = event->button();
	switch (pressedButton)
	{
	case Qt::LeftButton:
		_model->appendData<Point>(Point(event->localPos()));
		break;

	case Qt::MiddleButton:
		_model->clearData();
		break;

	case Qt::RightButton:
		if (_model->size() > 1)
			emit switchController(id::drawingController);
		break;
	}
	emit updatePainting();
}

void bezier::InputController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void bezier::InputController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void bezier::InputController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void bezier::InputController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void bezier::InputController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	const int size = _model->size();
	for (int i = 0; i < size; ++i)
		_model->getP<Point>(i)->drawPoint(painter);
}

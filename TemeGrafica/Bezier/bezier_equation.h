#ifndef BEZIER_EQUATION_H
#define BEZIER_EQUATION_H

#include <cmath>
#include "model.h"
#include "point.h"

namespace bezier
{

    class Equation
    {
    public:
        Equation(Model* model = nullptr);
		~Equation();

		void setModel(Model* model);

		std::function<double(double)> getXFunction();
		std::function<double(double)> getYFunction();

		double getStart() const;
		double getStop() const;
		unsigned getResolution() const;

	private:
		unsigned combinari(unsigned n, unsigned k);
		void initCombinari();
		double Bernstein(double t, unsigned n, unsigned i);

		std::vector<unsigned> _combinari;
		Model * _model;
    };

}

#endif // BEZIER_EQUATION_H

HEADERS += \  
    $$PWD/bezier_inputcontroller.h \
    $$PWD/bezier_frame.h \
    $$PWD/bezier_drawingcontroller.h \
    $$PWD/bezier_equation.h \
    $$PWD/bezier_controllerids.h

SOURCES += \  
    $$PWD/bezier_inputcontroller.cpp \
    $$PWD/bezier_frame.cpp \
    $$PWD/bezier_drawingcontroller.cpp \
    $$PWD/bezier_equation.cpp

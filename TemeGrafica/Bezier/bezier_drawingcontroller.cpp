#include "Bezier/bezier_drawingcontroller.h"

bezier::DrawingController::DrawingController(QObject *parent, Model *model)
	: Controller(parent, model)
{

}

bezier::DrawingController::~DrawingController()
{

}

void bezier::DrawingController::myMousePressEvent(QMouseEvent * event)
{
	Qt::MouseButton pressedButton = event->button();
	switch (pressedButton)
	{
	case Qt::LeftButton:
		{
			const int size = _model->size();
			for (int i = 0; i < size; ++i)
			{
				_setPoint = _model->getP<Point>(i)->setterToPoint(event->localPos());
				if (_setPoint != nullptr)
					break;
			}
		}
		break;

	case Qt::RightButton:
		_model->clearData();
		emit switchController(id::inputController);
		break;
	}
}

void bezier::DrawingController::myMouseMoveEvent(QMouseEvent * event)
{
	if (_setPoint != nullptr)
	{
		_setPoint(event->localPos());
		emit updatePainting();
	}
}

void bezier::DrawingController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
	_setPoint = nullptr;
}

void bezier::DrawingController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void bezier::DrawingController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void bezier::DrawingController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	const int size = _model->size();
	for (int i = 0; i < size; ++i)
		_model->getP<Point>(i)->drawPoint(painter);

	_bezierEquation.setModel(_model);

	QVector<QPointF> points = _calc.calculatePoints(_bezierEquation.getXFunction(),
													_bezierEquation.getYFunction(),
													_bezierEquation.getStart(),
													_bezierEquation.getStop(),
													_bezierEquation.getResolution());

	QPainterPath path(points[0]);
	const int nbPoints = points.size();
	for (int i = 1; i < nbPoints; ++i)
		path.lineTo(points[i]);

	painter.drawPath(path);
}

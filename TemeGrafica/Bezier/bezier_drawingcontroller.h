#ifndef BEZIER_DRAWINGCONTROLLER_H
#define BEZIER_DRAWINGCONTROLLER_H

#include <QPainterPath>

#include "point.h"
#include "controller.h"
#include "curvepointscalculator.h"
#include "Bezier/bezier_controllerids.h"
#include "Bezier/bezier_equation.h"

namespace bezier
{

    class DrawingController : public Controller
    {
    public:
        explicit DrawingController(QObject *parent = nullptr, Model *model = nullptr);
		~DrawingController();

		// Inherited via Controller
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;

	private:
		Point::setterFPtr _setPoint{ nullptr };
		Equation _bezierEquation;
		CurvePointsCalculator _calc;
	};

}

#endif // BEZIER_DRAWINGCONTROLLER_H

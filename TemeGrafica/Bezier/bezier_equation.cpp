#include "Bezier/bezier_equation.h"

bezier::Equation::Equation(Model* model)
	: _model{ model }
{
	initCombinari();
}

bezier::Equation::~Equation()
{

}

void bezier::Equation::setModel(Model* model)
{
	_model = model;
	initCombinari();
}

std::function<double(double)> bezier::Equation::getXFunction()
{
	std::function<double(double)> fPtr = [this](double t) {
		double sum = 0.0;
		unsigned n = static_cast<unsigned>(this->_model->size() - 1);
		for (unsigned i = 0; i <= n; ++i)
			sum += this->_model->getP<Point>(static_cast<int>(i))->x() * this->Bernstein(t, n, i);
		return sum;
	};
	return fPtr;
}

std::function<double(double)> bezier::Equation::getYFunction()
{
	std::function<double(double)> fPtr = [this](double t) {
		double sum = 0.0;
		unsigned n = static_cast<unsigned>(this->_model->size() - 1);
		for (unsigned i = 0; i <= n; ++i)
			sum += this->_model->getP<Point>(static_cast<int>(i))->y() * this->Bernstein(t, n, i);
		return sum;
	};
	return fPtr;
}

double bezier::Equation::getStart() const
{
	return 0.0;
}

double bezier::Equation::getStop() const
{
	return 1.0;
}

unsigned bezier::Equation::getResolution() const
{
	return 500u;
}

unsigned bezier::Equation::combinari(unsigned n, unsigned k)
{
	if (k > n || k >= _combinari.size())
		return 0u;
	return _combinari[k];
}

void bezier::Equation::initCombinari()
{
	if (_model != nullptr && _model->size() > 1) // minim 2 puncte pentru a desena ceva
	{	// poate merge cu si
		if (static_cast<unsigned>(_model->size()) != _combinari.size()) // nu are sens sa recalculam combinarile decat atunci cand se modifica n, adica nr de puncte
		{
			_combinari.clear();
			_combinari.resize(_model->size(), 0u);

			_combinari[0] = 1u;

			unsigned n = static_cast<unsigned>(_combinari.size()) - 1u;
			for (unsigned i = 1u; i <= n; ++i) 
			{
				for (unsigned j = i; j > 0u; --j)
				{
					// from the recurrence C(n, r) = C(n - 1, r - 1) + C(n - 1, r)
					_combinari[j] += _combinari[j - 1];
				}
			}
		}
	}
	// altfel il eliberez pe _combinari (pt putine puncte nu e asa important)
}

double bezier::Equation::Bernstein(double t, unsigned n, unsigned i)
{
	return combinari(n, i) * std::pow(t, i) * std::pow(1.0 - t, n - i);
}

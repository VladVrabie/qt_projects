#ifndef BEZIER_INPUTCONTROLLER_H
#define BEZIER_INPUTCONTROLLER_H

#include "point.h"
#include "controller.h"
#include "Bezier/bezier_controllerids.h"

namespace bezier
{

    class InputController : public Controller
    {
    public:
        explicit InputController(QObject *parent = nullptr, Model *model = nullptr);
		~InputController();

		// Inherited via Controller
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;
	};

}

#endif // BEZIER_INPUTCONTROLLER_H

#ifndef BEZIER_FRAME_H
#define BEZIER_FRAME_H

#include <memory>
#include "myframe.h"
#include "Bezier/bezier_inputcontroller.h"
#include "Bezier/bezier_drawingcontroller.h"

namespace bezier
{

    class Frame : public MyFrame
    {
		Q_OBJECT

    public:
        explicit Frame(QWidget *parent = nullptr);
		~Frame();

	private:
		std::unique_ptr<Model> _uModel = std::make_unique<Model>();
    };

}

#endif // BEZIER_FRAME_H

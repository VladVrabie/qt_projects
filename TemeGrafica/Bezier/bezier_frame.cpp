#include "Bezier/bezier_frame.h"

bezier::Frame::Frame(QWidget *parent)
	: MyFrame(parent)
{
	InputController *inputController = new InputController(this, _uModel.get());
	_switcher->addController(id::inputController, inputController);

	ControllerSwitcher::ConnectionArgs inputUpdateArgs{ inputController, SIGNAL(updatePainting()), this, SLOT(update()) };
	_switcher->addConnection(id::inputController, inputUpdateArgs);

	ControllerSwitcher::ConnectionArgs inputSwitchArgs{ inputController, SIGNAL(switchController(std::string)),
														_switcher, SLOT(switchToController(std::string)) };
	_switcher->addConnection(id::inputController, inputSwitchArgs);


	DrawingController *drawingController = new DrawingController(this, _uModel.get());
	_switcher->addController(id::drawingController, drawingController);

	ControllerSwitcher::ConnectionArgs drawUpdateArgs{ drawingController, SIGNAL(updatePainting()), this, SLOT(update()) };
	_switcher->addConnection(id::drawingController, drawUpdateArgs);

	ControllerSwitcher::ConnectionArgs drawSwitchArgs{ drawingController, SIGNAL(switchController(std::string)),
														_switcher, SLOT(switchToController(std::string)) };
	_switcher->addConnection(id::drawingController, drawSwitchArgs);


	// setez pe inputController
	_switcher->switchToController(id::inputController);
}

bezier::Frame::~Frame()
{

}

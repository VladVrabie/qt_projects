#ifndef BEZIER_CONTROLLERIDS_H
#define BEZIER_CONTROLLERIDS_H

#include <string>

namespace bezier::id
{
    inline std::string inputController{ "inputController" };
    inline std::string drawingController{ "drawingController" };
}

#endif // BEZIER_CONTROLLERIDS_H

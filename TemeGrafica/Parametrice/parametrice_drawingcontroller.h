#ifndef PARAMETRICE_DRAWINGCONTROLLER_H
#define PARAMETRICE_DRAWINGCONTROLLER_H

#include <QPainterPath>
#include <QFont>

#include "controller.h"
#include "Parametrice/parametrice_pointscalculator.h"

namespace parametrice
{

    class DrawingController : public Controller
    {
        Q_OBJECT

    public:
        explicit DrawingController(QObject *parent = nullptr, Model *model = nullptr);
        ~DrawingController();

		// Inherited via Controller
		virtual void myMousePressEvent(QMouseEvent *event) override;
		virtual void myMouseMoveEvent(QMouseEvent *event) override;
		virtual void myMouseReleaseEvent(QMouseEvent *event) override;
		virtual void myKeyPressEvent(QKeyEvent *event) override;
		virtual void myResizeEvent(QResizeEvent *event) override;
		virtual void myPaintEvent(QPaintEvent *event, QPainter & painter) override;

    private:
        PointsCalculator calc;
	};

}

#endif // PARAMETRICE_DRAWINGCONTROLLER_H

#include "Parametrice/parametrice_drawingcontroller.h"

parametrice::DrawingController::DrawingController(QObject *parent, Model *model)
    : Controller(parent, model)
{

}

parametrice::DrawingController::~DrawingController()
{

}

void parametrice::DrawingController::myMousePressEvent(QMouseEvent * event)
{
	Q_UNUSED(event)
}

void parametrice::DrawingController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event)
}

void parametrice::DrawingController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event)
}

void parametrice::DrawingController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event)
}

void parametrice::DrawingController::myResizeEvent(QResizeEvent * event)
{
	calc.setWidgetWidth(event->size().width());
	calc.setWidgetHeight(event->size().height());
}

void parametrice::DrawingController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);

	auto& xFunction = _model->get<CurvePointsCalculator::fPtr>(0);
	auto& yFunction = _model->get<CurvePointsCalculator::fPtr>(1);
	double& startValue = _model->get<double>(2);
	double& endValue = _model->get<double>(3);
	unsigned& resolutionValue = _model->get<unsigned>(4);

	if (xFunction && yFunction)
	{
		QVector<QPointF> points(std::move(
			calc.calculatePoints(xFunction, yFunction,
				startValue, endValue, resolutionValue)));

		QPointF O = points.takeFirst();
		QPointF A = points.takeFirst();
		QPointF B = points.takeFirst();
		QPointF C = points.takeFirst();
		QPointF D = points.takeFirst();

		double arrowOffset = 6.0;
		double arrowLength = 12.0;

		QPointF upArrowLeft(D.x() - arrowOffset, D.y() + arrowLength);
		QPointF upArrowRight(D.x() + arrowOffset, D.y() + arrowLength);
		QPointF rightArrowUp(B.x() - arrowLength, B.y() - arrowOffset);
		QPointF rightArrowDown(B.x() - arrowLength, B.y() + arrowOffset);

		painter.drawLine(A, B);
		painter.drawLine(C, D);

		painter.drawLine(D, upArrowLeft);
		painter.drawLine(D, upArrowRight);
		painter.drawLine(B, rightArrowUp);
		painter.drawLine(B, rightArrowDown);

		painter.setFont(QFont(QStringLiteral("Times"), 11));
		painter.drawText(QPointF(O.x() + 5.0, O.y() - 5.0), QStringLiteral("O"));
		painter.drawText(QPointF(B.x() - 25.0, B.y() - 5.0), QStringLiteral("x"));
		painter.drawText(QPointF(D.x() + 5.0, D.y() + 25.0), QStringLiteral("y"));

		QPainterPath path(points[0]);
		int size = points.size();
		for (int i = 1; i < size; ++i)
			path.lineTo(points[i]);

		painter.drawPath(path);
	}

}

HEADERS += \
    $$PWD/parametrice_managertab.h \
    $$PWD/parametrice_functionlist.h \
    $$PWD/parametrice_pointscalculator.h \
    $$PWD/parametrice_drawingcontroller.h

SOURCES += \
    $$PWD/parametrice_managertab.cpp \
    $$PWD/parametrice_pointscalculator.cpp \
    $$PWD/parametrice_drawingcontroller.cpp

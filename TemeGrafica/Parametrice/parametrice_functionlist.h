#ifndef PARAMETRICE_FUNCTIONLIST_H
#define PARAMETRICE_FUNCTIONLIST_H

#include <QtMath>

namespace parametrice
{
    // TODO: move implementations?
    class FunctionList
    {
    public:

        static double xEllipse(double u)
        {
            return 1.0 * qCos(u);
        }

        static double yEllipse(double u)
        {
            return 2.0 * qSin(u);
        }

        static double xSpirala(double u)
        {
            return u * qSin(u);
        }

        static double ySpirala(double u)
        {
            return u * qCos(u);
        }

        static double xParabolaOriz(double t)
        {
            return t * t + t;
        }

        static double yParabolaOriz(double t)
        {
            return 2.0 * t - 1.0;
        }

        static double xLissajous1(double t)
        {
            return 3.0 * qCos(1.0 * t + M_PI_2);
        }

        static double xLissajous3(double t)
        {
            return 3.0 * qCos(3.0 * t + M_PI_2);
        }

        static double yLissajous2(double t)
        {
            return 3.0 * qSin(2.0 * t);
        }

        static double xHypotrochoid535(double t)
        {
            double R = 5.0, r = 3.0, d = 5.0;

            return (R - r) * qCos(t) + d * qCos((R - r) / r * t);
        }

        static double yHypotrochoid535(double t)
        {
            double R = 5.0, r = 3.0, d = 5.0;

            return (R - r) * qSin(t) - d * qSin((R - r) / r * t);
        }

        static double xStar1(double t)
        {
            return qCos(1.0 * t) - qPow(qCos(5.0 * t), 3.0);
        }

        static double yStar1(double t)
        {
            return qSin(1.0 * t) - qPow(qSin(5 * t), 3.0);
        }

        static double xCircles(double t)
        {
            return qCos(5.0 * t) - qPow(qCos(1 * t), 3.0);
        }

        static double yCircles(double t)
        {
            return qSin(5.0 * t) - qPow(qSin(1 * t), 3.0);
        }

        static double xHearts(double t)
        {
            return qCos(1.0 * t) - qPow(qCos(80.0 * t), 3.0);
        }

        static double yHearts(double t)
        {
            return qSin(80.0 * t) - qPow(qSin(80.0 * t), 4.0);
        }

        static double xVale(double t)
        {
            return qCos(9.0 * t) - qPow(qCos(200.0 * t), 3.0);
        }

        static double yVale(double t)
        {
            return qSin(100.0 * t) - qPow(qSin(9.0 * t), 4.0);
        }

        static double xEvantai(double t)
        {
            double i =  1.0, a = 1.0, b = 1.0, c = 60.0;
            return i * qCos(a * t) - qCos(b * t) * qSin(c * t);
        }

        static double yEvantai(double t)
        {
            double j =  2.0, d = 1.0, e = 60.0;
            return j * qSin(d * t) - qSin(e * t);
        }
    };

}

#endif // PARAMETRICE_FUNCTIONLIST_H

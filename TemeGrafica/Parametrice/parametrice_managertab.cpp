#include "Parametrice/parametrice_managertab.h"

parametrice::ManagerTab::ManagerTab(QWidget *parent) : QWidget(parent)
{
    model->getData().resize(5);
    setModelValues(fPtr(nullptr), fPtr(nullptr), -1.0, 1.0, 0u);
}

parametrice::ManagerTab::~ManagerTab()
{

}

void parametrice::ManagerTab::childEvent(QChildEvent *event)
{
    if (event->type() == QEvent::ChildAdded ||
        event->type() == QEvent::ChildPolished)
    {
        QObject *objChild = event->child();

        if (tryInitFrame(objChild) == false)
            if (tryConnectComboBox(objChild) == false)
                if (tryConnectDoubleSpinBox(objChild) == false)
                    tryConnectSpinBox(objChild);
    }

}

void parametrice::ManagerTab::setFunctions(int index)
{
    switch (index)
    {
    case 0:
        setModelValues(fPtr(nullptr), fPtr(nullptr), -1.0, 1.0, 0u);
        setViewValues(-1.0, 1.0, 0);
        break;

    case 1:
        setModelValues(fPtr(FunctionList::xEllipse),
                       fPtr(FunctionList::yEllipse),
                       0.0, 2.0 * M_PI, 150u);
        setViewValues(0.0, 2.0 * M_PI, 150);
        break;

    case 2:
        setModelValues(fPtr(FunctionList::xSpirala),
                       fPtr(FunctionList::ySpirala),
                       0.0, 20.0, 300u);
        setViewValues(0.0, 20.0, 300);
        break;

    case 3:
        setModelValues(fPtr(FunctionList::xParabolaOriz),
                       fPtr(FunctionList::yParabolaOriz),
                       -2.0, 1.0, 300u);
        setViewValues(-2.0, 1.0, 300);
        break;

    case 4:
        setModelValues(fPtr(FunctionList::xLissajous1),
                       fPtr(FunctionList::yLissajous2),
                       0.0, 2.0 * M_PI, 150u);
        setViewValues(0.0, 2.0 * M_PI, 150);
        break;

    case 5:
        setModelValues(fPtr(FunctionList::xLissajous3),
                       fPtr(FunctionList::yLissajous2),
                       0.0, 2.0 * M_PI, 150u);
        setViewValues(0.0, 2.0 * M_PI, 150);
        break;

    case 6:
        setModelValues(fPtr(FunctionList::xHypotrochoid535),
                       fPtr(FunctionList::yHypotrochoid535),
                       0.0, 6.0 * M_PI, 500u);
        setViewValues(0.0, 6.0 * M_PI, 500);
        break;

    case 7:
        setModelValues(fPtr(FunctionList::xStar1),
                       fPtr(FunctionList::yStar1),
                       0.0, 2.0 * M_PI, 400u);
        setViewValues(0.0, 2.0 * M_PI, 400u);
        break;

    case 8:
        setModelValues(fPtr(FunctionList::xCircles),
                       fPtr(FunctionList::yCircles),
                       0.0, 2.0 * M_PI, 400u);
        setViewValues(0.0, 2.0 * M_PI, 400);
        break;

    case 9:
        setModelValues(fPtr(FunctionList::xHearts),
                       fPtr(FunctionList::yHearts),
                       0.0, 2.0 * M_PI, 10000u);
        setViewValues(0.0, 2.0 * M_PI, 10000);
        break;

    case 10:
        setModelValues(fPtr(FunctionList::xVale),
                       fPtr(FunctionList::yVale),
                       0.0, 2.0 * M_PI, 10000u);
        setViewValues(0.0, 2.0 * M_PI, 10000);
        break;

    case 11:
        setModelValues(fPtr(FunctionList::xEvantai),
                       fPtr(FunctionList::yEvantai),
                       0.0, 2.0 * M_PI, 10000u);
        setViewValues(0.0, 2.0 * M_PI, 10000);
        break;

    default:
        break;
    }
    frame->update();
}

void parametrice::ManagerTab::setStartValue(double value)
{
    (*model)[2] = value;
    frame->update();
}

void parametrice::ManagerTab::setStopValue(double value)
{
	(*model)[3] = value;
    frame->update();
}

void parametrice::ManagerTab::setResolution(int value)
{
	(*model)[4] = static_cast<unsigned>(value);
    frame->update();
}

void parametrice::ManagerTab::setModelValues(parametrice::ManagerTab::fPtr xFunction,
                                          parametrice::ManagerTab::fPtr yFunction,
                                          double start, double stop, unsigned resolution)
{
    (*model)[0] = xFunction;
    (*model)[1] = yFunction;
    (*model)[2] = start;
    (*model)[3] = stop;
    (*model)[4] = resolution;
}

void parametrice::ManagerTab::setViewValues(double start, double stop, int resolution)
{
    startSpinBox->setValue(start);
    stopSpinBox->setValue(stop);
    resolutionSpinBox->setValue(resolution);
}

bool parametrice::ManagerTab::tryInitFrame(QObject *child)
{
    MyFrame *myFrame = qobject_cast<MyFrame *>(child);

    if (myFrame != nullptr)
    {
        this->frame = myFrame;

		DrawingController *drawingController = new DrawingController(frame, model.get());

		frame->getSwitcher()->addController("drawingController", drawingController);
		frame->getSwitcher()->switchToController("drawingController");
    
        return true;
    }
    return false;
}

bool parametrice::ManagerTab::tryConnectComboBox(QObject *child)
{
    QComboBox *comboBox = qobject_cast<QComboBox *>(child);

    if (comboBox != nullptr)
    {
        functionsComboBox = comboBox;
        connect(functionsComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
                this, &ManagerTab::setFunctions);
        return true;
    }
    return false;
}

bool parametrice::ManagerTab::tryConnectDoubleSpinBox(QObject *child)
{
    QDoubleSpinBox *spinBox = qobject_cast<QDoubleSpinBox *>(child);

    if (spinBox != nullptr)
    {
        if (spinBox->objectName() == QStringLiteral("spinBoxStart"))
        {
            startSpinBox = spinBox;
            connect(startSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
                    this, &ManagerTab::setStartValue);
            return true;
        }
        else if (spinBox->objectName() == QStringLiteral("spinBoxStop"))
        {
            stopSpinBox = spinBox;
            connect(stopSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged),
                    this, &ManagerTab::setStopValue);
            return true;
        }
    }
    return false;
}

bool parametrice::ManagerTab::tryConnectSpinBox(QObject *child)
{
    QSpinBox *spinBox = qobject_cast<QSpinBox *>(child);

    if (spinBox != nullptr)
    {
        resolutionSpinBox = spinBox;
        connect(resolutionSpinBox, QOverload<int>::of(&QSpinBox::valueChanged),
                this, &ManagerTab::setResolution);
        return true;
    }
    return false;
}

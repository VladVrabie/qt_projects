#ifndef PARAMETRICE_MANAGERTAB_H
#define PARAMETRICE_MANAGERTAB_H

#include <QChildEvent>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QSpinBox>

#include <memory>
#include "myframe.h"
#include "Parametrice/parametrice_drawingcontroller.h"
#include "Parametrice/parametrice_functionlist.h"

namespace parametrice
{

    class ManagerTab : public QWidget
    {
        Q_OBJECT

    public:
        explicit ManagerTab(QWidget *parent = nullptr);
        ~ManagerTab();

    protected:
        virtual void childEvent(QChildEvent *event) override;

    public slots:
        void setFunctions(int index);
        void setStartValue(double value);
        void setStopValue(double value);
        void setResolution(int value);

    private:
        typedef std::function<double(double)> fPtr;
        void setModelValues(fPtr xFunction, fPtr yFunction, double start, double stop, unsigned resolution);
        void setViewValues(double start, double stop, int resolution);

        bool tryInitFrame(QObject *child);
        bool tryConnectComboBox(QObject *child);
        bool tryConnectDoubleSpinBox(QObject *child);
        bool tryConnectSpinBox(QObject *child);

        MyFrame *frame = nullptr;
        QComboBox *functionsComboBox = nullptr;
        QDoubleSpinBox *startSpinBox = nullptr;
        QDoubleSpinBox *stopSpinBox = nullptr;
        QSpinBox *resolutionSpinBox = nullptr;

        std::unique_ptr<Model> model = std::make_unique<Model>();
    };

}

#endif // PARAMETRICE_MANAGERTAB_H

#ifndef PARAMETRICE_POINTSCALCULATOR_H
#define PARAMETRICE_POINTSCALCULATOR_H

#include <algorithm>
#include "curvepointscalculator.h"
#include "transform.h"

namespace parametrice
{
    // TODO: better name?
    class PointsCalculator : public CurvePointsCalculator
    {
    public:
        PointsCalculator();
        ~PointsCalculator();

        void setWidgetWidth(double width);
        void setWidgetHeight(double height);

    protected:
        virtual QVector<QPointF> extraProcessing() override;

    private:
        void firstStep();
        void secondStep();
        void thirdStep();
        void fourthStep();


        QVector<QPointF> _aux;
        Transform _transform;
        double _widgetWidth;
        double _widgetHeight;
        double _s3_xMax;
        double _s3_yMax;
    };

}

#endif // PARAMETRICE_POINTSCALCULATOR_H

#include "controllerswitcher.h"

ControllerSwitcher::ControllerSwitcher(QObject *parent)
    : QObject(parent)
{

}

ControllerSwitcher::~ControllerSwitcher()
{

}

// Add a new controller to be managed by this switcher. If the id is already used, function does nothing
void ControllerSwitcher::addController(const std::string &idController, Controller *controller)
{
	auto result = controllers.emplace(idController, controller);
	if (result.second == true)
	{
		// presupun ca id-ul nu exista in mapa de conexiuni si ca insertia reuseste
		connections.try_emplace(idController);
	}
}

// Add a new connection to a controller when switched to. If the id is unknown or the arguments are already present, function does nothing
void ControllerSwitcher::addConnection(const std::string &idController,
                                       const ControllerSwitcher::ConnectionArgs &arguments)
{
    if (auto it = connections.find(idController); it != connections.end()) // daca id-ul controllerului e in mapa
    {
        if (std::none_of(it->second.cbegin(), it->second.cend(),
                         std::bind(std::equal_to<ConnectionArgs>(), std::placeholders::_1, arguments)))
        {
            // daca argumentele conexiunii nu sunt deja prezente in vector
            it->second.push_back(arguments);
        }
    }
}

//void ControllerSwitcher::setModelToAll(Model *model)
//{
//    for (auto& idControllerPair : controllers)
//		idControllerPair.second->setModel(model);
//}

// Switches to the parameter controller. If the id is unknown, function does nothing
void ControllerSwitcher::switchToController(std::string idController)
{
	if (auto it = controllers.find(idController); it != controllers.end()) // daca id-ul controllerului e in mapa
	{
		// daca exista un controller conectat
		// il deconectez
		if (currentController.empty() == false)
			controllers[currentController]->disconnect();
		

		// am nevoie de pointer la connect pt a selecta care connect sa il apeleze std::apply
		QMetaObject::Connection (*_connect)(const QObject *, const char *,
			const QObject *, const char *, Qt::ConnectionType) = QObject::connect;

		// conectez noul controller
		for (auto& connectArgs : connections[idController])
			std::apply(_connect, connectArgs.asTuple());
		
		// il fac drept controllerul curent
		currentController = std::move(idController);

		// emit ca s-a schimbat
		emit controllerChanged(it->second);

		// emit o redesenare cu noul controller
		emit it->second->updatePainting();
	}
}

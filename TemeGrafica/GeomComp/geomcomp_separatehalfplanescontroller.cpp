#include "GeomComp/geomcomp_separatehalfplanescontroller.h"

geomcomp::SeparateHalfPlanesController::SeparateHalfPlanesController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void geomcomp::SeparateHalfPlanesController::myMousePressEvent(QMouseEvent * event)
{
	Qt::MouseButton pressedButton = event->button();

	if (pressedButton == Qt::LeftButton)
	{
		if (_model->size() < maxNbPoints)
		{
			_model->appendData(event->localPos());

			if (_model->size() == maxNbPoints)
			{
				if (ComputationalGeometry::separateHalfPlanes(
					_model->get<QPointF>(0),
					_model->get<QPointF>(1),
					_model->get<QPointF>(2),
					_model->get<QPointF>(3)))
				{
					emit updateLabel(QStringLiteral("Sunt separate"));
				}
				else
					emit updateLabel(QStringLiteral("Nu sunt separate"));
			}
		}
	}
	else if (pressedButton == Qt::RightButton)
	{
		_model->clearData();
		emit updateLabel(QStringLiteral(""));
	}
	emit updatePainting();
}

void geomcomp::SeparateHalfPlanesController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::SeparateHalfPlanesController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::SeparateHalfPlanesController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::SeparateHalfPlanesController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::SeparateHalfPlanesController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	for (int i = 0; i < _model->size(); ++i)
		Point(_model->get<QPointF>(i)).drawPoint(painter);
	if (_model->size() == 4)
		painter.drawLine(_model->get<QPointF>(2), _model->get<QPointF>(3));
}

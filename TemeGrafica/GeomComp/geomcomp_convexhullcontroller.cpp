#include "GeomComp/geomcomp_convexhullcontroller.h"

geomcomp::ConvexHullController::ConvexHullController(QObject *parent, Model *model)
    : Controller(parent, model)
{
}


void geomcomp::ConvexHullController::myMousePressEvent(QMouseEvent *event)
{
	Qt::MouseButton pressedButton = event->button();

	if (pressedButton == Qt::LeftButton)
	{
		if (!finished)
			_model->appendData(event->localPos());
	}
	else if (pressedButton == Qt::RightButton)
	{
		if (_model->size() >= minimumNbPoints)
			finished = true;
	}
	else if (pressedButton == Qt::MiddleButton)
	{
		finished = false;
		_model->clearData();
	}
	emit updatePainting();
}

void geomcomp::ConvexHullController::myMouseMoveEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void geomcomp::ConvexHullController::myMouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void geomcomp::ConvexHullController::myKeyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event);
}

void geomcomp::ConvexHullController::myResizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
}

void geomcomp::ConvexHullController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
    Q_UNUSED(event);
	painter.setRenderHint(QPainter::Antialiasing);

	int size = _model->size();
	for (int i = 0; i < size; ++i)
		Point(_model->get<QPointF>(i)).drawPoint(painter);

	if (QVector<QPointF> points; finished)
	{
		std::for_each(_model->begin(), _model->end(), [&points](auto point) {
			points.append(std::any_cast<QPointF>(point));
		});

		auto convexHull = ComputationalGeometry::convexHull(points);

		int hullSize = convexHull.size();
		for (int i = 0; i < hullSize; ++i)
			painter.drawLine(convexHull[i], convexHull[(i + 1) % hullSize]);
	}
}

void geomcomp::ConvexHullController::reset()
{
	finished = false;
}
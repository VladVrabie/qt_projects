HEADERS += \ 
    $$PWD/geomcomp_managertab.h \
    $$PWD/geomcomp_controllerids.h \
    $$PWD/geomcomp_convexhullcontroller.h \
    $$PWD/geomcomp_separatehalfplanescontroller.h \
    $$PWD/geomcomp_convexquadrilateralcontroller.h \
    $$PWD/geomcomp_convexpolygoncontroller.h \
    $$PWD/geomcomp_colourpolygoncontroller.h \
    $$PWD/geomcomp_triangulationcontroller.h
    

SOURCES += \ 
    $$PWD/geomcomp_managertab.cpp \
    $$PWD/geomcomp_convexhullcontroller.cpp \
    $$PWD/geomcomp_separatehalfplanescontroller.cpp \
    $$PWD/geomcomp_convexquadrilateralcontroller.cpp \
    $$PWD/geomcomp_convexpolygoncontroller.cpp \
    $$PWD/geomcomp_colourpolygoncontroller.cpp \
    $$PWD/geomcomp_triangulationcontroller.cpp
    
	

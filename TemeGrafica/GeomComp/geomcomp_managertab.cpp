#include "GeomComp/geomcomp_managertab.h"

geomcomp::ManagerTab::ManagerTab(QWidget *parent)
	: QWidget(parent)
{

}

void geomcomp::ManagerTab::problemSelectionChanged(int index)
{
	switch (index)
	{
	case 0:
		model->clearData();
		frame->getSwitcher()->switchToController(id::separateHalfPlanesController);
		break;

	case 1:
		model->clearData();
		frame->getSwitcher()->switchToController(id::convexQuadrilateralController);
		break;

	case 2:
		emit resetPbController();
		model->clearData();
		frame->getSwitcher()->switchToController(id::convexPolygonController);
		break;

	case 3:
		emit resetPbController();
		model->clearData();
		colourPolygonController->setHeightAndWidht(frame->height(), frame->width());
		frame->getSwitcher()->switchToController(id::colourPolygonController);
		break;

	case 4:
		emit resetPbController();
		model->clearData();
		frame->getSwitcher()->switchToController(id::triangulationController);
		break;

	case 5:
		emit resetPbController();
		model->clearData();
		frame->getSwitcher()->switchToController(id::convexHullController);
		break;

	default:
		break;
	}
	labelTextChanged(QStringLiteral(""));
}

void geomcomp::ManagerTab::labelTextChanged(QString newText)
{
	if (resultLabel != nullptr)
		resultLabel->setText(newText);
}

void geomcomp::ManagerTab::childEvent(QChildEvent *event)
{
	if (event->type() == QEvent::ChildAdded ||
		event->type() == QEvent::ChildPolished)
	{
		QObject *objChild = event->child();

		if (tryInitFrame(objChild) == false)
			if (tryConnectComboBox(objChild) == false)
				tryInitLabel(objChild);
	}
}

bool geomcomp::ManagerTab::tryInitFrame(QObject *child)
{
	MyFrame *myFrame = qobject_cast<MyFrame *>(child);

	if (myFrame != nullptr)
	{
		this->frame = myFrame;

		/****************************************************************************************************************************************/

		SeparateHalfPlanesController *separateHalfPlanesController = new SeparateHalfPlanesController(frame, model.get());
		frame->getSwitcher()->addController(id::separateHalfPlanesController, separateHalfPlanesController);

		ControllerSwitcher::ConnectionArgs separateHalfPlanesControllerUpdateArgs{ separateHalfPlanesController, SIGNAL(updatePainting()), frame, SLOT(update()) };
		frame->getSwitcher()->addConnection(id::separateHalfPlanesController, separateHalfPlanesControllerUpdateArgs);

		ControllerSwitcher::ConnectionArgs separateHalfPlanesControllerLabelUpdateArgs{ separateHalfPlanesController, SIGNAL(updateLabel(QString)), this, SLOT(labelTextChanged(QString)) };
		frame->getSwitcher()->addConnection(id::separateHalfPlanesController, separateHalfPlanesControllerLabelUpdateArgs);

		/****************************************************************************************************************************************/

		ConvexQuadrilateralController *convexQuadrilateralController = new ConvexQuadrilateralController(frame, model.get());
		frame->getSwitcher()->addController(id::convexQuadrilateralController, convexQuadrilateralController);

		ControllerSwitcher::ConnectionArgs convexQuadrilateralControllerUpdateArgs{ convexQuadrilateralController, SIGNAL(updatePainting()), frame, SLOT(update()) };
		frame->getSwitcher()->addConnection(id::convexQuadrilateralController, convexQuadrilateralControllerUpdateArgs);

		ControllerSwitcher::ConnectionArgs convexQuadrilateralControllerLabelUpdateArgs{ convexQuadrilateralController, SIGNAL(updateLabel(QString)), this, SLOT(labelTextChanged(QString)) };
		frame->getSwitcher()->addConnection(id::convexQuadrilateralController, convexQuadrilateralControllerLabelUpdateArgs);

		/****************************************************************************************************************************************/

		ConvexPolygonController *convexPolygonController = new ConvexPolygonController(frame, model.get());
		frame->getSwitcher()->addController(id::convexPolygonController, convexPolygonController);

		ControllerSwitcher::ConnectionArgs convexPolygonControllerUpdateArgs{ convexPolygonController, SIGNAL(updatePainting()), frame, SLOT(update()) };
		frame->getSwitcher()->addConnection(id::convexPolygonController, convexPolygonControllerUpdateArgs);

		ControllerSwitcher::ConnectionArgs convexPolygonControllerLabelUpdateArgs{ convexPolygonController, SIGNAL(updateLabel(QString)), this, SLOT(labelTextChanged(QString)) };
		frame->getSwitcher()->addConnection(id::convexPolygonController, convexPolygonControllerLabelUpdateArgs);

		ControllerSwitcher::ConnectionArgs convexPolygonControllerResetArgs{ this, SIGNAL(resetPbController()), convexPolygonController, SLOT(reset()) };
		frame->getSwitcher()->addConnection(id::convexPolygonController, convexPolygonControllerResetArgs);

		/****************************************************************************************************************************************/

		colourPolygonController = new ColourPolygonController(frame, model.get());
		colourPolygonController->setHeightAndWidht(frame->height(), frame->width());
		frame->getSwitcher()->addController(id::colourPolygonController, colourPolygonController);

		ControllerSwitcher::ConnectionArgs colourPolygonControllerUpdateArgs{ colourPolygonController, SIGNAL(updatePainting()), frame, SLOT(update()) };
		frame->getSwitcher()->addConnection(id::colourPolygonController, colourPolygonControllerUpdateArgs);

		ControllerSwitcher::ConnectionArgs colourPolygonControllerResetArgs{ this, SIGNAL(resetPbController()), colourPolygonController, SLOT(reset()) };
		frame->getSwitcher()->addConnection(id::colourPolygonController, colourPolygonControllerResetArgs);

		/****************************************************************************************************************************************/

		ConvexHullController *convexHullController = new ConvexHullController(frame, model.get());
		frame->getSwitcher()->addController(id::convexHullController, convexHullController);

		ControllerSwitcher::ConnectionArgs convexHullControllerUpdateArgs{ convexHullController, SIGNAL(updatePainting()), frame, SLOT(update()) };
		frame->getSwitcher()->addConnection(id::convexHullController, convexHullControllerUpdateArgs);

		ControllerSwitcher::ConnectionArgs convexHullControllerResetArgs{ this, SIGNAL(resetPbController()), convexHullController, SLOT(reset()) };
		frame->getSwitcher()->addConnection(id::convexHullController, convexHullControllerResetArgs);

		/****************************************************************************************************************************************/

		TriangulationController *triangulationController = new TriangulationController(frame, model.get());
		frame->getSwitcher()->addController(id::triangulationController, triangulationController);

		ControllerSwitcher::ConnectionArgs triangulationControllerUpdateArgs{ triangulationController,  SIGNAL(updatePainting()), frame, SLOT(update()) };
		frame->getSwitcher()->addConnection(id::triangulationController, triangulationControllerUpdateArgs);

		ControllerSwitcher::ConnectionArgs triangulationControllerResetArgs{ this, SIGNAL(resetPbController()), triangulationController, SLOT(reset()) };
		frame->getSwitcher()->addConnection(id::triangulationController, triangulationControllerResetArgs);

		/****************************************************************************************************************************************/

		frame->getSwitcher()->switchToController(id::separateHalfPlanesController);

		return true;
	}
	return false;
}

bool geomcomp::ManagerTab::tryConnectComboBox(QObject *child)
{
	QComboBox *comboBox = qobject_cast<QComboBox *>(child);

	if (comboBox != nullptr)
	{
		problemsComboBox = comboBox;
		connect(problemsComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
			this, &ManagerTab::problemSelectionChanged);
		return true;
	}
	return false;
}

bool geomcomp::ManagerTab::tryInitLabel(QObject * child)
{
	QLabel *label = qobject_cast<QLabel *>(child);

	if (label != nullptr)
	{
		resultLabel = label;
		return true;
	}
	return false;
}

#ifndef GEOMCOMP_PB5CONTROLLER_H
#define GEOMCOMP_PB5CONTROLLER_H

#include "controller.h"
#include "computationalgeometry.h"

namespace geomcomp
{

    class ColourPolygonController : public Controller
	{
		Q_OBJECT

	public:
        explicit ColourPolygonController(QObject *parent = nullptr, Model *model = nullptr);
        ~ColourPolygonController() = default;

		void setHeightAndWidht(int newHeight, int newWidth);

		// Inherited via Controller
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;

	public slots:
		void reset();

	private:
		int height = 0;
		int width = 0;
		bool finished = false;
		const int minimumNbPoints = 3;
	};

}

#endif // GEOMCOMP_PB5CONTROLLER_H

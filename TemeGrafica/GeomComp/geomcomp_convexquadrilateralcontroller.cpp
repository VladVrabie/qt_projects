#include "GeomComp/geomcomp_convexquadrilateralcontroller.h"

geomcomp::ConvexQuadrilateralController::ConvexQuadrilateralController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void geomcomp::ConvexQuadrilateralController::myMousePressEvent(QMouseEvent * event)
{
	Qt::MouseButton pressedButton = event->button();

	if (pressedButton == Qt::LeftButton)
	{
		if (_model->size() < maxNbPoints)
		{
			_model->appendData(event->localPos());

			if (_model->size() == maxNbPoints)
			{
				if (ComputationalGeometry::convexQuadrilateral(
					_model->get<QPointF>(0),
					_model->get<QPointF>(1),
					_model->get<QPointF>(2),
					_model->get<QPointF>(3)))
				{
					emit updateLabel(QStringLiteral("Patrulater convex"));
				}
				else
					emit updateLabel(QStringLiteral("Patrulater concav"));
			}
		}
	}
	else if (pressedButton == Qt::RightButton)
	{
		_model->clearData();
		emit updateLabel(QStringLiteral(""));
	}
	emit updatePainting();
}

void geomcomp::ConvexQuadrilateralController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::ConvexQuadrilateralController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::ConvexQuadrilateralController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::ConvexQuadrilateralController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::ConvexQuadrilateralController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);

	if (_model->size() > 1)
		for (int i = 0; i < _model->size() - 1; ++i)
			painter.drawLine(_model->get<QPointF>(i), _model->get<QPointF>(i+1));
	
	if (_model->size() == maxNbPoints)
		painter.drawLine(_model->get<QPointF>(0), _model->get<QPointF>(_model->size() - 1));
}

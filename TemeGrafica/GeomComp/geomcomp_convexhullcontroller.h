#ifndef GEOMCOMP_CONVEXHULLCONTROLLER_H
#define GEOMCOMP_CONVEXHULLCONTROLLER_H

#include "controller.h"
#include "point.h"
#include "computationalgeometry.h"

namespace geomcomp
{

	class ConvexHullController : public Controller
	{
		Q_OBJECT

	public:
		explicit ConvexHullController(QObject *parent = nullptr, Model *model = nullptr);
		~ConvexHullController() = default;

		// Controller interface
		virtual void myMousePressEvent(QMouseEvent *event) override;
		virtual void myMouseMoveEvent(QMouseEvent *event) override;
		virtual void myMouseReleaseEvent(QMouseEvent *event) override;
		virtual void myKeyPressEvent(QKeyEvent *event) override;
		virtual void myResizeEvent(QResizeEvent *event) override;
		virtual void myPaintEvent(QPaintEvent *event, QPainter &painter) override;

	public slots:
		void reset();

	private:
		bool finished = false;
		const int minimumNbPoints = 3;
	};

}

#endif // GEOMCOMP_CONVEXHULLCONTROLLER_H

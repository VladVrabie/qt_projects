#include "GeomComp/geomcomp_convexpolygoncontroller.h"

geomcomp::ConvexPolygonController::ConvexPolygonController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void geomcomp::ConvexPolygonController::myMousePressEvent(QMouseEvent * event)
{
	Qt::MouseButton pressedButton = event->button();

	if (pressedButton == Qt::LeftButton)
	{
		if (!finished)
			_model->appendData(event->localPos());
	}
	else if (pressedButton == Qt::RightButton)
	{
		if (QVector<QPointF> points; _model->size() > minimumNbPoints)
		{
			finished = true;

			std::for_each(_model->begin(), _model->end(), [&points](auto point) {
				points.append(std::any_cast<QPointF>(point));
			});

			if (ComputationalGeometry::convexPolygon(points))
				emit updateLabel(QStringLiteral("Poligon convex"));
			else
				emit updateLabel(QStringLiteral("Poligon concav"));
		}
	}
	else if (pressedButton == Qt::MiddleButton)
	{
		finished = false;
		_model->clearData();
		emit updateLabel(QStringLiteral(""));
	}
	emit updatePainting();
}

void geomcomp::ConvexPolygonController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::ConvexPolygonController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::ConvexPolygonController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::ConvexPolygonController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::ConvexPolygonController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	for (int i = 0; i < _model->size() - 1; ++i)
		painter.drawLine(_model->get<QPointF>(i), _model->get<QPointF>(i + 1));

	if (finished && _model->size() > 1)
		painter.drawLine(_model->get<QPointF>(0), _model->get<QPointF>(_model->size() - 1));
}

void geomcomp::ConvexPolygonController::reset()
{
	finished = false;
}
#ifndef GEOMCOMP_PB2CONTROLLER_H
#define GEOMCOMP_PB2CONTROLLER_H

#include <QString>

#include "controller.h"
#include "computationalgeometry.h"

namespace geomcomp
{

    class ConvexQuadrilateralController : public Controller
	{
		Q_OBJECT

	public:
        explicit ConvexQuadrilateralController(QObject *parent = nullptr, Model *model = nullptr);
        ~ConvexQuadrilateralController() = default;

		// Inherited via Controller
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;

	signals:
		void updateLabel(QString labelText);

	private:
		const int maxNbPoints = 4;
	};

}

#endif // GEOMCOMP_PB2CONTROLLER_H

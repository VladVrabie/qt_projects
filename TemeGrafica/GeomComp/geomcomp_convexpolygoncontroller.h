#ifndef GEOMCOMP_PB3CONTROLLER_H
#define GEOMCOMP_PB3CONTROLLER_H

#include <QString>

#include "controller.h"
#include "computationalgeometry.h"

namespace geomcomp
{

    class ConvexPolygonController : public Controller
	{
		Q_OBJECT

	public:
        explicit ConvexPolygonController(QObject *parent = nullptr, Model *model = nullptr);
        ~ConvexPolygonController() = default;

		// Inherited via Controller
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;

	signals:
		void updateLabel(QString labelText);

	public slots:
		void reset();

	private:
		bool finished = false;
		const int minimumNbPoints = 3;
	};

}

#endif // GEOMCOMP_PB3CONTROLLER_H

#ifndef GEOMCOMP_MANAGERTAB_H
#define GEOMCOMP_MANAGERTAB_H

#include <QWidget>
#include <QChildEvent>
#include <QComboBox>
#include <QLabel>

#include <memory>
#include "myframe.h"
#include "GeomComp/geomcomp_controllerids.h"
#include "GeomComp/geomcomp_separatehalfplanescontroller.h"
#include "GeomComp/geomcomp_convexquadrilateralcontroller.h"
#include "GeomComp/geomcomp_convexpolygoncontroller.h"
#include "GeomComp/geomcomp_colourpolygoncontroller.h"
#include "GeomComp/geomcomp_triangulationcontroller.h"
#include "GeomComp/geomcomp_convexhullcontroller.h"

namespace geomcomp
{

	class ManagerTab : public QWidget
	{
		Q_OBJECT

	public:
		explicit ManagerTab(QWidget *parent = nullptr);
		~ManagerTab() = default;

	signals:
		void resetPbController();

	public slots:
		void problemSelectionChanged(int index);
		void labelTextChanged(QString newText);

	protected:
		virtual void childEvent(QChildEvent *event) override;

	private:
		bool tryInitFrame(QObject *child);
		bool tryConnectComboBox(QObject *child);
		bool tryInitLabel(QObject *child);


		MyFrame * frame = nullptr;
		QComboBox *problemsComboBox = nullptr;
		QLabel *resultLabel = nullptr;

		ColourPolygonController *colourPolygonController = nullptr;
		//ConvexHullController *convexHullController = nullptr;
		//TriangulationController *triangulationController = nullptr;

		std::unique_ptr<Model> model = std::make_unique<Model>();
	};

}

#endif // GEOMCOMP_MANAGERTAB_H
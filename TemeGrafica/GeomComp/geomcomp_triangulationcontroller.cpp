#include "GeomComp/geomcomp_triangulationcontroller.h"

geomcomp::TriangulationController::TriangulationController(QObject *parent, Model *model)
    : Controller(parent, model)
{
}

void geomcomp::TriangulationController::myMousePressEvent(QMouseEvent * event)
{
	Qt::MouseButton pressedButton = event->button();

	if (pressedButton == Qt::LeftButton)
	{
		if (!finished)
			_model->appendData(event->localPos());
	}
	else if (pressedButton == Qt::RightButton)
	{
		if (_model->size() >= minimumNbPoints)
			finished = true;
	}
	else if (pressedButton == Qt::MiddleButton)
	{
		finished = false;
		_model->clearData();
	}
	emit updatePainting();
}

void geomcomp::TriangulationController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::TriangulationController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::TriangulationController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::TriangulationController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::TriangulationController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);
	painter.setRenderHint(QPainter::Antialiasing);

	painter.setPen(QColor(qRgb(255, 0, 0)));
	int size = _model->size();
	for (int i = 0; i < size - 1; ++i)
		painter.drawLine(_model->get<QPointF>(i), _model->get<QPointF>(i + 1));

	if (QVector<QPointF> points; finished)
	{
		painter.drawLine(_model->get<QPointF>(0), _model->get<QPointF>(_model->size() - 1));

		std::for_each(_model->begin(), _model->end(), [&points](auto point) {
			points.append(std::any_cast<QPointF>(point));
		});

		auto diagonalsArray = ComputationalGeometry::triangulatePolygon(points);

		painter.setPen(QColor(qRgb(0, 0, 0)));
		std::for_each(diagonalsArray.begin(), diagonalsArray.end(), [&painter](std::pair<QPointF, QPointF> diagonal) {
			//std::apply(&(painter.drawLine), diagonal);
			//std::apply(QOverload<const QPointF&, const QPointF&>::of(&QPainter::drawLine), painter, diagonal.first, diagonal.second);
			//std::invoke(QOverload<const QPointF&, const QPointF&>::of(&QPainter::drawLine), painter, diagonal);
			//std::invoke(QOverload<const QPointF&, const QPointF&>::of(&QPainter::drawLine), std::tuple_cat(std::tie(painter), diagonal));
			
			std::invoke(QOverload<const QPointF&, const QPointF&>::of(&QPainter::drawLine), painter, diagonal.first, diagonal.second);
			//std::apply(QOverload<const QPointF&, const QPointF&>::of(&QPainter::drawLine), std::tuple_cat(std::tie(painter), diagonal));
		});
	}
}

void geomcomp::TriangulationController::reset()
{
	finished = false;
}
#include "GeomComp/geomcomp_colourpolygoncontroller.h"

geomcomp::ColourPolygonController::ColourPolygonController(QObject * parent, Model * model)
	: Controller(parent, model)
{
}

void geomcomp::ColourPolygonController::setHeightAndWidht(int newHeight, int newWidth)
{
	height = newHeight;
	width = newWidth;
}

void geomcomp::ColourPolygonController::myMousePressEvent(QMouseEvent * event)
{
	Qt::MouseButton pressedButton = event->button();

	if (pressedButton == Qt::LeftButton)
	{
		if (!finished)
			_model->appendData(event->localPos());
	}
	else if (pressedButton == Qt::RightButton)
	{
		if (_model->size() >= minimumNbPoints)
			finished = true;
	}
	else if (pressedButton == Qt::MiddleButton)
	{
		finished = false;
		_model->clearData();
	}
	emit updatePainting();
}

void geomcomp::ColourPolygonController::myMouseMoveEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::ColourPolygonController::myMouseReleaseEvent(QMouseEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::ColourPolygonController::myKeyPressEvent(QKeyEvent * event)
{
	Q_UNUSED(event);
}

void geomcomp::ColourPolygonController::myResizeEvent(QResizeEvent * event)
{
	height = event->size().height();
	width = event->size().width();
}

void geomcomp::ColourPolygonController::myPaintEvent(QPaintEvent * event, QPainter & painter)
{
	Q_UNUSED(event);

	if (QVector<QPointF> points; finished && _model->size() >= minimumNbPoints)
	{
		std::for_each(_model->begin(), _model->end(), [&points](auto point) {
			points.append(std::any_cast<QPointF>(point));
		});

		for (int i = 0; i < width; ++i)
			for (int j = 0; j < height; ++j)
			{
				QPointF point(i, j);
				if (ComputationalGeometry::pointInPolygon(point, points))
					painter.setPen(QColor(qRgb(255, 0, 0)));
				else
					painter.setPen(QColor(qRgb(0, 255, 0)));
				painter.drawPoint(point);
			}
		
		painter.setPen(QColor(qRgb(0, 0, 0)));
		painter.drawLine(_model->get<QPointF>(0), _model->get<QPointF>(_model->size() - 1));
	}

	for (int i = 0; i < _model->size() - 1; ++i)
		painter.drawLine(_model->get<QPointF>(i), _model->get<QPointF>(i + 1));
}

void geomcomp::ColourPolygonController::reset()
{
	finished = false;
}

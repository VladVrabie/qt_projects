#ifndef GEOMCOMP_TRIANGULATIONCONTROLLER_H
#define GEOMCOMP_TRIANGULATIONCONTROLLER_H

#include <functional>
#include <tuple>
#include "controller.h"
#include "computationalgeometry.h"

namespace geomcomp
{

	class TriangulationController : public Controller
	{
		Q_OBJECT

	public:
		explicit TriangulationController(QObject *parent = nullptr, Model *model = nullptr);
		~TriangulationController() = default;

		// Inherited via Controller
		virtual void myMousePressEvent(QMouseEvent * event) override;
		virtual void myMouseMoveEvent(QMouseEvent * event) override;
		virtual void myMouseReleaseEvent(QMouseEvent * event) override;
		virtual void myKeyPressEvent(QKeyEvent * event) override;
		virtual void myResizeEvent(QResizeEvent * event) override;
		virtual void myPaintEvent(QPaintEvent * event, QPainter & painter) override;

	public slots:
		void reset();

	private:
		bool finished = false;
		const int minimumNbPoints = 4;
	};

}

#endif // GEOMCOMP_TRIANGULATIONCONTROLLER_H

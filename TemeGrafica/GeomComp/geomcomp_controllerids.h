#ifndef GEOMCOMP_IDS_H
#define GEOMCOMP_IDS_H

#include <string>

namespace geomcomp::id
{
    inline std::string separateHalfPlanesController{ "separateHalfPlanesController" };
    inline std::string convexQuadrilateralController{ "convexQuadrilateralController" };
    inline std::string convexPolygonController{ "convexPolygonController" };
    inline std::string colourPolygonController{ "colourPolygonController" };
	inline std::string triangulationController{ "triangulationController" };
	inline std::string convexHullController{ "convexHullController" };
}

#endif // GEOMCOMP_IDS_H

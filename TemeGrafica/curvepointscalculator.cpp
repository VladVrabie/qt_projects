#include "curvepointscalculator.h"

CurvePointsCalculator::CurvePointsCalculator()
{

}

CurvePointsCalculator::~CurvePointsCalculator()
{

}

QVector<QPointF> CurvePointsCalculator::calculatePoints (const fPtr &xFunction,
                                                          const fPtr &yFunction,
                                                          double start,
                                                          double end,
                                                          unsigned resolution)
{
    _f = xFunction;
    _g = yFunction;
    _start = start;
    _end = end;
    _resolution = resolution;
    _P.clear();
    _P.fill(QPointF(0.0, 0.0), resolution + 1);

    applyFunctions();
    //QVector<QPointF> finalPoints = extraProcessing();
    //return finalPoints;
    return extraProcessing();
}

void CurvePointsCalculator::applyFunctions()
{
    double resolutionStep = (_end - _start) / _resolution;

    for (unsigned i = 0u; i < _resolution; ++i)
    {
        double u = _start + i * resolutionStep;
        _P[i].setX(_f(u));
        _P[i].setY(_g(u));
    }
    _P[_resolution].setX(_f(_end));
    _P[_resolution].setY(_g(_end));
}

QVector<QPointF> CurvePointsCalculator::extraProcessing()
{
    return _P;
}

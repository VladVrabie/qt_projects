#ifndef POLIGON_TRANFORMATIONCONTROLLER_H
#define POLIGON_TRANFORMATIONCONTROLLER_H

#include <QPainterPath>
#include <QtMath>

#include "controller.h"
#include "Poligon/poligon_poligon2d.h"
#include "Poligon/poligon_controllerids.h"

namespace poligon
{

    class TransformationController : public Controller
    {
        Q_OBJECT

    public:
        explicit TransformationController(QObject *parent = nullptr, Model *model = nullptr);
        ~TransformationController();

        // Controller interface
		virtual void myMousePressEvent(QMouseEvent *event) override;
		virtual void myMouseMoveEvent(QMouseEvent *event) override;
		virtual void myMouseReleaseEvent(QMouseEvent *event) override;
		virtual void myKeyPressEvent(QKeyEvent *event) override;
		virtual void myResizeEvent(QResizeEvent *event) override;
		virtual void myPaintEvent(QPaintEvent *event, QPainter & painter) override;

    private:
        Poligon2D *poligon = nullptr;
        Qt::MouseButton pressedButton = Qt::NoButton;
        QPointF mousePressPos;
        QPointF lastPos;
    };

}

#endif // POLIGON_TRANFORMATIONCONTROLLER_H

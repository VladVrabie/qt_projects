#include "Poligon/poligon_transformationcontroller.h"

poligon::TransformationController::TransformationController(QObject *parent, Model *model)
    : Controller(parent, model)
{
    poligon = std::any_cast<Poligon2D>(&(*model)[0]);
}

poligon::TransformationController::~TransformationController()
{

}

void poligon::TransformationController::myMousePressEvent(QMouseEvent *event)
{
    pressedButton = event->button();
    mousePressPos = event->localPos();
    lastPos = mousePressPos;
}

void poligon::TransformationController::myMouseMoveEvent(QMouseEvent *event)
{
    QPointF currentPos = event->localPos();

    switch (pressedButton) {
    case Qt::LeftButton: // translatie
    {
        QPointF delta = currentPos - lastPos;
        poligon->translatie(delta.x(), delta.y());

        poligon->aplicaTransformare();
        poligon->calculateCentroid();
        break;
    }
    case Qt::MiddleButton: // rotatie
    {
        double currentAngle = atan2(currentPos.y() - poligon->getCentroid().y(),
                                    currentPos.x() - poligon->getCentroid().x());
        if (currentAngle < 0)
            currentAngle += 2 * M_PI;

        double previousAngle = atan2(lastPos.y() - poligon->getCentroid().y(),
                                     lastPos.x() - poligon->getCentroid().x());
        if (previousAngle < 0)
            previousAngle += 2 * M_PI;

        double delta = currentAngle - previousAngle;
        poligon->rotatiePunct(poligon->getCentroid(), delta);

        poligon->aplicaTransformare();
        break;
    }
    case Qt::RightButton: // scalare
    {
        // Varianta cu scalare pe x si pe y
//        double sx = (currentPos.x() - model->getCentroid().x())
//                / (lastPos.x() - model->getCentroid().x());
//        double sy = (currentPos.y() - model->getCentroid().y())
//                / (lastPos.y() - model->getCentroid().y());

//        model->scalarePunct(model->getCentroid(), sx, sy);


        double currentDistance = qSqrt(qPow((currentPos.x() - poligon->getCentroid().x()), 2.0)
                                  + qPow((currentPos.y() - poligon->getCentroid().y()), 2.0));

        double lastDistance = qSqrt(qPow((lastPos.x() - poligon->getCentroid().x()), 2.0)
                                  + qPow((lastPos.y() - poligon->getCentroid().y()), 2.0));
        double s = currentDistance / lastDistance;

        poligon->scalarePunct(poligon->getCentroid(), s, s);

        poligon->aplicaTransformare();
        break;
    }
    default:
        break;
    }

    lastPos = currentPos;
    emit updatePainting();
}

void poligon::TransformationController::myMouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
    pressedButton = Qt::NoButton;
}

void poligon::TransformationController::myKeyPressEvent(QKeyEvent *event)
{
    // cand apas 'r' sa se reseteze poligonul
    if (event->key() == Qt::Key_R)
    {
        poligon->undoTransformations();
        emit updatePainting();
    }
    // cand apas 'd' se intoarce la modul desenat
    else if (event->key() == Qt::Key_D)
    {
        poligon->clearPolygon();
		emit switchController(id::drawingController);
    }
    // cand apas 's' se face simetrie
    else if (event->key() == Qt::Key_S)
    {
        poligon->simetrieDreapta(0.0, 0.0, 300.0, 300.0);
        poligon->aplicaTransformare();
        poligon->calculateCentroid();
        emit updatePainting();
    }
}

void poligon::TransformationController::myResizeEvent(QResizeEvent * event)
{
	Q_UNUSED(event);
}

void poligon::TransformationController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
    Q_UNUSED(event);
    int size = poligon->getPoints().size();

    QPainterPath path(poligon->getPoints()[0]);
    for (int i = 1; i < size; ++i)
        path.lineTo(poligon->getPoints()[i]);

    path.lineTo(poligon->getPoints()[0]);
    painter.drawPath(path);

    painter.drawEllipse(poligon->getCentroid(), 3.0, 3.0);
}

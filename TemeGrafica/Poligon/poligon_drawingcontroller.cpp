#include "Poligon/poligon_drawingcontroller.h"

poligon::DrawingController::DrawingController(QObject *parent, Model *model)
    : Controller(parent, model)
{
    poligon = std::any_cast<Poligon2D>(&(*model)[0]);
}

poligon::DrawingController::~DrawingController()
{

}

void poligon::DrawingController::myMousePressEvent(QMouseEvent *event)
{
    Qt::MouseButton pressedButton = event->button();
    switch (pressedButton)
    {
    case Qt::LeftButton:
        poligon->getPoints().append(event->localPos());
        break;

    case Qt::MiddleButton:
        poligon->getPoints().clear();
        emit updatePainting();
        break;

    case Qt::RightButton:
        if (poligon->getPoints().size() > 2)
        {
            poligon->setInitialPoints();
            poligon->calculateCentroid();
            emit switchController(id::transformController);
        }
        break;
    }
}

void poligon::DrawingController::myMouseMoveEvent(QMouseEvent *event)
{
    currentMousePosition = event->localPos();
    emit updatePainting();
}

void poligon::DrawingController::myMouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void poligon::DrawingController::myKeyPressEvent(QKeyEvent *event)
{
    Q_UNUSED(event);
}

void poligon::DrawingController::myResizeEvent(QResizeEvent *event)
{
	Q_UNUSED(event);
}

void poligon::DrawingController::myPaintEvent(QPaintEvent *event, QPainter &painter)
{
    Q_UNUSED(event);
    int size = poligon->getPoints().size();
    if (size > 0)
    {
        QPainterPath path(poligon->getPoints()[0]);
        for (int i = 1; i < size; ++i)
            path.lineTo(poligon->getPoints()[i]);
        path.lineTo(currentMousePosition);
        painter.drawPath(path);
    }
}

#ifndef POLIGON_POLIGON2D_H
#define POLIGON_POLIGON2D_H

#include "transform.h"

namespace poligon
{

    class Poligon2D
    {
    public:
        Poligon2D();
        ~Poligon2D();

        QVector<QPointF>& getPoints();
        QPointF getCentroid();

        void aplicaTransformare();
        void translatie(double dx, double dy);
        void scalarePunct(const QPointF &p, double sx, double sy);
        void scalareOrig(double sx, double sy);
        void rotatiePunct(const QPointF &p, double alpha);
        void rotatieOrig(double alpha);
        void simetrieDreapta(double xA, double yA, double xB, double yB);

        void setInitialPoints();
        void calculateCentroid();
        void undoTransformations();
        void clearPolygon();

    private:
        QVector<QPointF> points;
        QVector<QPointF> initialPoints;
        QPointF centroid;
        Transform transform;
    };

}

#endif // POLIGON_POLIGON2D_H

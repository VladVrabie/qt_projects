#include "Poligon/poligon_poligon2d.h"

poligon::Poligon2D::Poligon2D()
{

}

poligon::Poligon2D::~Poligon2D()
{

}

QVector<QPointF>& poligon::Poligon2D::getPoints()
{
    return points;
}

QPointF poligon::Poligon2D::getCentroid()
{
    return centroid;
}

void poligon::Poligon2D::aplicaTransformare()
{
    transform.aplicaTransformare(initialPoints, points);
}

void poligon::Poligon2D::translatie(double dx, double dy)
{
    transform.translatie(dx, dy);
}

void poligon::Poligon2D::scalarePunct(const QPointF &p, double sx, double sy)
{
    transform.scalarePunct(p, sx, sy);
}

void poligon::Poligon2D::scalareOrig(double sx, double sy)
{
    transform.scalareOrig(sx, sy);
}

void poligon::Poligon2D::rotatiePunct(const QPointF &p, double alpha)
{
    transform.rotatiePunct(p, alpha);
}

void poligon::Poligon2D::rotatieOrig(double alpha)
{
    transform.rotatieOrig(alpha);
}

void poligon::Poligon2D::simetrieDreapta(double xA, double yA, double xB, double yB)
{
    transform.simetrieDreapta(xA, yA, xB, yB);
}

void poligon::Poligon2D::setInitialPoints()
{
    initialPoints = points;
}

void poligon::Poligon2D::calculateCentroid()
{
    double area = 0.0;
    double cx = 0.0;
    double cy = 0.0;
    int nbPoints = points.size();

    for (int i = 0; i < nbPoints; ++i)
    {
        double aux = points[i].x() * points[(i + 1) % nbPoints].y()
              - points[(i + 1) % nbPoints].x() * points[i].y();
        area += aux;

        cx += (points[i].x() + points[(i + 1) % nbPoints].x()) * aux;
        cy += (points[i].y() + points[(i + 1) % nbPoints].y()) * aux;
    }

    area /= 2.0;
    cx /= 6.0 * area;
    cy /= 6.0 * area;

    centroid.setX(cx);
    centroid.setY(cy);
}

void poligon::Poligon2D::undoTransformations()
{
    points = initialPoints;
    calculateCentroid();
    transform.toIdentity();
}

void poligon::Poligon2D::clearPolygon()
{
    transform.toIdentity();
    initialPoints.clear();
    points.clear();
}

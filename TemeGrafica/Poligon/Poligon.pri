HEADERS += \
    $$PWD/poligon_frame.h \
    $$PWD/poligon_drawingcontroller.h \
    $$PWD/poligon_transformationcontroller.h \
    $$PWD/poligon_poligon2d.h \
    $$PWD/poligon_controllerids.h

SOURCES += \
    $$PWD/poligon_frame.cpp \
    $$PWD/poligon_drawingcontroller.cpp \
    $$PWD/poligon_transformationcontroller.cpp \
    $$PWD/poligon_poligon2d.cpp


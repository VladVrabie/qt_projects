#ifndef POLIGON_CONTROLLERIDS_H
#define POLIGON_CONTROLLERIDS_H

#include <string>

namespace poligon::id
{
	// need some explanation for this magic:
    inline std::string drawingController{ "drawingController" };
    inline std::string transformController{ "transformationController" };
}

#endif // POLIGON_CONTROLLERIDS_H

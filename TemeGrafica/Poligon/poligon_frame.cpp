#include "Poligon/poligon_frame.h"

poligon::Frame::Frame(QWidget *parent)
    : MyFrame(parent)
{
	// initializarea modelului
    model->appendData(Poligon2D());

    DrawingController *drawingController = new DrawingController(this, model.get());
	_switcher->addController(id::drawingController, drawingController);

	ControllerSwitcher::ConnectionArgs drawUpdateArgs{ drawingController, SIGNAL(updatePainting()), this, SLOT(update()) };
	_switcher->addConnection(id::drawingController, drawUpdateArgs);

	ControllerSwitcher::ConnectionArgs drawSwitchArgs{ drawingController, SIGNAL(switchController(std::string)),
														_switcher, SLOT(switchToController(std::string)) };
	_switcher->addConnection(id::drawingController, drawSwitchArgs);


	TransformationController *transformationController = new TransformationController(this, model.get());
	_switcher->addController(id::transformController, transformationController);

	ControllerSwitcher::ConnectionArgs transfUpdateArgs{ transformationController, SIGNAL(updatePainting()), this, SLOT(update()) };
	_switcher->addConnection(id::transformController, transfUpdateArgs);

	ControllerSwitcher::ConnectionArgs transfSwitchArgs{ transformationController, SIGNAL(switchController(std::string)),
														_switcher, SLOT(switchToController(std::string)) };
	_switcher->addConnection(id::transformController, transfSwitchArgs);


	// initialize with drawingController
	_switcher->switchToController(id::drawingController);
}

poligon::Frame::~Frame()
{

}

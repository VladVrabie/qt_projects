#ifndef POLIGON_FRAME_H
#define POLIGON_FRAME_H

#include <memory>

#include "model.h"
#include "myframe.h"
#include "Poligon/poligon_poligon2d.h"
#include "Poligon/poligon_drawingcontroller.h"
#include "Poligon/poligon_transformationcontroller.h"

namespace poligon
{

    class Frame : public MyFrame
    {
        Q_OBJECT

    public:
        explicit Frame(QWidget *parent = nullptr);
        ~Frame();

    private:
        std::unique_ptr<Model> model = std::make_unique<Model>();
    };

}

#endif // POLIGON_FRAME_H

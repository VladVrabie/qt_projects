#ifndef POLIGON_DRAWINGCONTROLLER_H
#define POLIGON_DRAWINGCONTROLLER_H

#include <QPainterPath>

#include "controller.h"
#include "Poligon/poligon_poligon2d.h"
#include "Poligon/poligon_controllerids.h"

namespace poligon
{

    class DrawingController : public Controller
    {
        Q_OBJECT

    public:
        explicit DrawingController(QObject *parent = nullptr, Model *model = nullptr);
        ~DrawingController();

        // Controller interface
        virtual void myMousePressEvent(QMouseEvent *event) override;
        virtual void myMouseMoveEvent(QMouseEvent *event) override;
        virtual void myMouseReleaseEvent(QMouseEvent *event) override;
        virtual void myKeyPressEvent(QKeyEvent *event) override;
		virtual void myResizeEvent(QResizeEvent *event) override;
        virtual void myPaintEvent(QPaintEvent *event, QPainter & painter) override;

    private:
        QPointF currentMousePosition;
        Poligon2D *poligon = nullptr;
    };

}

#endif // POLIGON_DRAWINGCONTROLLER_H

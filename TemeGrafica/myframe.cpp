#include "myframe.h"

MyFrame::MyFrame(QWidget *parent)
    : QFrame(parent)
{
    _switcher = new ControllerSwitcher(this);
    connect(_switcher, &ControllerSwitcher::controllerChanged,
            this, &MyFrame::setController);
}

MyFrame::~MyFrame()
{

}

ControllerSwitcher* MyFrame::getSwitcher()
{
    return _switcher;
}

void MyFrame::setController(Controller *controller)
{
    this->_controller = controller;
}

void MyFrame::mousePressEvent(QMouseEvent *event)
{
    if (_controller)
        _controller->myMousePressEvent(event);;
}

void MyFrame::mouseMoveEvent(QMouseEvent *event)
{
    if (_controller)
        _controller->myMouseMoveEvent(event);
}

void MyFrame::mouseReleaseEvent(QMouseEvent *event)
{
    if (_controller)
        _controller->myMouseReleaseEvent(event);
}

void MyFrame::keyPressEvent(QKeyEvent *event)
{
    if (_controller)
        _controller->myKeyPressEvent(event);
}

void MyFrame::resizeEvent(QResizeEvent * event)
{
	if (_controller)
		_controller->myResizeEvent(event);
}

void MyFrame::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    if (_controller)
        _controller->myPaintEvent(event, painter);
}

#ifndef CURVEPOINTSCALCULATOR_H
#define CURVEPOINTSCALCULATOR_H

#include <QVector>
#include <QPointF>
#include <functional>

class CurvePointsCalculator
{
public:
    CurvePointsCalculator();
    ~CurvePointsCalculator();


    typedef std::function<double(double)> fPtr;

    QVector<QPointF> calculatePoints(const fPtr & xFunction,
                                     const fPtr & yFunction,
                                     double start,
                                     double end,
                                     unsigned resolution);

private:
    void applyFunctions();

protected:
    virtual QVector<QPointF> extraProcessing();


    fPtr _f;
    fPtr _g;
    double _start;
    double _end;
    unsigned _resolution;
    QVector<QPointF> _P;
};

#endif // CURVEPOINTSCALCULATOR_H
